import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:womenes/Models/Request/RegisterRequest.dart';
import 'package:womenes/Services/RetrofitServices.dart';

import 'Login.dart';

bool _obscureTextP = true;
bool _obscureTextCP = true;

final _fromkey = GlobalKey<FormState>();
final _scaffoldstate = GlobalKey<ScaffoldState>();

class RegistrationPage extends StatefulWidget {
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  bool autovalidate = false;

  TextEditingController _controllerUsn = TextEditingController();
  TextEditingController _controllerFn = TextEditingController();
  TextEditingController _controllerLn = TextEditingController();
  TextEditingController _controllerEn = TextEditingController();
  TextEditingController _controllerMn = TextEditingController();
  TextEditingController _controllerPn = TextEditingController();
  TextEditingController _controllerCPn = TextEditingController();

  @override
  Widget build(BuildContext context) {

    String valiadeUsername(String value) {
      if (value.isEmpty) {
        return "Enter UserName";
      }
      if (value.length < 3) {
        return "UserName Should be minimum 3 chracters";
      }
      String un = "[a-zA-Z0-9]";
      RegExp regExp = new RegExp(un);
      if (regExp.hasMatch(value)) {
        return null;
      }
      return null;
    }

    String valiadeFirstname(String value) {
      String patttern = r'(^[a-zA-Z ]*$)';
      RegExp regExp = new RegExp(patttern);

      if (value.isEmpty||value.length < 3||regExp.hasMatch(value)) {
        return "Please enter a valid name ";
      }
      else {
        return null;
      }
    }

    String valiadeLastname(String value) {

      String patttern = r'(^[a-zA-Z ]*$)';
      RegExp regExp = new RegExp(patttern);

      if (value.isEmpty||value.length < 3||regExp.hasMatch(value)) {
        return "Please enter a valid name ";
      }
      else {
        return null;
      }
    }

    String validateEmail(String value) {
      if (value.isEmpty) {
        return 'Enter Your Email';
      }
      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return 'Enter Valid Email';
      else
        return null;
    }

    String validateMobile(String value) {
      String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
      RegExp regExp = new RegExp(patttern);
      if (value.length == 0 ||!regExp.hasMatch(value) ) {
        return 'Please enter  Valid mobile number';
      }
      else if (!regExp.hasMatch(value)) {
        return 'Please enter valid mobile number';
      }
      return null;
    }

    String valiadtePassword(String value) {
      if (value.length == 0) {
        return "Enter Password";
      }
      if (value.length < 6)
        return "password should be more than 6 charactes";
      else {
        return null;
      }
    }

    String validatepwdmatch(String convalue) {
      var pwd = _controllerPn.text;
      if (convalue.length == 0) {
        return 'confirm password is required';
      }
      if (convalue != pwd) {
        return 'password mismatch';
      } else {
        return null;
      }
    }

    final _userName = TextFormField(
        validator: valiadeUsername,
        controller: _controllerUsn,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: "UserName",
          labelText: "Enter UserName",
          border: new OutlineInputBorder(
            borderSide: new BorderSide(),
          ),
          prefixIcon: const Icon(
            Icons.account_circle,
            color: Color(0xffff1493),
          ),
        ));

    final _firstName = TextFormField(
        validator: valiadeFirstname,
        controller: _controllerFn,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: "FirstName",
          labelText: "Enter FirstName",
          border: new OutlineInputBorder(
            borderSide: new BorderSide(),
          ),
          prefixIcon: const Icon(
            Icons.person,
            color: Color(0xffff1493),
          ),
        ));

    final _lastName = TextFormField(
        validator: valiadeLastname,
        controller: _controllerLn,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: "LastName",
          labelText: "Enter LastName",
          border: new OutlineInputBorder(
            borderSide: new BorderSide(),
          ),
          prefixIcon: const Icon(
            Icons.person,
            color: Color(0xffff1493),
          ),
        ));

    final _email = TextFormField(
        controller: _controllerEn,
        validator: validateEmail,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          hintText: "Email",
          labelText: "Enter Email",
          border: new OutlineInputBorder(
            borderSide: new BorderSide(),
          ),
          prefixIcon: const Icon(
            Icons.mail,
            color: Color(0xffff1493),
          ),
        ));

    final _mobile = TextFormField(
        validator: validateMobile,
        controller: _controllerMn,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
          hintText: "MobileNumber",
          labelText: "Enter MobileNumber",
          border: new OutlineInputBorder(
            borderSide: new BorderSide(),
          ),
          prefixIcon: const Icon(
            Icons.phone,
            color: Color(0xffff1493),
          ),
        ));

    final _password = TextFormField(
        obscureText: _obscureTextP,
        validator: valiadtePassword,
        controller: _controllerPn,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: "Password",
          labelText: "Enter Password",
          border: new OutlineInputBorder(
            borderSide: new BorderSide(),
          ),
          prefixIcon: const Icon(
            Icons.lock,
            color: Color(0xffff1493),
          ),
          suffixIcon: GestureDetector(
            child: const Icon(Icons.remove_red_eye),
            onTap: (){
              setState(() {
                _obscureTextP =! _obscureTextP;
              });
            },
          ),
        ));

    final _confirmpassword = TextFormField(
        obscureText: _obscureTextCP,
        validator: validatepwdmatch,
        controller: _controllerCPn,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: "ConfirmPassword",
          labelText: "Re-Enter Password",
          border: new OutlineInputBorder(
            borderSide: new BorderSide(),
          ),
          prefixIcon: const Icon(
            Icons.lock,
            color: Color(0xffff1493),
          ),
          suffixIcon: GestureDetector(
            child: const Icon(Icons.remove_red_eye),
            onTap: (){
              setState(() {
                _obscureTextCP =! _obscureTextCP;
              });
            },
          ),
        ));

    final button = CupertinoButton.filled(
        child: Text('Register'),
        onPressed: () {
          /* if(_formKey.currentState.validate()){
       return Text('hai');
      }else{
        return null;
      }*/

          String userName = _controllerUsn.text.toString().trim();
          String firstName = _controllerFn.text.toString().trim();
          String lastName = _controllerLn.text.toString().trim();
          String email = _controllerEn.text.toString().trim();
          String mobileNumber = _controllerMn.text.toString().trim();
          String password = _controllerPn.text.toString().trim();
          String confirmPassword = _controllerCPn.text.toString().trim();

          if (userName.isEmpty) {
            showSnackbarMessage("user name is required");
          } else if (firstName.isEmpty) {
            showSnackbarMessage("first name is required");
          } else if (lastName.isEmpty) {
            showSnackbarMessage("lastname is required");
          } else if (email.isEmpty) {
            showSnackbarMessage("email is required");
          } else if (mobileNumber.isEmpty) {
            showSnackbarMessage("mobile number is required");
          } else if (password.isEmpty) {
            showSnackbarMessage("password is mandidatory");
          } else if (confirmPassword.isEmpty) {
            showSnackbarMessage("confirm password is requires");
          } else {
            print(userName);
            print(firstName);
            print(lastName);

            RegistrationRequest register = new RegistrationRequest(
                userName: userName,
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNumber: mobileNumber,
                password: password,
                confirmPassword: confirmPassword);

            final dio = Dio(); // Provide a dio instance
            final client = RestClient(dio);
            client.PostRegister(register).then((res) {
              print(res.endUserMessage);
              Fluttertoast.showToast(
                  msg: res.endUserMessage,
                  toastLength: Toast.LENGTH_SHORT,
                  timeInSecForIos: 2);
              Navigator.pop(context);
              // showSnackbarMessage(res.endUserMessage);
//         RegistrationResponse response = RegistrationResponse.fromJson(res.toJson());
            }).catchError((Object obj) {
              print(obj.toString());
              switch (obj.runtimeType) {
                case DioError:
                  final res = (obj as DioError).response;
                  if (res.statusCode == 401) {
                    logger.e(
                        "Got error 401 : ${res.statusCode} -> ${res.statusMessage}");
                  } else {
                    logger.e(
                        "Got error : ${res.statusCode} -> ${res.statusMessage}");
                  }
                  break;
                default:
              }
            });
          }
        });

    final resetLabel = FlatButton(
      child: Text(
        'RESET',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {
        reset();
      },
    );

    return SafeArea(
      child: Scaffold(
        key: _scaffoldstate,
        appBar: AppBar(
          title: Text('Womenes'),
          centerTitle: true,
        ),
        body: Form(
            key: _fromkey,
            autovalidate: true,
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(9),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 22,
                    ),
                    Text(
                      "Registration",
                      style: TextStyle(
                        fontSize: 22,
                        color: Color(0xffff1493),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    _userName,
                    SizedBox(
                      height: 10,
                    ),
                    _firstName,
                    SizedBox(
                      height: 10,
                    ),
                    _lastName,
                    SizedBox(
                      height: 10,
                    ),
                    _email,
                    SizedBox(
                      height: 10,
                    ),
                    _mobile,
                    SizedBox(
                      height: 10,
                    ),
                    _password,
                    SizedBox(
                      height: 10,
                    ),
                    _confirmpassword,
                    SizedBox(
                      height: 10,
                    ),
                    button,
                    SizedBox(
                      height: 22,
                    ),
                    resetLabel,
                  ],
                ),
              ),
            )),
      ),
    );
  }

  void reset() {
    _controllerUsn.text = '';
    _controllerFn.text = '';
    _controllerLn.text = '';
    _controllerEn.text = '';
    _controllerMn.text = '';
    _controllerPn.text = '';
    _controllerCPn.text = '';
  }

  void showSnackbarMessage(String message) {
    _scaffoldstate.currentState.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }
}
