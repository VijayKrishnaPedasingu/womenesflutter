// To parse this JSON data, do
//
//     final basicProfileReq = basicProfileReqFromJson(jsonString);

import 'dart:convert';

List<BasicProfileReq> basicProfileReqFromJson(String str) => List<BasicProfileReq>.from(json.decode(str).map((x) => BasicProfileReq.fromJson(x)));

String basicProfileReqToJson(List<BasicProfileReq> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BasicProfileReq {
  int questionId;
  String text;
  String userId;

  BasicProfileReq({
    this.questionId,
    this.text,
    this.userId,
  });

  factory BasicProfileReq.fromJson(Map<String, dynamic> json) => BasicProfileReq(
    questionId: json["questionId"] == null ? null : json["questionId"],
    text: json["text"] == null ? null : json["text"],
    userId: json["userId"] == null ? null : json["userId"],
  );

  Map<String, dynamic> toJson() => {
    "questionId": questionId == null ? null : questionId,
    "text": text == null ? null : text,
    "userId": userId == null ? null : userId,
  };
}
