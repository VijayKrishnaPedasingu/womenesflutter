// To parse this JSON data, do
//
//     final forgetRequest = forgetRequestFromJson(jsonString);

import 'dart:convert';

class ForgetRequest {
  String userNameOrEmail;

  ForgetRequest({
    this.userNameOrEmail,
  });

  factory ForgetRequest.fromRawJson(String str) => ForgetRequest.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ForgetRequest.fromJson(Map<String, dynamic> json) => ForgetRequest(
    userNameOrEmail: json["userNameOrEmail"] == null ? null : json["userNameOrEmail"],
  );

  Map<String, dynamic> toJson() => {
    "userNameOrEmail": userNameOrEmail == null ? null : userNameOrEmail,
  };
}
