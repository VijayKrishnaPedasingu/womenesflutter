import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Response/GetPackages.dart';
import '../../Services/RetrofitServices.dart';
import '../../Utils/Animations.dart';
import '../../Utils/StringsFile.dart';
import '../Account/Login.dart';
import 'PackagesDes.dart';

class PackagesList extends StatefulWidget {
  @override
  _PackagesListState createState() => _PackagesListState();
}

class _PackagesListState extends State<PackagesList> {
  String token;
  SharedPreferences pref;
  String userId;
  final AsyncMemoizer _memoizer = AsyncMemoizer();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor:Color(0xffff1493),
          title: Text('Packages'),
        ),
        body: FadeIn(1, PackageList()));
  }

  Widget PackageList() {
    return FutureBuilder(
        future: _fetchData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data != null) {
              PackagesResponse data = snapshot.data;

              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                child: ListView.builder(
                    itemCount: data.listResult.length,
                    itemBuilder: (BuildContext context, int index) {
                      return FadeIn(2, packages(data, index));
                    }),
              );
            } else {
              return Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.flash_on,color: Colors.yellow,size: 50,),
                    Text("Something Went Wrong !!",style: TextStyle(color: Colors.black87,fontSize: 18),)
                  ],
                ),
              );
            }
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(
                valueColor:
                new AlwaysStoppedAnimation<Color>(Color(0xffff1493)),
              ),
            );
          } else {
            return Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.flash_on,
                    color: Colors.yellow,
                    size: 50,
                  ),
                  Text(
                    "No Internet",
                    style: TextStyle(color: Colors.black87, fontSize: 18),
                  )
                ],
              ),
            );
          }
        });
  }

  Widget packages(PackagesResponse data, int index) {
    return Card(
      color: Colors.white,
      elevation: 2,
      child: ListTile(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PackageRating(
                      data.listResult[index].fileLocation +
                          data.listResult[index].fileName +
                          data.listResult[index].fileExtension,
                      data.listResult[index].name,
                      data.listResult[index].description,
                      data.listResult[index].totalAmount,
                      data.listResult[index].duration,
                      data.listResult[index].id)));
        },
        leading: CircleAvatar(
          radius: 35.0,
          backgroundImage: NetworkImage(data.listResult[index].fileLocation +
              data.listResult[index].fileName +
              data.listResult[index].fileExtension),
        ),
        isThreeLine: true,
        title: Text(data.listResult[index].name,
            style: TextStyle(fontSize: 18, color: Colors.black)),
        subtitle: Column(
          children: <Widget>[
            Text(
              data.listResult[index].description,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 14, color: Colors.black45),
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: <Widget>[
                Text(
                  "Amount : ${data.listResult[index].totalAmount}",
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  "Duration : ${data.listResult[index].duration.trim().substring(0, 5)} Hr",
                  overflow: TextOverflow.fade,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlutterRatingBar(
                  initialRating: data.listResult[index].avgRating,
                  ignoreGestures: true,
                  itemSize: 20,
                  fillColor: Color(0xffff1493),
                  borderColor: Colors.grey,
                  allowHalfRating: true,
                  onRatingUpdate: (rating) {},
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  _fetchData() {
    return this._memoizer.runOnce(() async {
      return getData();
    });
  }

  Future<PackagesResponse> getData() async {
    pref = await SharedPreferences.getInstance();
    token = await pref.getString(StringsFile.AccessToken);


    final dio = Dio();
    PackagesResponse packages;
    dio.options.headers["Authorization"] = ("Bearer " + token);
    final client = RestClient(dio);
    packages = (await client.getPackages().then((res) {
      logger.d(res.listResult);
      return res;
    }).catchError((Object obj) {
      print(obj);
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;

          if(res.statusCode == 401){
            pref.clear();
            pref.commit();
            Navigator.pushReplacement(context,MaterialPageRoute(builder: (context)=> LoginPage()) );
            Fluttertoast.showToast(
                msg: " Connection TimeOut..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor:Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }else if (res.statusCode == 500) {
            Fluttertoast.showToast(
                msg: "Failed to connect Server",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor:Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }
          else {
            Fluttertoast.showToast(
                msg: " Error Occured ..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor:Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);

          }
          break;
        default:
      }
    }));


    return packages;
  }
}
