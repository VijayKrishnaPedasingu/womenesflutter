import 'dart:async';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Request/BasicProfileReq.dart';
import 'package:womenes/Models/Response/BasicProfileRes.dart';

import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/Utils/StringsFile.dart';

import '../../Models/Response/GetQuestionsByUserId.dart';
import '../Account/Login.dart';

List<Widget> DynamicWidgets = List<Widget>();
List<TextEditingController> DynamicTextControllers = List<TextEditingController>();

class BasicProfile extends StatefulWidget {
  @override
  _BasicProfileState createState() => _BasicProfileState();
}

class _BasicProfileState extends State<BasicProfile> {
  SharedPreferences sharedPreferences;

  Future futuredata;
  var token, userid;

  String _picked;
  List<String> _checked;
  GetQuestions list;
  var checkradio = null;
  List<String> checkcheckbox = [];
  String selectedvalue;


  @override
  void initState() {
    futuredata = getQuestionsData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Basic Profile"),
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            onPressed: () {

              updateprofile();
            },
            child: Text("Save",style: TextStyle(fontSize: 18),),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent,)
            ),
          ),
        ],),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[


          FutureBuilder(
              future: futuredata,
              builder: (context, snapshot) {
                list = snapshot.data;
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.data != null) {
                    // print(DynamicWidgets.length);
                    return Container(

                      child: SingleChildScrollView(
                          child: Column(
                            children: DynamicWidgets,
                          )
                      ),
                    );

                  } else {
                    return Center(child: Text('Error Occured'));
                  }
                } else if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor:
                      new AlwaysStoppedAnimation<Color>(Color(0xffff1493)),
                    ),

                  );
                } else {
                  return Center(child: Text("No Internet"));
                }
              }),
          /* RaisedButton(
            onPressed: () {
              updateprofile();
            },
            child: Text("Save",style: TextStyle(fontSize: 18,),),
            color: Color(0xffff1493),
            textColor: Colors.white,
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(30.0),
            ),
          ),*/

        ],
        ),
      ),
    );
  }

  Future getQuestionsData() async {
    sharedPreferences = await SharedPreferences.getInstance();
    userid = sharedPreferences.getString(StringsFile.UserID);
    token = sharedPreferences.getString(StringsFile.AccessToken);

    DynamicWidgets.clear();
    /* await Future.delayed(const Duration(milliseconds: 3000));*/

    final dio = Dio(); // Provide a dio instance
    dio.options.headers["Authorization"] = "Bearer " + token;
    //   dio.interceptors.add(element);
    final client = RestClient(dio);
    GetQuestions response =
    await client.getQuestionsAndAnswers(userid).then((questionanswers) {
      return questionanswers;
      //   logger.d(questionanswers.listResult);
    }).catchError((Object obj) {
      print(obj.runtimeType.toString());
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;
          if (res.statusCode == 401) {
            logger.e("Got error 401 : ${res.statusCode} -> ${res.statusMessage}");
          } else {
            logger.e("Got error : ${res.statusCode} -> ${res.statusMessage}");
          }
          break;
        default:
      }
    });
    // client.getQuestions(userid)
    dynamicWidgets(response);

    return response;
  }

  dynamicWidgets(GetQuestions data) {
    List<String> options = [];

    for (int i = 0; i < data.listResult.length; i++) {
      SizedBox(
        width: 100,
        height: 10,
      );



      if (data.listResult.elementAt(i).questionType.name == "description") {
        DynamicWidgets.add(Container(
            width: double.infinity,
            margin: EdgeInsets.only(right: 20, left: 20, top: 10),
            child: new prefix0.Text(
              data.listResult.elementAt(i).header.toString(),
              style: TextStyle(fontSize: 20),
              textAlign: TextAlign.start,
            )));
        TextEditingController  textControllers = new TextEditingController();
        for(int j = 0; j < data.listResult.elementAt(i).userAnswers.length; j++) {
          data.listResult.elementAt(i).userAnswers == null ? null :
          textControllers.text = data.listResult.elementAt(i).userAnswers.elementAt(j).text;
        }

        DynamicWidgets.add(Container(
            width: double.infinity,
            margin: EdgeInsets.only(right: 20, left: 20, top: 10),
            child: new TextFormField(
                controller: textControllers,
                style: TextStyle(fontSize: 18),
                textAlign: TextAlign.start,
                /* onChanged: (value){

                 textControllers.text = value;

                },*/
                decoration: InputDecoration(
                  hintText: "Answers",
                  border: OutlineInputBorder(borderSide: BorderSide()),
                ))));
        DynamicTextControllers.add(textControllers);
      }
  /*    else if (data.listResult.elementAt(i).questionType.name.contains("boolean")) {

        for(int j = 0 ; j<data.listResult[i].questionSets.length;j++){
          if(  data.listResult[i].id == data.listResult[i].questionSets[j].questionId )
          {
            options.add(data.listResult[i].questionSets[j].option);
          }

          // print(data.listResult[i].userAnswers.length);
          // print("msg"+data.listResult[i].userAnswers.elementAt(0).text);
        }
        checkradio  = data.listResult[i].userAnswers.length !=0 ? data.listResult[i].userAnswers.elementAt(0).text:null;
        print("radio $checkradio");

        DynamicWidgets.add((RadioButtonGroup(
          //picked: checkradio,
          orientation: GroupedButtonsOrientation.VERTICAL,
          margin: const EdgeInsets.only(left: 12.0),
          onSelected: (selected){
            _picked = selected;
            print(_picked);
          },

          activeColor: Color(0xffff1493),


          labels: options.toList(),
          itemBuilder: (Radio rb, Text txt, int i) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                rb,
                txt,
              ],
            );
          },
        )
        ));
        options.clear();
      }
      else if (data.listResult.elementAt(i).questionType.name.contains("options")) {

        for(int j=0; j<data.listResult.elementAt(i).questionSets.length;j++){

          if(data.listResult[i].id == data.listResult[i].questionSets[j].questionId){
            options.add(data.listResult[i].questionSets[j].option);

          }
        }


        data.listResult[i].userAnswers.length !=0 ? checkcheckbox.add(data.listResult[i].userAnswers.elementAt(0).text): null;
        print("ckeckbox $checkcheckbox");

        DynamicWidgets.add(CheckboxGroup(
          //checked: checkcheckbox,
          orientation: GroupedButtonsOrientation.VERTICAL,
          margin: const EdgeInsets.only(left: 12.0),
          activeColor: Color(0xffff1493),
          onSelected: (List selected){
            _checked = selected;
            print(_checked);
          },

          labels: options.toList() ,


          itemBuilder: (Checkbox cb, Text txt, int i){
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                cb,
                txt,
              ],
            );
          },
        )

        );
        options.clear();
      }*/
    }
  }

  void updateprofile() {

    userid = sharedPreferences.getString(StringsFile.UserID);
    token = sharedPreferences.getString(StringsFile.AccessToken);

    for (int i = 0; i < list.listResult.length; i++) {

      if (list.listResult.elementAt(i).questionType.name == "description") {
        if (DynamicTextControllers.elementAt(i).text != null) {
          post(list.listResult[i].id, DynamicTextControllers.elementAt(i).text, userid);
        }
      }

/*      else if(list.listResult.elementAt(i).questionType.name == "boolean"){

          for (int j = 0; j < list.listResult[i].questionSets.length; j++) {
            if (list.listResult[i].id == list.listResult[i].questionSets[j].questionId) {
              if(checkradio != null) {
              post(list.listResult[i].id, _picked.toString(), userid);
              print("pick $_picked");
            }
          }
        }
      }

      else if(list.listResult.elementAt(i).questionType.name == "options"){

           for (int j = 0; j < list.listResult[i].questionSets.length; j++) {
             if (list.listResult[i].id == list.listResult[i].questionSets[j].questionId) {
               if(checkcheckbox != null){
               post(list.listResult[i].id, _checked.toString(), userid);
             }
           }
          }
      }*/

    }
    Navigator.pop(context);

  }

  Future post(int id, String text, String userId) async {

    List<BasicProfileReq> res = [];
    BasicProfileReq basicProfileReq = new BasicProfileReq(questionId: id,text: text,userId: userId);
    res.add(basicProfileReq);

    final dio = Dio(); // Provide a dio instance
    dio.options.headers["Authorization"] = "Bearer " + token;
    //   dio.interceptors.add(element);
    final client = RestClient(dio);
    client.updateAnswers(res).then((responseanswers) {

      DynamicWidgets.clear();
      DynamicTextControllers.clear();


      Fluttertoast.showToast(
          msg:responseanswers.endUserMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 3,
          backgroundColor:Color(0xffff1493),
          textColor: Colors.white,
          fontSize: 16.0);



    }).catchError((Object obj) {
      print("error $obj.toString");
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;
          if (res.statusCode == 401) {
            Fluttertoast.showToast(
                msg:"UnAuthorized attempt",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor:Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);

            //  logger.e(
            //    "Got an Error 401: ${res.statusCode}-> ${res.statusMessage}");
          } else {
            Fluttertoast.showToast(
                msg:"Error Occured",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor:Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
            // logger.e("Got Error: ${res.statusCode}->${res.data}");
          }
          break;
        default:
      }
    });
    // client.getQuestions(userid)



  }




}






