// To parse this JSON data, do
//
//     final tipsResponsebyId = tipsResponsebyIdFromJson(jsonString);

import 'dart:convert';

TipsResponsebyId tipsResponsebyIdFromJson(String str) => TipsResponsebyId.fromJson(json.decode(str));

String tipsResponsebyIdToJson(TipsResponsebyId data) => json.encode(data.toJson());

class TipsResponsebyId {
  List<ListResult> listResult;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  dynamic links;
  List<dynamic> validationErrors;
  dynamic exception;

  TipsResponsebyId({
    this.listResult,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory TipsResponsebyId.fromJson(Map<String, dynamic> json) => TipsResponsebyId(
    listResult: json["listResult"] == null ? null : List<ListResult>.from(json["listResult"].map((x) => ListResult.fromJson(x))),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"],
    validationErrors: json["validationErrors"] == null ? null : List<dynamic>.from(json["validationErrors"].map((x) => x)),
    exception: json["exception"],
  );

  Map<String, dynamic> toJson() => {
    "listResult": listResult == null ? null : List<dynamic>.from(listResult.map((x) => x.toJson())),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links,
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x)),
    "exception": exception,
  };
}

class ListResult {
  int id;
  String title;
  List<TypesInfo> typesInfo;

  ListResult({
    this.id,
    this.title,
    this.typesInfo,
  });

  factory ListResult.fromJson(Map<String, dynamic> json) => ListResult(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    typesInfo: json["typesInfo"] == null ? null : List<TypesInfo>.from(json["typesInfo"].map((x) => TypesInfo.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "typesInfo": typesInfo == null ? null : List<dynamic>.from(typesInfo.map((x) => x.toJson())),
  };
}

class TypesInfo {
  int id;
  String title;
  String description;
  String fileName;
  String fileExtension;
  String fileLocation;
  int typeId;

  TypesInfo({
    this.id,
    this.title,
    this.description,
    this.fileName,
    this.fileExtension,
    this.fileLocation,
    this.typeId,
  });

  factory TypesInfo.fromJson(Map<String, dynamic> json) => TypesInfo(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    description: json["description"] == null ? null : json["description"],
    fileName: json["fileName"] == null ? null : json["fileName"],
    fileExtension: json["fileExtension"] == null ? null : json["fileExtension"],
    fileLocation: json["fileLocation"] == null ? null : json["fileLocation"],
    typeId: json["typeId"] == null ? null : json["typeId"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "description": description == null ? null : description,
    "fileName": fileName == null ? null : fileName,
    "fileExtension": fileExtension == null ? null : fileExtension,
    "fileLocation": fileLocation == null ? null : fileLocation,
    "typeId": typeId == null ? null : typeId,
  };
}
