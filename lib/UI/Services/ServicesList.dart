import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Response/GetServices.dart';

import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/UI/Services/ServicesDes.dart';
import 'package:womenes/Utils/Animations.dart';

import '../Account/Login.dart';


class ServicesCalling extends StatefulWidget {
  @override
  _ServicesCallingState createState() => _ServicesCallingState();
}

class _ServicesCallingState extends State<ServicesCalling> {
  Logger logger = Logger();
  SharedPreferences pref;
  final AsyncMemoizer _memoizer = AsyncMemoizer();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xffff1493),
          title: Text('Services'),
        ),
        body: FutureBuilder(
            future: _fetchData(),
            builder: (context, snapshot) {
              if(snapshot.connectionState == ConnectionState.done){
                if (snapshot.data != null) {
                  GetServices data = snapshot.data;


                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4.0),
                    child: ListView.builder(
                        //itemCount: 7, // data.listResult.length,
                      itemCount: data.listResult.length,
                        itemBuilder: (BuildContext context, int index) {
                          return FadeIn(3, Cards(data, index));
                        }),
                  );
                } else {
                  return Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.flash_on, color: Colors.yellow, size: 50,),
                        Text("Something Went Wrong !!", style: TextStyle(
                            color: Colors.black87, fontSize: 18),)
                      ],
                    ),
                  );
                }
              }else if (snapshot.connectionState == ConnectionState.waiting){return Center(child: CircularProgressIndicator(  valueColor: new AlwaysStoppedAnimation<Color>(Color(0xffff1493)),),);}
              else{
                return Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.flash_on,color: Colors.yellow,size: 50,),
                      Text("No Internet",style: TextStyle(color: Colors.black87,fontSize: 18),)
                    ],
                  ),
                );
              }
            }));
  }

  Widget Cards(GetServices data,int index){
      print( data.listResult[index].serviceRepositories.length);
      var logo = AssetImage('/assets/images/logo.png');
      var filename;
      var filelocation;
      var fileextension;

      try {
     filename = data.listResult[index].serviceRepositories.length == 0
        ? logo: data
        .listResult[index]
        .serviceRepositories[0]
        .fileName;
     filelocation = data.listResult[index].serviceRepositories.length == 0
        ? '': data
        .listResult[index]
        .serviceRepositories[0]
        .fileLocation;
      fileextension = data.listResult[index].serviceRepositories.length == 0
        ? '':data
        .listResult[index]
        .serviceRepositories[0]
        .fileExtension;
  }catch(e){
    print (e);
  }

    return  Card(
      color: Colors.white,
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: ListTile(
          onTap: () {Navigator.push(context, MaterialPageRoute(
                    builder: (context) => ServiceRating(
                        (data.listResult[index].serviceRepositories.length == 0 ?  ('$filelocation$filename$fileextension'):'/assets/images/logo.png'),
                        data.listResult[index].serviceName,
                        data.listResult[index].description,data.listResult.elementAt(index).id)));
          },
          leading: data.listResult[index].serviceRepositories.length == 0 ? CircleAvatar(
            backgroundImage:
                  AssetImage('/assets/images/logo.png'),
            radius: 40.0,
          ) : CircleAvatar(
            radius: 35.0,
            backgroundImage: NetworkImage('$filelocation$filename$fileextension'),
          ),
          title: Text(data.listResult[index].serviceName,
              style: TextStyle(
                fontSize: 16, color: Colors.black87,)),
          subtitle: Column(
            children: <Widget>[
              Text(
                data.listResult[index].description,maxLines: 2,overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 15, color: Colors.black45),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlutterRatingBar(
                    initialRating:
                    data.listResult[index].avgRating,
                    ignoreGestures: true,
                    itemSize: 20,
                    fillColor:Color(0xffff1493),
                    borderColor: Colors.grey,
                    allowHalfRating: true,
                    onRatingUpdate: (rating) {},
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }


  _fetchData() {
    return this._memoizer.runOnce(() async {
      return getData();
    });
  }


  Future<GetServices> getData() async {
    pref = await SharedPreferences.getInstance();

    final dio = Dio();
    GetServices services;
    final client = RestClient(dio);
    services = (await client.getService().then((res) {
      return res;
    }).catchError((Object obj) {
      print(obj);
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;

          if(res.statusCode == 401){
            pref.clear();
            pref.commit();
            Navigator.pushReplacement(context,MaterialPageRoute(builder: (context)=> LoginPage()) );
            Fluttertoast.showToast(
                msg: " Connection TimeOut..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor:Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }else if (res.statusCode == 500) {
            Fluttertoast.showToast(
                msg: "Failed to connect Server",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor:Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }
          else {
            Fluttertoast.showToast(
                msg: " Error Occured ..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor:Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }
          break;
        default:
      }
    }));


    return services;
  }
}
