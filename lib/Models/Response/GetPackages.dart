// To parse this JSON data, do
//
//     final packagesResponse = packagesResponseFromJson(jsonString);

import 'dart:convert';

PackagesResponse packagesResponseFromJson(String str) => PackagesResponse.fromJson(json.decode(str));

String packagesResponseToJson(PackagesResponse data) => json.encode(data.toJson());

class PackagesResponse {
  List<ListResult> listResult;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  List<Exception> links;
  List<ValidationError> validationErrors;
  Exception exception;

  PackagesResponse({
    this.listResult,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory PackagesResponse.fromJson(Map<String, dynamic> json) => PackagesResponse(
    listResult: json["listResult"] == null ? null : List<ListResult>.from(json["listResult"].map((x) => ListResult.fromJson(x))),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"] == null ? null : List<Exception>.from(json["links"].map((x) => Exception.fromJson(x))),
    validationErrors: json["validationErrors"] == null ? null : List<ValidationError>.from(json["validationErrors"].map((x) => ValidationError.fromJson(x))),
    exception: json["exception"] == null ? null : Exception.fromJson(json["exception"]),
  );

  Map<String, dynamic> toJson() => {
    "listResult": listResult == null ? null : List<dynamic>.from(listResult.map((x) => x.toJson())),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links == null ? null : List<dynamic>.from(links.map((x) => x.toJson())),
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x.toJson())),
    "exception": exception == null ? null : exception.toJson(),
  };
}

class Exception {
  Exception();

  factory Exception.fromJson(Map<String, dynamic> json) => Exception(
  );

  Map<String, dynamic> toJson() => {
  };
}

class ListResult {
  int id;
  String duration;
  String name;
  double totalAmount;
  int taxPercentage;
  String description;
  bool isActive;
  String fileName;
  String fileExtension;
  String fileLocation;
  double avgRating;
  String createdBy;
  String modifiedBy;
  DateTime createdDate;
  DateTime modifiedDate;
  int discountPercentageUptoOrFlatAmount;
  int discountTypeId;
  DiscountType discountType;
  String packageCode;
  List<PackageService> packageServices;

  ListResult({
    this.id,
    this.duration,
    this.name,
    this.totalAmount,
    this.taxPercentage,
    this.description,
    this.isActive,
    this.fileName,
    this.fileExtension,
    this.fileLocation,
    this.avgRating,
    this.createdBy,
    this.modifiedBy,
    this.createdDate,
    this.modifiedDate,
    this.discountPercentageUptoOrFlatAmount,
    this.discountTypeId,
    this.discountType,
    this.packageCode,
    this.packageServices,
  });

  factory ListResult.fromJson(Map<String, dynamic> json) => ListResult(
    id: json["id"] == null ? null : json["id"],
    duration: json["duration"] == null ? null : json["duration"],
    name: json["name"] == null ? null : json["name"],
    totalAmount: json["totalAmount"] == null ? null : json["totalAmount"],
    taxPercentage: json["taxPercentage"] == null ? null : json["taxPercentage"],
    description: json["description"] == null ? null : json["description"],
    isActive: json["isActive"] == null ? null : json["isActive"],
    fileName: json["fileName"] == null ? null : json["fileName"],
    fileExtension: json["fileExtension"] == null ? null : json["fileExtension"],
    fileLocation: json["fileLocation"] == null ? null : json["fileLocation"],
    avgRating: json["avgRating"] == null ? null : json["avgRating"],
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    modifiedBy: json["modifiedBy"] == null ? null : json["modifiedBy"],
    createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
    modifiedDate: json["modifiedDate"] == null ? null : DateTime.parse(json["modifiedDate"]),
    discountPercentageUptoOrFlatAmount: json["discountPercentageUptoOrFlatAmount"] == null ? null : json["discountPercentageUptoOrFlatAmount"],
    discountTypeId: json["discountTypeId"] == null ? null : json["discountTypeId"],
    discountType: json["discountType"] == null ? null : DiscountType.fromJson(json["discountType"]),
    packageCode: json["packageCode"] == null ? null : json["packageCode"],
    packageServices: json["packageServices"] == null ? null : List<PackageService>.from(json["packageServices"].map((x) => PackageService.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "duration": duration == null ? null : duration,
    "name": name == null ? null : name,
    "totalAmount": totalAmount == null ? null : totalAmount,
    "taxPercentage": taxPercentage == null ? null : taxPercentage,
    "description": description == null ? null : description,
    "isActive": isActive == null ? null : isActive,
    "fileName": fileName == null ? null : fileName,
    "fileExtension": fileExtension == null ? null : fileExtension,
    "fileLocation": fileLocation == null ? null : fileLocation,
    "avgRating": avgRating == null ? null : avgRating,
    "createdBy": createdBy == null ? null : createdBy,
    "modifiedBy": modifiedBy == null ? null : modifiedBy,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
    "modifiedDate": modifiedDate == null ? null : modifiedDate.toIso8601String(),
    "discountPercentageUptoOrFlatAmount": discountPercentageUptoOrFlatAmount == null ? null : discountPercentageUptoOrFlatAmount,
    "discountTypeId": discountTypeId == null ? null : discountTypeId,
    "discountType": discountType == null ? null : discountType.toJson(),
    "packageCode": packageCode == null ? null : packageCode,
    "packageServices": packageServices == null ? null : List<dynamic>.from(packageServices.map((x) => x.toJson())),
  };
}

class DiscountType {
  int id;
  String discounttype;

  DiscountType({
    this.id,
    this.discounttype,
  });

  factory DiscountType.fromJson(Map<String, dynamic> json) => DiscountType(
    id: json["id"] == null ? null : json["id"],
    discounttype: json["discounttype"] == null ? null : json["discounttype"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "discounttype": discounttype == null ? null : discounttype,
  };
}

class PackageService {
  int packageId;
  int serviceId;
  Service service;

  PackageService({
    this.packageId,
    this.serviceId,
    this.service,
  });

  factory PackageService.fromJson(Map<String, dynamic> json) => PackageService(
    packageId: json["packageId"] == null ? null : json["packageId"],
    serviceId: json["serviceId"] == null ? null : json["serviceId"],
    service: json["service"] == null ? null : Service.fromJson(json["service"]),
  );

  Map<String, dynamic> toJson() => {
    "packageId": packageId == null ? null : packageId,
    "serviceId": serviceId == null ? null : serviceId,
    "service": service == null ? null : service.toJson(),
  };
}

class Service {
  int id;
  String serviceName;
  String serviceCode;
  String description;
  double avgRating;
  String createdBy;
  String modifiedBy;
  DateTime createdDate;
  DateTime modifiedDate;
  int discountPercentageUptoOrFlatAmount;
  int discountTypeId;
  DiscountType discountType;
  double totalAmount;
  String duration;
  int taxPercentage;
  bool isActive;
  List<ServiceRepository> serviceRepositories;

  Service({
    this.id,
    this.serviceName,
    this.serviceCode,
    this.description,
    this.avgRating,
    this.createdBy,
    this.modifiedBy,
    this.createdDate,
    this.modifiedDate,
    this.discountPercentageUptoOrFlatAmount,
    this.discountTypeId,
    this.discountType,
    this.totalAmount,
    this.duration,
    this.taxPercentage,
    this.isActive,
    this.serviceRepositories,
  });

  factory Service.fromJson(Map<String, dynamic> json) => Service(
    id: json["id"] == null ? null : json["id"],
    serviceName: json["serviceName"] == null ? null : json["serviceName"],
    serviceCode: json["serviceCode"] == null ? null : json["serviceCode"],
    description: json["description"] == null ? null : json["description"],
    avgRating: json["avgRating"] == null ? null : json["avgRating"],
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    modifiedBy: json["modifiedBy"] == null ? null : json["modifiedBy"],
    createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
    modifiedDate: json["modifiedDate"] == null ? null : DateTime.parse(json["modifiedDate"]),
    discountPercentageUptoOrFlatAmount: json["discountPercentageUptoOrFlatAmount"] == null ? null : json["discountPercentageUptoOrFlatAmount"],
    discountTypeId: json["discountTypeId"] == null ? null : json["discountTypeId"],
    discountType: json["discountType"] == null ? null : DiscountType.fromJson(json["discountType"]),
    totalAmount: json["totalAmount"] == null ? null : json["totalAmount"],
    duration: json["duration"] == null ? null : json["duration"],
    taxPercentage: json["taxPercentage"] == null ? null : json["taxPercentage"],
    isActive: json["isActive"] == null ? null : json["isActive"],
    serviceRepositories: json["serviceRepositories"] == null ? null : List<ServiceRepository>.from(json["serviceRepositories"].map((x) => ServiceRepository.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "serviceName": serviceName == null ? null : serviceName,
    "serviceCode": serviceCode == null ? null : serviceCode,
    "description": description == null ? null : description,
    "avgRating": avgRating == null ? null : avgRating,
    "createdBy": createdBy == null ? null : createdBy,
    "modifiedBy": modifiedBy == null ? null : modifiedBy,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
    "modifiedDate": modifiedDate == null ? null : modifiedDate.toIso8601String(),
    "discountPercentageUptoOrFlatAmount": discountPercentageUptoOrFlatAmount == null ? null : discountPercentageUptoOrFlatAmount,
    "discountTypeId": discountTypeId == null ? null : discountTypeId,
    "discountType": discountType == null ? null : discountType.toJson(),
    "totalAmount": totalAmount == null ? null : totalAmount,
    "duration": duration == null ? null : duration,
    "taxPercentage": taxPercentage == null ? null : taxPercentage,
    "isActive": isActive == null ? null : isActive,
    "serviceRepositories": serviceRepositories == null ? null : List<dynamic>.from(serviceRepositories.map((x) => x.toJson())),
  };
}

class ServiceRepository {
  int id;
  int serviceId;
  String fileName;
  String fileExtension;
  String fileLocation;

  ServiceRepository({
    this.id,
    this.serviceId,
    this.fileName,
    this.fileExtension,
    this.fileLocation,
  });

  factory ServiceRepository.fromJson(Map<String, dynamic> json) => ServiceRepository(
    id: json["id"] == null ? null : json["id"],
    serviceId: json["serviceId"] == null ? null : json["serviceId"],
    fileName: json["fileName"] == null ? null : json["fileName"],
    fileExtension: json["fileExtension"] == null ? null : json["fileExtension"],
    fileLocation: json["fileLocation"] == null ? null : json["fileLocation"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "serviceId": serviceId == null ? null : serviceId,
    "fileName": fileName == null ? null : fileName,
    "fileExtension": fileExtension == null ? null : fileExtension,
    "fileLocation": fileLocation == null ? null : fileLocation,
  };
}

class ValidationError {
  String name;
  String description;

  ValidationError({
    this.name,
    this.description,
  });

  factory ValidationError.fromJson(Map<String, dynamic> json) => ValidationError(
    name: json["name"] == null ? null : json["name"],
    description: json["description"] == null ? null : json["description"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "description": description == null ? null : description,
  };
}
