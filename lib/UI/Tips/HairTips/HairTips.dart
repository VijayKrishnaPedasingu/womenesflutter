import 'package:flutter/material.dart';

import 'HairTipstabbar.dart';

class HairTips extends StatefulWidget {
  @override
  _HairTipsState createState() => _HairTipsState();
}

class _HairTipsState extends State<HairTips> {
  Color pink = Color(0xffff1493);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tips'), backgroundColor: pink),
      body: ListView(
        children: <Widget>[
          Card(
            elevation: 3.0,
            child: ListTile(
              leading: Icon(Icons.video_library, color: pink),
              title: Text('Dandruff'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HairTipstabbar(0)));
              },
            ),
          ),
          Card(
            elevation: 3.0,
            child: ListTile(
              leading: Icon(Icons.video_library, color: pink),
              title: Text('HairFall'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HairTipstabbar(1)));
              },
            ),
          ),
          Card(
            elevation: 3.0,
            child: ListTile(
                leading: Icon(Icons.video_library, color: pink),
                title: Text('Grey Hair'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => HairTipstabbar(2)));
                }),
          ),
          Card(
            elevation: 3.0,
            child: ListTile(
                leading: Icon(Icons.video_library, color: pink),
                title: Text('Lice'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => HairTipstabbar(3)));
                }),
          ),
          Card(
            elevation: 3.0,
            child: ListTile(
                leading: Icon(Icons.video_library, color: pink),
                title: Text('Split Ends'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => HairTipstabbar(4)));
                }),
          ),
          Card(
            elevation: 3.0,
            child: ListTile(
                leading: Icon(Icons.video_library, color: pink),
                title: Text('Damaged Hair'),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => HairTipstabbar(5)));
                }),
          ),
        ],
      ),
    );
  }
}
