import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/UI/Account/Login.dart';
import 'package:womenes/Utils/StringsFile.dart';

import '../../Models/Response/VisitSummaryByuserId.dart';
import '../../Services/RetrofitServices.dart';



final logger = Logger();

class VisitSummary extends StatefulWidget {

  @override


  _VisitSummaryState createState() => _VisitSummaryState();
}

class _VisitSummaryState extends State<VisitSummary> {


  SharedPreferences pref;
  var userid;
  var token;

  TextStyle textStyle = TextStyle(
      color: Color(0xFFF54291), fontSize: 18, fontWeight: FontWeight.w600);
  TextStyle contentStyle = TextStyle(
      color: Color(0xff757575), fontSize: 18, fontWeight: FontWeight.w500);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFFF54291),
          title: Text('Visit Summary'),
        ),


        body: FutureBuilder(
            future: getdatasummary(),
            builder: (context, snapshot) {
              VisitSummaryByuserId result = snapshot.data;
              if (snapshot.data != null) {
                return result.isSuccess == false ? Center(child: Text(result.endUserMessage,
                  style: TextStyle(fontSize: 30.0,color: Color(0xff757575)),),)
                    : ListView.builder(
                    shrinkWrap: true,
                    itemCount: result.listResult.length,
                    itemBuilder: (context, index) {
                      return Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Card(
                              color: Colors.white,
                              elevation: 3.0,
                              child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text("Name", style: textStyle,),
                                                Text("Age", style: textStyle,),
                                                Text("Bp", style: textStyle,),
                                                Text("Weight", style: textStyle,),
                                                Text("Visited Date", style: textStyle,),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(result.listResult[index].appointment.customerUser.userName, style: contentStyle,),
                                                Text(result.listResult[index].age.toString(), style: contentStyle,),
                                                Text(result.listResult[index].bp, style: contentStyle,),
                                                Text(result.listResult[index].weight, style: contentStyle),
                                                Text(result.listResult[index].visitedDate.toIso8601String().trim().substring(0,10),
                                                    style: contentStyle),



                                              ],
                                            ),
                                          ],
                                        ),
                                      ]
                                  ))));
                    });
              }
              else {
                return Center(child: CircularProgressIndicator( valueColor: new AlwaysStoppedAnimation<Color>(Color(0xffff1493)),));
              }
            }));
  }

  Future getdatasummary() async {

    pref = await SharedPreferences.getInstance().then((sp){
      token =  sp.getString(StringsFile.AccessToken);
      userid =  sp.getString(StringsFile.UserID);
      return sp;

    });

    final dio = Dio();
    dio.options.headers["Authorization"] = ("Bearer "+token);

    final client = RestClient(dio);
    VisitSummaryByuserId data = await client.getsummary(userid).then((response) {
      return response;
    }).catchError((Object obj) {
      print(obj);
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;

          if (res.statusCode == 401) {
            pref.clear();
            pref.commit();
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => LoginPage()));
            Fluttertoast.showToast(
                msg: " Connection TimeOut..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          } else if (res.statusCode == 500) {
            Fluttertoast.showToast(
                msg: "Failed to connect Server",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }
          else {
            Fluttertoast.showToast(
                msg: " Error Occured ..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }
          break;
        default:
      }
    });

    return data;
  }

}
