// To parse this JSON data, do
//
//     final feedbackResponse = feedbackResponseFromJson(jsonString);

import 'dart:convert';

FeedbackResponse feedbackResponseFromJson(String str) => FeedbackResponse.fromJson(json.decode(str));

String feedbackResponseToJson(FeedbackResponse data) => json.encode(data.toJson());

class FeedbackResponse {
  List<ListResult> listResult;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  List<Exception> links;
  List<ValidationError> validationErrors;
  Exception exception;

  FeedbackResponse({
    this.listResult,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory FeedbackResponse.fromJson(Map<String, dynamic> json) => FeedbackResponse(
    listResult: json["listResult"] == null ? null : List<ListResult>.from(json["listResult"].map((x) => ListResult.fromJson(x))),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"] == null ? null : List<Exception>.from(json["links"].map((x) => Exception.fromJson(x))),
    validationErrors: json["validationErrors"] == null ? null : List<ValidationError>.from(json["validationErrors"].map((x) => ValidationError.fromJson(x))),
    exception: json["exception"] == null ? null : Exception.fromJson(json["exception"]),
  );

  Map<String, dynamic> toJson() => {
    "listResult": listResult == null ? null : List<dynamic>.from(listResult.map((x) => x.toJson())),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links == null ? null : List<dynamic>.from(links.map((x) => x.toJson())),
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x.toJson())),
    "exception": exception == null ? null : exception.toJson(),
  };
}

class Exception {
  Exception();

  factory Exception.fromJson(Map<String, dynamic> json) => Exception(
  );

  Map<String, dynamic> toJson() => {
  };
}

class ListResult {
  int id;
  String parameter;

  ListResult({
    this.id,
    this.parameter,
  });

  factory ListResult.fromJson(Map<String, dynamic> json) => ListResult(
    id: json["id"] == null ? null : json["id"],
    parameter: json["parameter"] == null ? null : json["parameter"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "parameter": parameter == null ? null : parameter,
  };
}

class ValidationError {
  String name;
  String description;

  ValidationError({
    this.name,
    this.description,
  });

  factory ValidationError.fromJson(Map<String, dynamic> json) => ValidationError(
    name: json["name"] == null ? null : json["name"],
    description: json["description"] == null ? null : json["description"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "description": description == null ? null : description,
  };
}
