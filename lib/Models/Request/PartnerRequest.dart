// To parse this JSON data, do
//
//     final partnerRequset = partnerRequsetFromJson(jsonString);

import 'dart:convert';

PartnerRequset partnerRequsetFromJson(String str) => PartnerRequset.fromJson(json.decode(str));

String partnerRequsetToJson(PartnerRequset data) => json.encode(data.toJson());

class PartnerRequset {
  String firstName;
  String lastName;
  String email;
  String address;
  String phoneNumber;
  String message;

  PartnerRequset({
    this.firstName,
    this.lastName,
    this.email,
    this.address,
    this.phoneNumber,
    this.message,
  });

  factory PartnerRequset.fromJson(Map<String, dynamic> json) => PartnerRequset(
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    email: json["email"] == null ? null : json["email"],
    address: json["address"] == null ? null : json["address"],
    phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "email": email == null ? null : email,
    "address": address == null ? null : address,
    "phoneNumber": phoneNumber == null ? null : phoneNumber,
    "message": message == null ? null : message,
  };
}
