// To parse this JSON data, do
//
//     final loginRequest = loginRequestFromJson(jsonString);

import 'dart:convert';

LoginRequest loginRequestFromJson(String str) => LoginRequest.fromJson(json.decode(str));

String loginRequestToJson(LoginRequest data) => json.encode(data.toJson());

class LoginRequest {
  String clientId;
  String clientSecret;
  String scope;
  String userName;
  String password;

  LoginRequest({
    this.clientId,
    this.clientSecret,
    this.scope,
    this.userName,
    this.password,
  });

  factory LoginRequest.fromJson(Map<String, dynamic> json) => LoginRequest(
    clientId: json["clientId"] == null ? null : json["clientId"],
    clientSecret: json["clientSecret"] == null ? null : json["clientSecret"],
    scope: json["scope"] == null ? null : json["scope"],
    userName: json["userName"] == null ? null : json["userName"],
    password: json["password"] == null ? null : json["password"],
  );

  Map<String, dynamic> toJson() => {
    "clientId": clientId == null ? null : clientId,
    "clientSecret": clientSecret == null ? null : clientSecret,
    "scope": scope == null ? null : scope,
    "userName": userName == null ? null : userName,
    "password": password == null ? null : password,
  };
}
