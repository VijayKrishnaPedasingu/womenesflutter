import 'package:flutter/material.dart';
import 'package:womenes/UI/Tips/FaceTips/Pigmentation.dart';
import 'package:womenes/UI/Tips/FaceTips/acneandpimple.dart';

class FaceTipstabbar extends StatefulWidget {
  int indexs;

  FaceTipstabbar(this.indexs);

  @override
  _FaceTipstabbarState createState() => _FaceTipstabbarState(this.indexs);
}

class _FaceTipstabbarState extends State<FaceTipstabbar> {
  int indexs;

  _FaceTipstabbarState(this.indexs);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 11,
      initialIndex: indexs,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xffff1493),
          bottom: TabBar(
              isScrollable: true,
              indicatorWeight: 2.0,
              labelPadding: EdgeInsets.symmetric(horizontal: 9.0),
              indicatorColor: Colors.white,
              indicatorSize: TabBarIndicatorSize.label,
              tabs: [
                Tab(
                  text: "Acne&Pimple",
                ),
                Tab(
                  text: "Pigmentation",
                ),
                Tab(
                  text: "Pits&Openpores",
                ),
                Tab(
                  text: "BlackHeads&WhiteHeads",
                ),
                Tab(
                  text: "DarkLips",
                ),
                Tab(
                  text: "Wrinkles",
                ),
                Tab(
                  text: "Color Improvement",
                ),
                Tab(
                  text: "Dark Circles",
                ),
                Tab(
                  text: "Uneven skintone",
                ),
                Tab(
                  text: "Excessive or unwanted hair growth on face",
                ),
                Tab(
                  text: "Dark Neck",
                ),
              ]),
          title: Text('Face Tips'),
        ),
        body: TabBarView(children: [
          Acneandpimple(),
          Pigmentation(),
          Center(
            child: Text(
              "Tips will Update soon...",
              style: TextStyle(fontSize: 20.0, color: Color(0xffff1493)),
            ),
          ),
          Center(
            child: Text(
              "Tips will Update soon...",
              style: TextStyle(fontSize: 20.0, color: Color(0xffff1493)),
            ),
          ),
          Center(
            child: Text(
              "Tips will Update soon...",
              style: TextStyle(fontSize: 20.0, color: Color(0xffff1493)),
            ),
          ),
          Center(
            child: Text(
              "Tips will Update soon...",
              style: TextStyle(fontSize: 20.0, color: Color(0xffff1493)),
            ),
          ),
          Center(
            child: Text(
              "Tips will Update soon...",
              style: TextStyle(fontSize: 20.0, color: Color(0xffff1493)),
            ),
          ),
          Center(
            child: Text(
              "Tips will Update soon...",
              style: TextStyle(fontSize: 20.0, color: Color(0xffff1493)),
            ),
          ),
          Center(
            child: Text(
              "Tips will Update soon...",
              style: TextStyle(fontSize: 20.0, color: Color(0xffff1493)),
            ),
          ),
          Center(
            child: Text(
              "Tips will Update soon...",
              style: TextStyle(fontSize: 20.0, color: Color(0xffff1493)),
            ),
          ),
          Center(
            child: Text(
              "Tips will Update soon...",
              style: TextStyle(fontSize: 20.0, color: Color(0xffff1493)),
            ),
          ),
        ]),
      ),
    );
  }
}
