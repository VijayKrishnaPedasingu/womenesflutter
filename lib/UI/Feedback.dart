import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Request/feedbackpostrequest.dart';
import 'package:womenes/Models/Response/feedbackperametersresponse.dart';
import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/Utils/StringsFile.dart';
import 'HomePage.dart';
import 'package:fluttertoast/fluttertoast.dart';

class FeedbackPage extends StatefulWidget {
  @override
  _FeedbackPageState createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  List<double> feedback = [];
  List<int> parameterId = [];
//  List<FeedBack> values = [];

  SharedPreferences pref;
  bool showStars = true;

  Future _future;

  @override
  void initState() {
    _future = getData();
    super.initState();
  }

  Future getData() async {
    pref = await SharedPreferences.getInstance();
    var _token = pref.getString(StringsFile.AccessToken);

    final dio = Dio();
    final client = RestClient(dio);
    dio.options.headers["Authorization"] = ("Bearer " + _token);
    Feedbackresponse feedbackresponse;
    feedbackresponse = (await client.getFeedback().then((resp) {
      return resp;
    }).catchError((Object obj) {
      print(obj);
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;

          if (res.statusCode == 401) {
            pref.clear();
            pref.commit();
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => HomePage()));
            Fluttertoast.showToast(
                msg: " Connection TimeOut..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          } else if (res.statusCode == 500) {
            Fluttertoast.showToast(
                msg: "Failed to connect Server",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          } else {
            Fluttertoast.showToast(
                msg: " Error Occured ..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }
          break;
        default:
      }
    }));

    return feedbackresponse;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Feedback'),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: FutureBuilder(
                future: _future,
                builder: (context, snapshot) {
                  Feedbackresponse result = snapshot.data;
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.data != null) {
                      feedback.length = result.listResult.length;

                      parameterId.length = result.listResult.length;
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                              child: ListView.builder(
                                  itemCount: result.listResult.length,
                                  itemBuilder: (context, index) {
                                    return Column(
                                      children: <Widget>[
                                        Text(result.listResult[index].parameter,style: TextStyle(fontSize: 20),),
                                        FlutterRatingBar(
                                          initialRating: 0,
                                          ignoreGestures: false,
                                          itemSize: 35,
                                          fillColor: Color(0xffff1493),
                                          borderColor: Colors.grey,
                                          allowHalfRating: true,
                                          onRatingUpdate: (rating) {

                                            parameterId[index] =
                                                result.listResult[index].id;

                                            feedback[index] = rating;

                                          },
                                        ),
                                        Divider(
                                          endIndent: 10,
                                          indent: 10,
                                          height: 10,
                                        ),
                                      ],
                                    );
                                  }),
                            ),
                          ),
                          RaisedButton(
                              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              color: Color(0xffff1493),
                              child: Text(
                                "Submit",
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () async {
                                var userId = pref.getString(StringsFile.UserID);
                                var _token = pref.getString(StringsFile.AccessToken);
                                List<FeedBack> fb = [];
                                for(int i = 0 ; i< result.listResult.length;i++){
                                  var _feedback = FeedBack(rating: feedback[i].toString(),parameterId: parameterId[i].toString());
                                  fb.add(_feedback);
                                }
                                print(fb);
                                FeedbackPostRequest request =
                                new FeedbackPostRequest(receiverId: userId, feedBack:fb );

                                final dio = Dio();
                                dio.options.headers["Authorization"] = ("Bearer " + _token);

                                final client = RestClient(dio);


                                client.postFeedBack(request).then((resp) {

                                  Fluttertoast.showToast(
                                      msg:"Submitted Successfully ",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIos: 3,
                                      backgroundColor: Color(0xffff1493),
                                      textColor: Colors.white,
                                      fontSize: 16.0);

                                  Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => HomePage()));

                                }).catchError((Object obj) {
                                  print("mobin $obj");
                                  switch (obj.runtimeType) {
                                    case DioError:
                                      final res = (obj as DioError).response;

                                      if (res.statusCode == 401) {
                                        pref.clear();
                                        pref.commit();
                                        Navigator.pushReplacement(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => FeedbackPage()));
                                        Fluttertoast.showToast(
                                            msg: " Connection TimeOut..",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIos: 3,
                                            backgroundColor: Color(0xffff1493),
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      } else if (res.statusCode == 500) {
                                        Fluttertoast.showToast(
                                            msg: "Failed to connect Server",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIos: 3,
                                            backgroundColor: Color(0xffff1493),
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      } else if (res.statusCode == 400) {
                                        Fluttertoast.showToast(
                                            msg: "Please Give rating to all ",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIos: 3,
                                            backgroundColor: Color(0xffff1493),
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      }else {
                                        Fluttertoast.showToast(
                                            msg: " Error Occured ..",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIos: 3,
                                            backgroundColor: Color(0xffff1493),
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      }
                                  }
                                });
                              }),
                        ],
                      );
                    } else {
                      return Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.flash_on,
                              color: Colors.yellow,
                              size: 50,
                            ),
                            Text(
                              "Something Went Wrong",
                              style: TextStyle(
                                  color: Colors.black87, fontSize: 18),
                            )
                          ],
                        ),
                      );
                    }
                  } else if (snapshot.connectionState ==
                      ConnectionState.waiting) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    return Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.flash_on,
                            color: Colors.yellow,
                            size: 50,
                          ),
                          Text(
                            "No Internet",
                            style:
                            TextStyle(color: Colors.black87, fontSize: 18),
                          )
                        ],
                      ),
                    );
                  }
                }),
          ),

        ],
      ),
    );
  }
}
