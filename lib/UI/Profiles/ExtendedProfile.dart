
import 'dart:async';

import 'package:dio/dio.dart';

import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/Utils/StringsFile.dart';

import '../../Models/Response/GetQuestionsByUserId.dart';
import '../Account/Login.dart';

List<Widget> DynamicWidgets =  List<Widget>();
List<TextEditingController> DynamicTextControllers =  List<TextEditingController>();

class ExtendedProfile extends StatefulWidget {

  @override
  _ExtendedProfileState createState() => _ExtendedProfileState();
}

class _ExtendedProfileState extends State<ExtendedProfile> {

  SharedPreferences sharedPreferences;
  Future futuredata;

  @override
  void initState() {
    futuredata = getQuestionData();
    super.initState();
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Extended Profile")),
      body: FutureBuilder(
          future: futuredata,
          builder: (context,snapshot) {
            GetQuestions listresult = snapshot.data;
            if(snapshot.connectionState == ConnectionState.done) {
              if(snapshot.data != null) {
                dynamicWidgets(listresult);
                return SingleChildScrollView(child: Column(children: DynamicWidgets,));
              }
              else {
                return  Center(child: Text('Error Occured'));
              }
            }
            else if(snapshot.connectionState == ConnectionState.waiting){
              return Center(
                child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color> (Color(0xffff1493)),),);
            }
            else{
              return  Center(child: Text("No Internet"));
            }
          }
      ),
    );
  }



  Future getQuestionData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var userid  = pref.getString(StringsFile.UserID);
    var Token = pref.getString(StringsFile.AccessToken);
    DynamicWidgets.clear();
    //  await Future.delayed(const Duration(milliseconds: 3000));

    final dio = Dio(); // Provide a dio instance
    dio.options.headers["Authorization"] = "Bearer "+Token;
    //   dio.interceptors.add(element);
    final client = RestClient(dio);
    GetQuestions response = await client.getQuestions(userid).then((questionanswers){
      return questionanswers;

    }).catchError((Object obj) {
      print(obj.runtimeType.toString());
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;
          if (res.statusCode == 401) {
            logger.e("Got error 401 : ${res.statusCode} -> ${res
                .statusMessage}");
          }
          else {
            logger.e("Got error : ${res.statusCode} -> ${res
                .statusMessage}");
          }
          break;
        default:
      }
    });
    // client.getQuestions(userid)


    return response;

  }

  void dynamicWidgets(GetQuestions questionanswers) {
    for(int i=0 ; i<questionanswers.listResult.length;i++){
      DynamicWidgets.add(Container(width:double.infinity,margin:EdgeInsets.only(right: 20,left:20,top: 10)
          ,child: new prefix0.Text(questionanswers.listResult.elementAt(i).header.toString(),
            style: TextStyle(fontSize: 18),textAlign: TextAlign.start,)));
      if(questionanswers.listResult.elementAt(i).questionType.name=="description") {
        TextEditingController textEditingController = new TextEditingController();

        for (int j =0;j<questionanswers.listResult.elementAt(i).userAnswers.length ;j++){
          questionanswers.listResult.elementAt(i).userAnswers == null ?  null:  textEditingController.text = questionanswers.listResult.elementAt(i).userAnswers.elementAt(j).text;
        }

        DynamicWidgets.add(Container(width:double.infinity,margin:EdgeInsets.only(right: 20,left:20,top: 10 ),
            child:new TextFormField(
                enabled: false,
                controller: textEditingController, style:TextStyle(fontSize: 18),textAlign: TextAlign.start,
                decoration: InputDecoration(
                  //labelText: questionanswers.listResult.elementAt(i).header.toString(),
                    hintText: "Answers",border: OutlineInputBorder(borderSide: BorderSide())
                )
            )
        )
        );

      }else if(questionanswers.listResult.elementAt(i).questionType.name.contains("radio")){
        DynamicWidgets.add((new Checkbox(value:true, onChanged: null)));

      }}

  }


}

