// To parse this JSON data, do
//
//     final changePasswordResponse = changePasswordResponseFromJson(jsonString);

import 'dart:convert';

ChangePasswordResponse changePasswordResponseFromJson(String str) => ChangePasswordResponse.fromJson(json.decode(str));

String changePasswordResponseToJson(ChangePasswordResponse data) => json.encode(data.toJson());

class ChangePasswordResponse {
  Exception result;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  List<Exception> links;
  List<ValidationError> validationErrors;
  Exception exception;

  ChangePasswordResponse({
    this.result,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory ChangePasswordResponse.fromJson(Map<String, dynamic> json) => ChangePasswordResponse(
    result: json["result"] == null ? null : Exception.fromJson(json["result"]),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"] == null ? null : List<Exception>.from(json["links"].map((x) => Exception.fromJson(x))),
    validationErrors: json["validationErrors"] == null ? null : List<ValidationError>.from(json["validationErrors"].map((x) => ValidationError.fromJson(x))),
    exception: json["exception"] == null ? null : Exception.fromJson(json["exception"]),
  );

  Map<String, dynamic> toJson() => {
    "result": result == null ? null : result.toJson(),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links == null ? null : List<dynamic>.from(links.map((x) => x.toJson())),
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x.toJson())),
    "exception": exception == null ? null : exception.toJson(),
  };
}

class Exception {
  Exception();

  factory Exception.fromJson(Map<String, dynamic> json) => Exception(
  );

  Map<String, dynamic> toJson() => {
  };
}

class ValidationError {
  String name;
  String description;

  ValidationError({
    this.name,
    this.description,
  });

  factory ValidationError.fromJson(Map<String, dynamic> json) => ValidationError(
    name: json["name"] == null ? null : json["name"],
    description: json["description"] == null ? null : json["description"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "description": description == null ? null : description,
  };
}
