import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Request/BaseProfileRequest.dart';
import 'package:womenes/Models/Response/BaseProfileResponse.dart';
import 'package:womenes/Models/Response/GetBaseProfileRes.dart';
import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/UI/Account/Login.dart';
import 'package:womenes/Utils/StringsFile.dart';

final logger = Logger();

class BaseProfile extends StatefulWidget {
  @override
  _BaseProfileState createState() => _BaseProfileState();
}

class _BaseProfileState extends State<BaseProfile> {
  /*FocusNode _focusNode = new FocusNode();
  TextEditingController _firstname = new TextEditingController();
  TextEditingController _lastname = new TextEditingController();
  TextEditingController _phonenumber = new TextEditingController();
  TextEditingController _email = new TextEditingController();
  TextEditingController _username = new TextEditingController();
  final AsyncMemoizer _memoizer = AsyncMemoizer();

  SharedPreferences sharedPreferences;
  var token;
  var name, location, extension, userid;

  final formkey = GlobalKey<FormState>();
  GlobalKey keys = GlobalKey();
  bool autovalidate = false;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Profile")),
        body: FutureBuilder(
            future: _fetchData(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.data != null) {
                  GetBaseProfileRes baseProfileRes = snapshot.data;

                  _username.text = baseProfileRes.result.userName;
                  _firstname.text = baseProfileRes.result.firstName;
                  _lastname.text = baseProfileRes.result.lastName;
                  _phonenumber.text = baseProfileRes.result.phoneNumber;
                  _email.text = baseProfileRes.result.email;

                  return SingleChildScrollView(
                    child:  Form(
                    key:  formkey,
                      autovalidate: true,

                      child: Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(right: 20.0, left: 20.0),
                        child: Column(
                          children: <Widget>[
                            name == null
                                ? CircleAvatar(
                                    backgroundImage:
                                        AssetImage('assets/images/logo.png'),
                                    radius: 50.0,
                                  )
                                : CircleAvatar(
                                    backgroundImage: NetworkImage(
                                        baseProfileRes.result.fileLocation +
                                            baseProfileRes.result.fileName +
                                            baseProfileRes.result.fileExtension),
                                    radius: 50.0,
                                  ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: TextFormField(
                                controller: _username,
                                decoration: InputDecoration(
                                    labelText: "User Name",
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide())),
                                enabled: false,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: TextFormField(
                                controller: _firstname,
                                textInputAction: TextInputAction.done,
                                onFieldSubmitted: (value){

                                  setState(() {
                                    _firstname.text = value;
                                  });

                                },
                                decoration: InputDecoration(
                                    labelText: "First Name",
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide())),
                                validator: validatefirstname,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: TextFormField(
                                controller: _lastname,
                                decoration: InputDecoration(
                                    labelText: "Last Name",
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide())),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: TextFormField(
                                controller: _phonenumber,
                                decoration: InputDecoration(
                                    labelText: "Phone Number",
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide())),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: TextFormField(
                                controller: _email,
                                decoration: InputDecoration(
                                    labelText: "Email",
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide())),
                                enabled: false,
                              ),
                            ),
                            RaisedButton(
                              child: Text(
                                "Update",
                                style: TextStyle(fontSize: 20),
                              ),
                              color: Color(0xffff1493),
                              textColor: Colors.white,
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0),
                              ),
                              onPressed: () {
              if(formkey.currentState.validate()){
              validateInputs();
              UpdateProfile();
              }

                              },
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                } else {
                  return Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.flash_on,
                          color: Colors.yellow,
                          size: 50,
                        ),
                        Text(
                          "Something Went Wrong !!",
                          style: TextStyle(color: Colors.black87, fontSize: 18),
                        )
                      ],
                    ),
                  );
                }
              } else if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Color(0xffff1493)),
                  ),
                );
              } else {
                return Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.flash_on,
                        color: Colors.yellow,
                        size: 50,
                      ),
                      Text(
                        "No Internet",
                        style: TextStyle(color: Colors.black87, fontSize: 18),
                      )
                    ],
                  ),
                );
              }


            }));
  }

  _fetchData() {
    return this._memoizer.runOnce(() async {
      return getBaseProfile();
    });
  }

  Future getBaseProfile() async {
    sharedPreferences = await SharedPreferences.getInstance().then((sp) {
      token = sp.getString(StringsFile.AccessToken);
      userid = sp.get(StringsFile.UserID);
      return sp;
    });

    //  print(token);
//    print(userid);
    final dio = Dio();
    dio.options.headers["Authorization"] = ("Bearer " + token);

    final client = RestClient(dio);
    GetBaseProfileRes res =
        await client.getBaseProfile(userid).then((response) {
      logger.d(response);
      return response;
    }).catchError((Object obj) {
      logger.d(obj);
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;

          if (res.statusCode == 401) {
            *//* sharedPreferences.clear();
            sharedPreferences.commit();*//*
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => LoginPage()));
            Fluttertoast.showToast(
                msg: " Connection TimeOut..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          } else if (res.statusCode == 500) {
            Fluttertoast.showToast(
                msg: "Failed to connect Server",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          } else {
            Fluttertoast.showToast(
                msg: " Error Occured ..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }
          break;
        default:
      }
    });

    return res;
  }*/

  TextEditingController _firstname = new TextEditingController();
  TextEditingController _lastname = new TextEditingController();
  TextEditingController _phonenumber = new TextEditingController();
  TextEditingController _Email = new TextEditingController();
  TextEditingController _username = new TextEditingController();


  SharedPreferences sharedPreferences;
  var token,userid;
  var name,location,extension;

  @override
  Widget build(BuildContext context) {
  return Scaffold(
  appBar: AppBar(title: Text("BaseProfile")),
  body: SingleChildScrollView(
  scrollDirection: Axis.vertical,
  child: Container(width: double.infinity,margin: EdgeInsets.only(right: 20.0,left: 20.0,top:30.0),
    child: Column(
    children: <Widget>[
      name == null
          ? CircleAvatar(
        backgroundImage:
        AssetImage('assets/images/logo.png'),
        radius: 50.0,
      )
          : CircleAvatar(
        backgroundImage: NetworkImage(
           location+name+extension),
        radius: 50.0,
      ),
    Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: TextField(controller: _username,decoration: InputDecoration(labelText: "UserName",border:OutlineInputBorder(borderSide: BorderSide()) ),),
    ),
    Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: TextField(controller: _firstname,decoration: InputDecoration(labelText: "FirstName",border:OutlineInputBorder(borderSide: BorderSide())),),
    ),
    Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: TextField(controller: _lastname,decoration: InputDecoration(labelText: "LastName",border:OutlineInputBorder(borderSide: BorderSide())),),
    ),
    Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: TextField(controller: _phonenumber,decoration: InputDecoration(labelText: "PhoneNumber",border:OutlineInputBorder(borderSide: BorderSide())),),
    ),
    Padding(
      padding: const EdgeInsets.only(top:8.0),
      child: TextField(controller: _Email,decoration: InputDecoration(labelText: "Email",border:OutlineInputBorder(borderSide: BorderSide())),),
    ),

    Padding(padding: const EdgeInsets.only(left: 25,right: 25,top: 10,bottom: 10),
      child: RaisedButton(child: Text("Update", style: TextStyle(fontSize: 20),),
        color: Color(0xffff1493),
        textColor: Colors.white,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0),
        ),
    onPressed: () {
        UpdateProfile();
    }
      ),
    )



    ],



    ),
  ),
  ),




  );
  }

  @override
  void initState() {
    getString();
  }


  Future getString() async {
  sharedPreferences = await SharedPreferences.getInstance();
  _firstname.text= await sharedPreferences.getString(StringsFile.FirstName);
  _lastname.text = await sharedPreferences.getString(StringsFile.LastName);
  _Email.text = await sharedPreferences.getString(StringsFile.Email);
  _phonenumber.text = await sharedPreferences.getString(StringsFile.PhoneNumber);
  _username.text =await sharedPreferences.getString(StringsFile.Username);
   token = await sharedPreferences.getString(StringsFile.AccessToken);
   userid = await sharedPreferences.getString(StringsFile.UserID);
   name = await sharedPreferences.getString(StringsFile.Filename);
   location = await sharedPreferences.getString(StringsFile.FileLocation);
   extension = await sharedPreferences.getString(StringsFile.FileExtenstion);


  }




  Future<BaseProfileRes> UpdateProfile() async {

    String fname = _firstname.text;
    String lname = _lastname.text;
    String phno = _phonenumber.text;

    BaseProfileReq req = new BaseProfileReq(
        firstName: fname,
        lastName: lname,
        phoneNumber: phno,
        aspNetUserId: userid);

    final dio = Dio();
    dio.options.headers["Authorization"] = ("Bearer " + token);
    final client = RestClient(dio);
    client.updateProfile(req).then((res) async {


       await sharedPreferences.setString(StringsFile.FirstName,_firstname.text);
     await sharedPreferences.setString(StringsFile.LastName,_lastname.text);
    await sharedPreferences.setString(StringsFile.PhoneNumber,_phonenumber.text);
       Fluttertoast.showToast(
           msg:res.endUserMessage,
           toastLength: Toast.LENGTH_SHORT,
           gravity: ToastGravity.BOTTOM,
           timeInSecForIos: 3,
           backgroundColor:Color(0xffff1493),
           textColor: Colors.white,
           fontSize: 16.0);
    /*  Fluttertoast.showToast(
          msg: res.endUserMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 3,
          backgroundColor: Colors.grey,
          textColor: Colors.white);*/
      Navigator.pop(context);
    }).catchError((Object obj) {
      print("error $obj.toString");
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;
          if (res.statusCode == 401) {
            Fluttertoast.showToast(
                msg:"UnAuthorized attempt",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor:Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);

          //  logger.e(
            //    "Got an Error 401: ${res.statusCode}-> ${res.statusMessage}");
          } else {
            Fluttertoast.showToast(
                msg:"Error Occured",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor:Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
           // logger.e("Got Error: ${res.statusCode}->${res.data}");
          }
          break;
        default:
      }
    });
  }



  }

