import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:html/dom.dart' as dom;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Response/ApplyGoddess.dart';
import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/UI/Account/Login.dart';
import 'package:womenes/Utils/StringsFile.dart';



class Goddess extends StatefulWidget {
  @override
  _GoddessState createState() => _GoddessState();
}

class _GoddessState extends State<Goddess> {
  bool checked = false;


  String token;
  SharedPreferences pref;
  String userId;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPrefs();
  }




  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Apply for Goddess Awards",
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          backgroundColor: Color(0xffff1493),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Center(child: Text("Apply as Goddess ", style: TextStyle(fontSize: 22,),)),
                SizedBox(height: 15,),
                Container(
                  decoration: BoxDecoration(border: Border.all(color: Color(0xffff1493),width: 2,),borderRadius: BorderRadius.circular(20)),
                  child: Html(
                    data: """
                    
                      <h5> Our Mission</h5>      \n \n
                
                       <ul >
                    <li>Women is the center of the family. If she gets educated about health and nutrition then the family gets benifited and eventually society gets better.</li>\n \n
                    <li>Irrespective of the status or a degree most of the women in the society ignore about health and nutrition values.</li>\n \n
                    <li>Most of the time they are focused on quantity but not on quality of nutrients, life style, physical activity, emotional, psychological etc. Same thing is transferred to family and society and ultimately a most of the preventable diseases become epidemic.</li>\n \n
                    <li>OWT nonprofit foundation mission is to create awareness among women about health,beauty and nutrition.</li>
        </ul> \n \n \n \n
        <h5>Goddess Awards Event</h5>   \n \n

        <ul>
        <li>Awards shall be conducted quarterly.</li> \n \n
        <li>Grand Final on 8th March of every year.</li> \n \n
        <li>Criteria: judged based on women health and nutrition along with beauty.</li> \n \n
        <li>Member of Jury: Doctor,Cosmetologist &amp; Any reputable personalities.</li> \n \n
        <li>Advertisement</li> \n \n
        <li>TV Program</li> \n \n
        <li>Radio Partner</li> \n \n
        <li>Digital/Direct marketing</li> \n \n
        <li>Website( www.GoddessAwards.in)</li> \n \n
        <li>TV Program Planning - \'Goddess Womeness\' show</li> \n \n
        <li>30 minutes slot with City Cable in the beginning.</li> \n \n
        <li>A beauty/health/nutrition program participated by panel of professionals.</li> \n \n
        <li>Involving/interviewing goddess awards participants on the show.</li> \n \n
        <li>Sponsors mentioned in the program.</li> \n \n
        <li>Two day program</li> \n \n
        <li>3rd and 4th august , 2019 ( sat and sun )</li> \n \n
        <li>venue : sesha sai kalyana vedika , vijayawada</li> \n \n
        <li>more than 2000 people - expected everyday</li> \n \n
        <li>counseling centre related to health by doctors</li> \n \n
        <li>live tv coverage</li> \n \n
        <li>print and digital media coverage</li> \n \n
        <li>parcitipation of women organizations</li> \n \n
        <li>web live stream on youtube , facebook and <b>www.GoddessAward.in </b>- website</li> \n \n
        <li>live updates on instagram , tiktok etc with sponsors names</li> \n \n
       <li>email communication to women paricipants with sponsors names and explaining about their products or services</li> \n \n \n \n
       </ul>
         <h5> Objective &amp; Target Participants </h5>  

        <ul>

        <li>Location: Amaravathi Capital City (Surrounding)</li>  
        <li>Target Participants: Women above the age of 18 are eligible to participate.</li> 
        <li>hey shall be segregated into different categories based on their age.</li>  
        <li>Category One -18 to 30 years of age</li> 
        <li>Category Two -31 to 40 years of age</li> 
        <li>Category Three -41 to 50 years of age</li>  
        <li>Category Four -51 and above years of age</li>  
        <li>Three prizes in each category defining first,second and third positions.</li>  

       </ul>


        <h5> Sponsorship Packages </h5>   \n \n

        <ul >

        <li >platinum - Rs.6,00,000 Goddess award sponsored by - one sponsor</li>  \n \n
        <li>diamond - Rs.1,00,000 Ramp side banners - 4 sponsors</li>  \n \n
        <li>Gold( Rs.60,000 to 1,00,000 ) Prize sponsors - 12 sponsors(First prize Rs.1,00,000 , second prize Rs.80,000 , third Prize Rs.60,000)</li>  \n \n
        <li>silver - Rs.25,000 continuous display of LED Advertisement-20 sponsors</li>  \n \n
        <li>Copper - Rs.10,000 Banners (5 * 3), Stalls (6 * 6) - 100 sponsors</li>

        </ul>

         """,
                    padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                  ),
                ),
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Checkbox(
                      value: checked,

                      activeColor:Color(0xffff1493),
                      onChanged: (value){

                        setState(() {
                          checked = value;
                        });
                      },
                    ),
                    Text(" I accept T&C to Apply as GODDESS",style: TextStyle(fontSize: 12),),


                  ],
                ),
                SizedBox(height: 10,),
                RaisedButton(
                  padding: EdgeInsets.symmetric(horizontal: 50, vertical: 12),
                  onPressed: () async {
                    if(checked == true){
                      print(checked );

                      final dio = Dio();
                      dio.options.headers["Authorization"] = ("Bearer "+token);
                      final client = RestClient(dio);



                      await client.applyGoddess(userId, false,checked).then((res) async {
                        print(res.isSuccess);

                        Fluttertoast.showToast(
                            msg: (res.isSuccess == true)?"Successfully Applied":" Failed",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIos: 2,
                            backgroundColor:Color(0xffff1493),
                            textColor:Colors.white,
                            fontSize: 16.0
                        );
                        Navigator.pop(context);
                        return res;
                      }).catchError((Object obj){

                        switch(obj.runtimeType){

                          case DioError:
                            final res = (obj as DioError).response;
                            if(res.statusCode == 401){
                              pref.clear();
                              pref.commit();
                              Navigator.pushReplacement(context,MaterialPageRoute(builder: (context)=> LoginPage()) );
                            }
                            else if(res.statusCode == 500){
                              Fluttertoast.showToast(
                                  msg: " Failed To Apply",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 2,
                                  backgroundColor: Color(0xffff1493),
                                  textColor:Colors.white,
                                  fontSize: 16.0
                              );
                            }
                            else{
                              Fluttertoast.showToast(
                                  msg: "Error",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 2,
                                  backgroundColor: Color(0xffff1493),
                                  textColor:Colors.white,
                                  fontSize: 16.0
                              );



                            }
                        }

                      });



                    }else{
                      Fluttertoast.showToast(
                          msg: "Please Accept T&c",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIos: 2,
                          backgroundColor: Colors.white,
                          textColor: Color(0xffff1493),
                          fontSize: 16.0
                      );

                    }

                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  color: Color(0xffff1493),
                  child: Text(
                    "Submit ",
                    style: TextStyle(color: Colors.white),
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }



  Future getPrefs() async {
    pref = await SharedPreferences.getInstance();
    token = await pref.getString(StringsFile.AccessToken);
    userId = await pref.getString(StringsFile.UserID);
  }
}

