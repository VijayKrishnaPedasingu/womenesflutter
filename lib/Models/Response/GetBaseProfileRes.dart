// To parse this JSON data, do
//
//     final getBaseProfileRes = getBaseProfileResFromJson(jsonString);

import 'dart:convert';

GetBaseProfileRes getBaseProfileResFromJson(String str) => GetBaseProfileRes.fromJson(json.decode(str));

String getBaseProfileResToJson(GetBaseProfileRes data) => json.encode(data.toJson());

class GetBaseProfileRes {
  Result result;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  dynamic links;
  List<dynamic> validationErrors;
  dynamic exception;

  GetBaseProfileRes({
    this.result,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory GetBaseProfileRes.fromJson(Map<String, dynamic> json) => GetBaseProfileRes(
    result: json["result"] == null ? null : Result.fromJson(json["result"]),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"],
    validationErrors: json["validationErrors"] == null ? null : List<dynamic>.from(json["validationErrors"].map((x) => x)),
    exception: json["exception"],
  );

  Map<String, dynamic> toJson() => {
    "result": result == null ? null : result.toJson(),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links,
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x)),
    "exception": exception,
  };
}

class Result {
  String firstName;
  String lastName;
  String userName;
  String phoneNumber;
  String email;
  String profilePicUrl;
  String roleId;
  String roleName;
  String referalCode;
  int referalBonus;
  bool areYouStudent;
  bool areYouGoddess;
  int id;
  String aspNetUserId;
  String fileName;
  String fileExtension;
  String fileLocation;

  Result({
    this.firstName,
    this.lastName,
    this.userName,
    this.phoneNumber,
    this.email,
    this.profilePicUrl,
    this.roleId,
    this.roleName,
    this.referalCode,
    this.referalBonus,
    this.areYouStudent,
    this.areYouGoddess,
    this.id,
    this.aspNetUserId,
    this.fileName,
    this.fileExtension,
    this.fileLocation,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    userName: json["userName"] == null ? null : json["userName"],
    phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
    email: json["email"] == null ? null : json["email"],
    profilePicUrl: json["profilePicUrl"] == null ? null : json["profilePicUrl"],
    roleId: json["roleId"] == null ? null : json["roleId"],
    roleName: json["roleName"] == null ? null : json["roleName"],
    referalCode: json["referalCode"] == null ? null : json["referalCode"],
    referalBonus: json["referalBonus"] == null ? null : json["referalBonus"],
    areYouStudent: json["are_You_Student"] == null ? null : json["are_You_Student"],
    areYouGoddess: json["are_You_Goddess"] == null ? null : json["are_You_Goddess"],
    id: json["id"] == null ? null : json["id"],
    aspNetUserId: json["aspNetUserId"] == null ? null : json["aspNetUserId"],
    fileName: json["fileName"] == null ? null : json["fileName"],
    fileExtension: json["fileExtension"] == null ? null : json["fileExtension"],
    fileLocation: json["fileLocation"] == null ? null : json["fileLocation"],
  );

  Map<String, dynamic> toJson() => {
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "userName": userName == null ? null : userName,
    "phoneNumber": phoneNumber == null ? null : phoneNumber,
    "email": email == null ? null : email,
    "profilePicUrl": profilePicUrl == null ? null : profilePicUrl,
    "roleId": roleId == null ? null : roleId,
    "roleName": roleName == null ? null : roleName,
    "referalCode": referalCode == null ? null : referalCode,
    "referalBonus": referalBonus == null ? null : referalBonus,
    "are_You_Student": areYouStudent == null ? null : areYouStudent,
    "are_You_Goddess": areYouGoddess == null ? null : areYouGoddess,
    "id": id == null ? null : id,
    "aspNetUserId": aspNetUserId == null ? null : aspNetUserId,
    "fileName": fileName == null ? null : fileName,
    "fileExtension": fileExtension == null ? null : fileExtension,
    "fileLocation": fileLocation == null ? null : fileLocation,
  };
}
