// To parse this JSON data, do
//
//     final serviceRatingRequest = serviceRatingRequestFromJson(jsonString);

import 'dart:convert';

ServiceRatingRequest serviceRatingRequestFromJson(String str) => ServiceRatingRequest.fromJson(json.decode(str));

String serviceRatingRequestToJson(ServiceRatingRequest data) => json.encode(data.toJson());

class ServiceRatingRequest {
  int serviceId;
  double rating;

  ServiceRatingRequest({
    this.serviceId,
    this.rating,
  });

  factory ServiceRatingRequest.fromJson(Map<String, dynamic> json) => ServiceRatingRequest(
    serviceId: json["serviceId"] == null ? null : json["serviceId"],
    rating: json["rating"] == null ? null : json["rating"],
  );

  Map<String, dynamic> toJson() => {
    "serviceId": serviceId == null ? null : serviceId,
    "rating": rating == null ? null : rating,
  };
}
