import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Models/Response/ReceiptResponse.dart';
import '../Services/RetrofitServices.dart';
import '../Utils/StringsFile.dart';



final logger = Logger();

class Receipt extends StatefulWidget {

  @override


  _ReceiptState createState() => _ReceiptState();
}

class _ReceiptState extends State<Receipt> {


  SharedPreferences pref;
  var userid;
  var token;

  TextStyle textStyle = TextStyle(
      color: Color(0xFFF54291), fontSize: 16, fontWeight: FontWeight.w600);
  TextStyle contentStyle = TextStyle(
      color: Color(0xff757575), fontSize: 16, fontWeight: FontWeight.w500);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFFF54291),
          title: Text('Receipt'),
        ),


        body: FutureBuilder(
            future: getdata(),
            builder: (context, snapshot) {
              ReceiptResponse result = snapshot.data;
              if (snapshot.data != null) {

                return result.isSuccess == false ? Center(child: Text(result.endUserMessage,
                  style: TextStyle(fontSize: 30.0,color: Color(0xff757575)),),)
                    : ListView.builder(
                    shrinkWrap: true,
                    itemCount: result.listResult.length,
                    itemBuilder: (context, index) {
                      return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2.0),
                          child: Card(
                              color: Colors.white,
                              elevation: 3.0,
                              child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment
                                              .spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Icon(Icons.assignment,
                                                  color: Color(0xFFF54291),),
                                                Text("Receipt",
                                                  style: TextStyle(color: Color(
                                                      0xFFF54291)),),
                                              ],
                                            ),

                                            Row(
                                              children: <Widget>[
                                                Text("ReceiptID :",
                                                    style: contentStyle),
                                                Text(
                                                  result.listResult[index].id.toString(),
                                                  style: textStyle,),

                                              ],
                                            )
                                          ],),
                                        SizedBox(
                                          height: 15.0,
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  "Name", style: contentStyle,),
//                                                Text("Receipt Id",
//                                                  style: contentStyle,),
                                                Text("Purchased Minutes",
                                                  style: contentStyle,),
                                                Text("Used Minutes",
                                                  style: contentStyle,),
                                                Text("Remaining Minutes",
                                                  style: contentStyle,),
                                                Text("Discount%/FlatAmount",
                                                  style: contentStyle,),
                                                Text("Total Amount",
                                                  style: contentStyle,),
                                                Text("AmountPaid",
                                                  style: contentStyle,),
                                                Text("Remaining Amount",
                                                  style: contentStyle,),
                                                Text("Paid Date",
                                                  style: contentStyle,),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[

                                                Text(result.listResult[index].userPayment.userPorS.visitInfo.appointment.customerUser.userName, style: contentStyle,),
                                                //  Text(result.listResult[index].id.toString(), style: contentStyle,),
                                                Text(result.listResult[index].userPayment.userPorS.purchasedHours.toString() ,style: contentStyle,),
                                                Text(result.listResult[index].userPayment.usedHours.toString(), style: contentStyle,),
                                                Text(result.listResult[index].userPayment.remainingHours.toString(), style: contentStyle,),
                                                Text(result.listResult[index].userPayment.userPorS.packServicesX[0].service.discountPercentageUptoOrFlatAmount.toString(), style: contentStyle,),
                                                Text(
                                                  "Rs ${result.listResult[index]
                                                      .userPayment.grossAmount
                                                      .toString()}",
                                                  style: contentStyle,),
                                                Text(
                                                  "Rs ${result.listResult[index]
                                                      .userPayment.amountPaid
                                                      .toString()}",
                                                  style: contentStyle,),
                                                Text(
                                                  "Rs ${result.listResult[index]
                                                      .userPayment.dueBalance
                                                      .toString()}",
                                                  style: contentStyle,),
                                                Text(result.listResult[index].userPayment.amountPaidDate.toIso8601String().trim().substring(0,10),style: contentStyle,),

                                              ],
                                            ),
                                          ],
                                        ),
                                      ]
                                  ))));
                    });
              }
              else {
                return Center(child: CircularProgressIndicator(valueColor:
                new AlwaysStoppedAnimation<Color>(Color(0xffff1493))));
              }
            }));
  }

  Future getdata() async {

    pref = await SharedPreferences.getInstance().then((sp){
      token =  sp.getString(StringsFile.AccessToken);
      userid =  sp.getString(StringsFile.UserID);
      return sp;

    });

    final dio = Dio();
    dio.options.headers["Authorization"] = ("Bearer "+token);
    final client = RestClient(dio);
    ReceiptResponse data = await client.getReceipts(userid).then((response) {
      return response;
    }).catchError((Object obj) {
      print(obj.toString());
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;
          if (res.statusCode == 401) {
            logger
                .e("Got error 401 : ${res.statusCode} -> ${res.statusMessage}");
          } else {
            logger.e("Got error : ${res.statusCode} -> ${res.statusMessage}");
          }
          break;
        default:
      }
    });
    return data;
  }

}
