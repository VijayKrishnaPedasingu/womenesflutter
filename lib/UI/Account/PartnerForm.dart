

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Request/PartnerRequest.dart';
import 'package:womenes/Services/RetrofitServices.dart';

final logger = Logger();
class PartnerForm extends StatefulWidget {
  @override
  _PartnerFormState createState() => _PartnerFormState();
}

class _PartnerFormState extends State<PartnerForm> {


  TextEditingController _firstname = new TextEditingController();
  TextEditingController _lastname = new TextEditingController();
  TextEditingController _email = new TextEditingController();
  TextEditingController _phonenumber = new TextEditingController();
  TextEditingController _message = new TextEditingController();

  final formkey = GlobalKey<FormState>();
  GlobalKey keys = GlobalKey();
  SharedPreferences sharedPreferences;
  var token;


  @override
  void initState() {

    getString();
  }



  //bool ValidateTab = true;
  bool autovalidate = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Partner SignUp"),),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
            key: keys,
            child: Form(
              key:  formkey,
              autovalidate: true,

              child: Column(
                children: <Widget>[
                  SizedBox(height: 20,),
                  Center(

                      child: Text('Partner SignUp',style: TextStyle(fontSize: 25,color:Colors.black,fontStyle: FontStyle.normal,decorationThickness:20.0 ),)),

                  /* Padding(
                    padding: const EdgeInsets.only(right: 5.0,left: 5.0,top: 15.0),
                    child: TextFormField(
                      controller: _username,
                      decoration: InputDecoration( prefixIcon: Icon(Icons.account_circle,color: Colors.pinkAccent,),labelText: "User Name",hintText: "Enter User", border: OutlineInputBorder(borderSide: BorderSide())),
                      validator: validateusername,

                    ),
                  ),*/

                  Padding(
                    padding: const EdgeInsets.only(right: 5.0,left: 5.0,top: 30.0),
                    child: TextFormField(
                      controller: _firstname,
                      decoration: InputDecoration( prefixIcon: Icon(Icons.person_pin_circle,color:Color(0xffff1493),),labelText: "First Name",hintText: "Enter FirstName", border: OutlineInputBorder(borderSide: BorderSide())),
                      validator: validatefirstname,

                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(right: 5.0,left: 5.0,top: 10.0),
                    child: TextFormField(
                      controller: _lastname,
                      decoration: InputDecoration( prefixIcon: Icon(Icons.perm_identity,color: Color(0xffff1493),),labelText: "Last Name",hintText: "Enter LastName", border: OutlineInputBorder(borderSide: BorderSide())),
                      validator: validatelastname,
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(right: 5.0,left: 5.0, top: 10.0),
                    child: TextFormField(
                      controller: _email,
                      decoration: InputDecoration( prefixIcon: Icon(Icons.email,color:Color(0xffff1493),),labelText: "Email",hintText: "Enter Email", border: OutlineInputBorder(borderSide: BorderSide())),
                      validator: validatemail,
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(right: 5.0,left: 5.0,top: 10.0),
                    child: TextFormField(
                      controller: _phonenumber,
                      decoration: InputDecoration( prefixIcon: Icon(Icons.mobile_screen_share,color:Color(0xffff1493),),labelText: "Phone Number",hintText: "Enter Phone Number", border: OutlineInputBorder(borderSide: BorderSide())),
                      validator: validatephonenumber,
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(right: 5.0,left: 5.0,top: 10.0),
                    child: TextFormField(
                      controller: _message,
                      decoration: InputDecoration( prefixIcon: Icon(Icons.lock,color:Color(0xffff1493),),labelText: "Message",hintText: "Please Give Message ", border: OutlineInputBorder(borderSide: BorderSide())),
                      validator: validatemsg,
                    ),
                  ),
                  /* Padding(
                    padding: const EdgeInsets.only(right: 5.0,bottom: 15.0,left: 5.0,top: 10.0),
                    child: TextFormField(
                      controller: _cnf_password,
                      decoration: InputDecoration( prefixIcon: Icon(Icons.lock,color: Colors.pinkAccent),labelText: "Confirm Password",hintText: "Re-Enter Password", border: OutlineInputBorder(borderSide: BorderSide())),
                      validator: validatcnfpwd,
                    ),
                  ),*/

                  Padding(
                    padding: const EdgeInsets.only(right: 5.0,left: 5.0,top: 25.0),
                    child: Center(
                      child: RaisedButton(

                          child: Text("Submit",style: TextStyle(fontSize: 20),),color: Color(0xffff1493),textColor: Colors.white,
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0),),
                          onPressed: () async {


                            //ValidateTab = false;
                            if(formkey.currentState.validate()) {
                              validateInputs();
                              PartnerRequset req = new PartnerRequset(
                                  firstName: _firstname.text.toString(),
                                  lastName: _lastname.text.toString(),
                                  email: _email.text.toString(),
                                  phoneNumber: _phonenumber.text.toString(),
                                  message: _message.text.toString());

                              token = await sharedPreferences.getString("Token");

                              final dio = Dio();
                              final client = RestClient(dio);
                              client.postPartner(req).then((partnerresponse) async{


                                Fluttertoast.showToast(
                                    msg: partnerresponse.endUserMessage,
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 3,
                                    backgroundColor: Colors.grey,
                                    textColor: Colors.white
                                );
                                  Navigator.pop(context);

                              }

                              ).catchError((Object obj){
                                print("error $obj.toString");
                                switch(obj.runtimeType){
                                  case DioError:
                                    final res = (obj as DioError).response;
                                    if(res.statusCode == 401){
                                      logger.e("Got an Error 401: ${res.statusCode}-> ${res.statusMessage}");
                                    }
                                    else{
                                      logger.e("Got Error: ${res.statusCode}->${res.statusMessage}");
                                    }
                                    break;
                                  default:
                                }
                              });

                            }


                            else{

                              print("guyjm");

                              Fluttertoast.showToast(
                                  msg: "Please Fill all the Fields",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 3,
                                  backgroundColor: Colors.grey,
                                  textColor: Colors.white
                              );

                            }

                          }),

                    ),
                  )

                ],

              ),


            ),



          ),
        ),
      ),
    );
  }

  String validateusername(String value) {
    if(value.isEmpty){
      return "Enter UserName";
    }else if(value.length < 3)
    {
      return "UserName Should be minimum 3 chracters";
    }
    String un = "[a-zA-Z0-9]";
    RegExp regExp = new RegExp(un);
    if(regExp.hasMatch(value)){
      return null;
    }
    return null;

  }

  String validatefirstname(String value) {
    if(value.isEmpty){
      return "Enter FirstName";
    }else if(value.length < 3)
    {
      return "UserName Should be minimum 3 chracters";
    }
    String un = "[a-zA-Z0-9]";
    RegExp regExp = new RegExp(un);
    if(regExp.hasMatch(value)){
      return null;
    }
    return null;

  }

  String validatelastname(String value) {

    if(value.isEmpty){
      return "Enter LastName";
    }else if(value.length < 3)
    {
      return "UserName Should be minimum 3 chracters";
    }
    String un = "[a-zA-Z0-9]";
    RegExp regExp = new RegExp(un);
    if(regExp.hasMatch(value)){
      return null;
    }
    return null;
  }

  String validatemail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }
    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';

  }

  String validatephonenumber(String value) {
    String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return 'Please enter mobile number';
    }
    else if(value.length > 10){
      return 'Mobile number must nout Exceed 10 numbers';
    }
    else if (!regExp.hasMatch(value)) {
      return 'Please enter valid mobile number';
    }
    return null;

  }

  String validatemsg(String value) {
    if(value.length==0){
      return "Please Give Message";
    }
    else
    {
      return null;
    }

  }



  void validateInputs() {
    final form = formkey.currentState;
    if (form.validate()) {
      // Text forms was validated.
      form.save();
    } else {
      setState(() => autovalidate = true);
    }
  }

  Future getString() async {
    sharedPreferences = await SharedPreferences.getInstance();

  }
}



