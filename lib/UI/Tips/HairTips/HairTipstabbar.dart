import 'package:flutter/material.dart';

import 'DamagedHair.dart';
import 'Dandruff.dart';
import 'GreyHair.dart';
import 'HairFall.dart';
import 'Lice.dart';
import 'SplitEnds.dart';

class HairTipstabbar extends StatefulWidget {
  int indexs;

  HairTipstabbar(this.indexs);

  @override
  _HairTipstabbarState createState() => _HairTipstabbarState(this.indexs);
}

class _HairTipstabbarState extends State<HairTipstabbar> {
  int indexs;

  _HairTipstabbarState(this.indexs);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      initialIndex: indexs,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xffff1493),
          bottom: TabBar(
              isScrollable: true,
              indicatorWeight: 2.0,
              labelPadding: EdgeInsets.symmetric(horizontal: 9.0),
              indicatorColor: Colors.white,
              indicatorSize: TabBarIndicatorSize.label,
              tabs: [
                Tab(
                  text: "Danddruff",
                ),
                Tab(
                  text: "HairFall",
                ),
                Tab(
                  text: "GreyHair",
                ),
                Tab(
                  text: "Lice",
                ),
                Tab(
                  text: "SplitEnds",
                ),
                Tab(
                  text: "Damaged Hair",
                )
              ]),
          title: Text('Hair Tips'),
        ),
        body: TabBarView(children: [
          Dandruff(),
          HairFall(),
          GreyHair(),
          Lice(),
          SplitEnds(),
          DamagedHair(),
        ]),
      ),
    );
  }
}
