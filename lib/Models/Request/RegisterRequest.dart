// To parse this JSON data, do
//
//     final registrationRequest = registrationRequestFromJson(jsonString);

import 'dart:convert';

class RegistrationRequest {
  String userName;
  String firstName;
  String lastName;
  String email;
  String mobileNumber;
  String password;
  String confirmPassword;

  RegistrationRequest({
    this.userName,
    this.firstName,
    this.lastName,
    this.email,
    this.mobileNumber,
    this.password,
    this.confirmPassword,
  });

  factory RegistrationRequest.fromRawJson(String str) => RegistrationRequest.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RegistrationRequest.fromJson(Map<String, dynamic> json) => RegistrationRequest(
    userName: json["userName"] == null ? null : json["userName"],
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    email: json["email"] == null ? null : json["email"],
    mobileNumber: json["mobileNumber"] == null ? null : json["mobileNumber"],
    password: json["password"] == null ? null : json["password"],
    confirmPassword: json["confirmPassword"] == null ? null : json["confirmPassword"],
  );

  Map<String, dynamic> toJson() => {
    "userName": userName == null ? null : userName,
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "email": email == null ? null : email,
    "mobileNumber": mobileNumber == null ? null : mobileNumber,
    "password": password == null ? null : password,
    "confirmPassword": confirmPassword == null ? null : confirmPassword,
  };
}
