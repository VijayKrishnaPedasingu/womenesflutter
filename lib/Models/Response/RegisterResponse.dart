// To parse this JSON data, do
//
//     final registrationResponse = registrationResponseFromJson(jsonString);

import 'dart:convert';

class RegistrationResponse {
  Result result;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  List<Link> links;
  List<dynamic> validationErrors;
  dynamic exception;

  RegistrationResponse({
    this.result,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory RegistrationResponse.fromRawJson(String str) => RegistrationResponse.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RegistrationResponse.fromJson(Map<String, dynamic> json) => RegistrationResponse(
    result: json["result"] == null ? null : Result.fromJson(json["result"]),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"] == null ? null : List<Link>.from(json["links"].map((x) => Link.fromJson(x))),
    validationErrors: json["validationErrors"] == null ? null : List<dynamic>.from(json["validationErrors"].map((x) => x)),
    exception: json["exception"],
  );

  Map<String, dynamic> toJson() => {
    "result": result == null ? null : result.toJson(),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links == null ? null : List<dynamic>.from(links.map((x) => x.toJson())),
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x)),
    "exception": exception,
  };
}

class Link {
  String invitelink;

  Link({
    this.invitelink,
  });

  factory Link.fromRawJson(String str) => Link.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Link.fromJson(Map<String, dynamic> json) => Link(
    invitelink: json["invitelink"] == null ? null : json["invitelink"],
  );

  Map<String, dynamic> toJson() => {
    "invitelink": invitelink == null ? null : invitelink,
  };
}

class Result {
  String firstName;
  String lastName;
  int rating;
  String referalCode;
  int referalBonus;
  dynamic referredBy;
  dynamic latitude;
  dynamic longitude;
  dynamic studentRollNumber;
  dynamic collegeName;
  dynamic course;
  bool areYouStudent;
  bool goddess;
  String id;
  String userName;
  String normalizedUserName;
  String email;
  String normalizedEmail;
  bool emailConfirmed;
  String passwordHash;
  String securityStamp;
  String concurrencyStamp;
  String phoneNumber;
  bool phoneNumberConfirmed;
  bool twoFactorEnabled;
  dynamic lockoutEnd;
  bool lockoutEnabled;
  int accessFailedCount;

  Result({
    this.firstName,
    this.lastName,
    this.rating,
    this.referalCode,
    this.referalBonus,
    this.referredBy,
    this.latitude,
    this.longitude,
    this.studentRollNumber,
    this.collegeName,
    this.course,
    this.areYouStudent,
    this.goddess,
    this.id,
    this.userName,
    this.normalizedUserName,
    this.email,
    this.normalizedEmail,
    this.emailConfirmed,
    this.passwordHash,
    this.securityStamp,
    this.concurrencyStamp,
    this.phoneNumber,
    this.phoneNumberConfirmed,
    this.twoFactorEnabled,
    this.lockoutEnd,
    this.lockoutEnabled,
    this.accessFailedCount,
  });

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    rating: json["rating"] == null ? null : json["rating"],
    referalCode: json["referalCode"] == null ? null : json["referalCode"],
    referalBonus: json["referalBonus"] == null ? null : json["referalBonus"],
    referredBy: json["referredBy"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    studentRollNumber: json["studentRollNumber"],
    collegeName: json["collegeName"],
    course: json["course"],
    areYouStudent: json["are_You_Student"] == null ? null : json["are_You_Student"],
    goddess: json["goddess"] == null ? null : json["goddess"],
    id: json["id"] == null ? null : json["id"],
    userName: json["userName"] == null ? null : json["userName"],
    normalizedUserName: json["normalizedUserName"] == null ? null : json["normalizedUserName"],
    email: json["email"] == null ? null : json["email"],
    normalizedEmail: json["normalizedEmail"] == null ? null : json["normalizedEmail"],
    emailConfirmed: json["emailConfirmed"] == null ? null : json["emailConfirmed"],
    passwordHash: json["passwordHash"] == null ? null : json["passwordHash"],
    securityStamp: json["securityStamp"] == null ? null : json["securityStamp"],
    concurrencyStamp: json["concurrencyStamp"] == null ? null : json["concurrencyStamp"],
    phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
    phoneNumberConfirmed: json["phoneNumberConfirmed"] == null ? null : json["phoneNumberConfirmed"],
    twoFactorEnabled: json["twoFactorEnabled"] == null ? null : json["twoFactorEnabled"],
    lockoutEnd: json["lockoutEnd"],
    lockoutEnabled: json["lockoutEnabled"] == null ? null : json["lockoutEnabled"],
    accessFailedCount: json["accessFailedCount"] == null ? null : json["accessFailedCount"],
  );

  Map<String, dynamic> toJson() => {
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "rating": rating == null ? null : rating,
    "referalCode": referalCode == null ? null : referalCode,
    "referalBonus": referalBonus == null ? null : referalBonus,
    "referredBy": referredBy,
    "latitude": latitude,
    "longitude": longitude,
    "studentRollNumber": studentRollNumber,
    "collegeName": collegeName,
    "course": course,
    "are_You_Student": areYouStudent == null ? null : areYouStudent,
    "goddess": goddess == null ? null : goddess,
    "id": id == null ? null : id,
    "userName": userName == null ? null : userName,
    "normalizedUserName": normalizedUserName == null ? null : normalizedUserName,
    "email": email == null ? null : email,
    "normalizedEmail": normalizedEmail == null ? null : normalizedEmail,
    "emailConfirmed": emailConfirmed == null ? null : emailConfirmed,
    "passwordHash": passwordHash == null ? null : passwordHash,
    "securityStamp": securityStamp == null ? null : securityStamp,
    "concurrencyStamp": concurrencyStamp == null ? null : concurrencyStamp,
    "phoneNumber": phoneNumber == null ? null : phoneNumber,
    "phoneNumberConfirmed": phoneNumberConfirmed == null ? null : phoneNumberConfirmed,
    "twoFactorEnabled": twoFactorEnabled == null ? null : twoFactorEnabled,
    "lockoutEnd": lockoutEnd,
    "lockoutEnabled": lockoutEnabled == null ? null : lockoutEnabled,
    "accessFailedCount": accessFailedCount == null ? null : accessFailedCount,
  };
}
