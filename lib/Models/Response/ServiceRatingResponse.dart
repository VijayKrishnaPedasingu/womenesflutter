// To parse this JSON data, do
//
//     final serviceRatingResponse = serviceRatingResponseFromJson(jsonString);

import 'dart:convert';

ServiceRatingResponse serviceRatingResponseFromJson(String str) => ServiceRatingResponse.fromJson(json.decode(str));

String serviceRatingResponseToJson(ServiceRatingResponse data) => json.encode(data.toJson());

class ServiceRatingResponse {
  Result result;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  List<Exception> links;
  List<ValidationError> validationErrors;
  Exception exception;

  ServiceRatingResponse({
    this.result,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory ServiceRatingResponse.fromJson(Map<String, dynamic> json) => ServiceRatingResponse(
    result: json["result"] == null ? null : Result.fromJson(json["result"]),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"] == null ? null : List<Exception>.from(json["links"].map((x) => Exception.fromJson(x))),
    validationErrors: json["validationErrors"] == null ? null : List<ValidationError>.from(json["validationErrors"].map((x) => ValidationError.fromJson(x))),
    exception: json["exception"] == null ? null : Exception.fromJson(json["exception"]),
  );

  Map<String, dynamic> toJson() => {
    "result": result == null ? null : result.toJson(),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links == null ? null : List<dynamic>.from(links.map((x) => x.toJson())),
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x.toJson())),
    "exception": exception == null ? null : exception.toJson(),
  };
}

class Exception {
  Exception();

  factory Exception.fromJson(Map<String, dynamic> json) => Exception(
  );

  Map<String, dynamic> toJson() => {
  };
}

class Result {
  int id;
  String serviceName;
  String serviceCode;
  String description;
  double avgRating;
  String createdBy;
  String modifiedBy;
  DateTime createdDate;
  DateTime modifiedDate;
  int discountPercentageUptoOrFlatAmount;
  int discountTypeId;
  DiscountType discountType;
  double totalAmount;
  String duration;
  int taxPercentage;
  bool isActive;
  List<ServiceRepository> serviceRepositories;

  Result({
    this.id,
    this.serviceName,
    this.serviceCode,
    this.description,
    this.avgRating,
    this.createdBy,
    this.modifiedBy,
    this.createdDate,
    this.modifiedDate,
    this.discountPercentageUptoOrFlatAmount,
    this.discountTypeId,
    this.discountType,
    this.totalAmount,
    this.duration,
    this.taxPercentage,
    this.isActive,
    this.serviceRepositories,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    serviceName: json["serviceName"] == null ? null : json["serviceName"],
    serviceCode: json["serviceCode"] == null ? null : json["serviceCode"],
    description: json["description"] == null ? null : json["description"],
    avgRating: json["avgRating"] == null ? null : json["avgRating"],
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    modifiedBy: json["modifiedBy"] == null ? null : json["modifiedBy"],
    createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
    modifiedDate: json["modifiedDate"] == null ? null : DateTime.parse(json["modifiedDate"]),
    discountPercentageUptoOrFlatAmount: json["discountPercentageUptoOrFlatAmount"] == null ? null : json["discountPercentageUptoOrFlatAmount"],
    discountTypeId: json["discountTypeId"] == null ? null : json["discountTypeId"],
    discountType: json["discountType"] == null ? null : DiscountType.fromJson(json["discountType"]),
    totalAmount: json["totalAmount"] == null ? null : json["totalAmount"],
    duration: json["duration"] == null ? null : json["duration"],
    taxPercentage: json["taxPercentage"] == null ? null : json["taxPercentage"],
    isActive: json["isActive"] == null ? null : json["isActive"],
    serviceRepositories: json["serviceRepositories"] == null ? null : List<ServiceRepository>.from(json["serviceRepositories"].map((x) => ServiceRepository.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "serviceName": serviceName == null ? null : serviceName,
    "serviceCode": serviceCode == null ? null : serviceCode,
    "description": description == null ? null : description,
    "avgRating": avgRating == null ? null : avgRating,
    "createdBy": createdBy == null ? null : createdBy,
    "modifiedBy": modifiedBy == null ? null : modifiedBy,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
    "modifiedDate": modifiedDate == null ? null : modifiedDate.toIso8601String(),
    "discountPercentageUptoOrFlatAmount": discountPercentageUptoOrFlatAmount == null ? null : discountPercentageUptoOrFlatAmount,
    "discountTypeId": discountTypeId == null ? null : discountTypeId,
    "discountType": discountType == null ? null : discountType.toJson(),
    "totalAmount": totalAmount == null ? null : totalAmount,
    "duration": duration == null ? null : duration,
    "taxPercentage": taxPercentage == null ? null : taxPercentage,
    "isActive": isActive == null ? null : isActive,
    "serviceRepositories": serviceRepositories == null ? null : List<dynamic>.from(serviceRepositories.map((x) => x.toJson())),
  };
}

class DiscountType {
  int id;
  String discounttype;

  DiscountType({
    this.id,
    this.discounttype,
  });

  factory DiscountType.fromJson(Map<String, dynamic> json) => DiscountType(
    id: json["id"] == null ? null : json["id"],
    discounttype: json["discounttype"] == null ? null : json["discounttype"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "discounttype": discounttype == null ? null : discounttype,
  };
}

class ServiceRepository {
  int id;
  int serviceId;
  String fileName;
  String fileExtension;
  String fileLocation;

  ServiceRepository({
    this.id,
    this.serviceId,
    this.fileName,
    this.fileExtension,
    this.fileLocation,
  });

  factory ServiceRepository.fromJson(Map<String, dynamic> json) => ServiceRepository(
    id: json["id"] == null ? null : json["id"],
    serviceId: json["serviceId"] == null ? null : json["serviceId"],
    fileName: json["fileName"] == null ? null : json["fileName"],
    fileExtension: json["fileExtension"] == null ? null : json["fileExtension"],
    fileLocation: json["fileLocation"] == null ? null : json["fileLocation"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "serviceId": serviceId == null ? null : serviceId,
    "fileName": fileName == null ? null : fileName,
    "fileExtension": fileExtension == null ? null : fileExtension,
    "fileLocation": fileLocation == null ? null : fileLocation,
  };
}

class ValidationError {
  String name;
  String description;

  ValidationError({
    this.name,
    this.description,
  });

  factory ValidationError.fromJson(Map<String, dynamic> json) => ValidationError(
    name: json["name"] == null ? null : json["name"],
    description: json["description"] == null ? null : json["description"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "description": description == null ? null : description,
  };
}
