import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_date_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Response/AppointmentResponse.dart';
import 'package:womenes/Models/Response/GetServicesTimings.dart';
import 'package:womenes/Models/Request/AppointmentRequest.dart';
import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/Utils/StringsFile.dart';


class AddAppointments extends StatefulWidget {
  @override
  _BookAppState createState() => _BookAppState();
}

class _BookAppState extends State<AddAppointments> {
  String day;
  String month;
  String year;
  String selectedTime;
  SharedPreferences pref;
  var userId, token;
  List<bool> btn_bg = List<bool>();
  List<bool> btn_txt = List<bool>();
  int _selectedItem;

  bool timings = false;

  String time;

  DateTime date;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPrefs();
  }

  selectItem(index) {
    setState(() {
      _selectedItem = index;

      // print(selectItem.toString());
    });
  }

  selectTime(time) {
    setState(() {
      selectedTime = time;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:Color(0xffff1493),
        title: Text("Book Appointments"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: ListTile(
              title: Text(
                "Pick Up Date",
                style: TextStyle(fontSize: 18, color: Color(0xffff1493),),
              ),
              trailing: Icon(
                Icons.calendar_today,
                color:Color(0xffff1493),
              ),
              subtitle: Text(
                "Click here",
                style: TextStyle(color: Color(0xff757575)),
              ),
              onTap: () async {
                DateTime newDateTime = await RoundedDatePicker.show(context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(DateTime.now().year,
                        DateTime.now().month, DateTime.now().day),
                    lastDate: DateTime(DateTime.now().year + 101),
                    theme: ThemeData(
                        primaryColor:Color(0xffff1493),
                        accentColor:Color(0xffff1493),
                        textTheme: TextTheme(
                          caption: TextStyle(
                              color: Color(0xffff1493),
                              fontWeight: FontWeight.w600),
                        )));

                date = newDateTime;
                day = newDateTime.day.toString();
                month = newDateTime.month.toString();
                year = newDateTime.year.toString();
                setState(() {
                  timings = true;
                });
              },
            ),
          ),
          Visibility(
            visible: timings,
            child: FutureBuilder(
              future: timings ? getData() : null,
              builder: (context, snapshot) {
                GetServiceTimings data = snapshot.data;

                if (snapshot.data != null) {
                  return Column(children: <Widget>[
                    Text(
                      "Date : $day/$month/$year",
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    GridView.builder(
                      shrinkWrap: true,
                      itemCount:
                      data.listResult[0].timings.length, // timings  count
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        childAspectRatio: MediaQuery.of(context).size.width /
                            (MediaQuery.of(context).size.height / 4),
                      ), // for Row count

                      itemBuilder: (BuildContext context, int index) {
                        return CustomItem(
                          selectTime,
                          selectItem, // callback function, setstate for parent
                          index: index,
                          isSelected: _selectedItem == index ? true : false,
                          title: data.listResult[0].timings[index].time,
                        );
                      },
                    ),
                  ]);
                } else {
                  return Center(child: (CircularProgressIndicator()));
                }
              },
            ),
          ),
          SizedBox(
            height: 20,
          ),
          RaisedButton(
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 12),
              onPressed: () async {
                if (day != null && _selectedItem != null) {
                  AppointmentRequest request = AppointmentRequest(
                      appointmentDate: date,
                      facilityId: 9,
                      customerUserId: userId,
                      slotTime: selectedTime);
                  final dio = Dio();
                  dio.options.headers["Authorization"] = ("Bearer " + token);
                  final client = RestClient(dio);
                  print("TOKEN $token");

                  AppointmentResponse response =
                  await client.addAppointment(request).then((res) async {
                    Fluttertoast.showToast(
                        msg:res.endUserMessage,
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIos: 3,
                        backgroundColor:Color(0xffff1493),
                        textColor: Colors.white,
                        fontSize: 16.0);
                    print(res.isSuccess);
                    return res;
                  }).catchError((Object obj) {
                    print(obj);
                    switch (obj.runtimeType) {
                      case DioError:
                        final res = (obj as DioError).response;
                        if (res.statusCode == 401) {
                          Fluttertoast.showToast(
                              msg: "Error Occured ",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 3,
                              backgroundColor:Color(0xffff1493),
                              textColor: Colors.white,
                              fontSize: 16.0);
                        }
                        else {
                          Fluttertoast.showToast(
                              msg: " Connection TimeOut..",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 3,
                              backgroundColor:Color(0xffff1493),
                              textColor: Colors.white,
                              fontSize: 16.0);

                        }
                    }
                  });


                } else if (day == null) {
                  Fluttertoast.showToast(
                      msg: "Please Select Date",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 3,
                      backgroundColor:Color(0xffff1493),
                      textColor: Colors.white,
                      fontSize: 16.0);
                } else if (_selectedItem == null) {
                  Fluttertoast.showToast(
                      msg: "Please Select Time",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 3,
                      backgroundColor: Color(0xffff1493),
                      textColor: Colors.white,
                      fontSize: 16.0);
                }
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color:Color(0xffff1493),
              child: Text(
                "Book ",
                style: TextStyle(color: Colors.white),
              )),
        ],
      ),
    );
  }

  Future getPrefs() async {
    pref = await SharedPreferences.getInstance();
    token = await pref.getString(StringsFile.AccessToken);
    userId = await pref.getString(StringsFile.UserID);
  }

  Future<GetServiceTimings> getData() async {
    final dio = Dio();
    GetServiceTimings appointments; // model class  obj
    final client = RestClient(dio); // retro fit  obj

    appointments = (await client.getTimings(date.toIso8601String()).then((res) {
      return res;
    }));

    for (int i = 0; i < appointments.listResult[0].timings.length; i++) {
      btn_bg.add(false);
      btn_txt.add(false);
    }

    return appointments;
  }
}

class CustomItem extends StatefulWidget {
  final String title;
  final int index;
  final bool isSelected;
  Function(int) selectItem;
  Function(String) selectTime;

  CustomItem(
      this.selectTime,
      this.selectItem, {
        Key key,
        this.title,
        this.index,
        this.isSelected,
      }) : super(key: key);

  _CustomItemState createState() => _CustomItemState();
}

class _CustomItemState extends State<CustomItem> {
  Color pink = Color(0xffff1493);
  Color white = Color(0xffffffff);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: RaisedButton(
        onPressed: () {
          widget.selectItem(widget.index);
          widget.selectTime(widget.title);
        },
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: widget.isSelected ? pink : white,
        child: Text(
          "${widget.title}",
          style: TextStyle(color: widget.isSelected ? white : pink),
        ),
      ),
    );
  }
}
