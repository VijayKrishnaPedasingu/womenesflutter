// To parse this JSON data, do
//
//     final baseProfileReq = baseProfileReqFromJson(jsonString);

import 'dart:convert';

BaseProfileReq baseProfileReqFromJson(String str) => BaseProfileReq.fromJson(json.decode(str));

String baseProfileReqToJson(BaseProfileReq data) => json.encode(data.toJson());

class BaseProfileReq {
  String firstName;
  String lastName;
  String phoneNumber;
  String aspNetUserId;

  BaseProfileReq({
    this.firstName,
    this.lastName,
    this.phoneNumber,
    this.aspNetUserId,
  });

  factory BaseProfileReq.fromJson(Map<String, dynamic> json) => BaseProfileReq(
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
    aspNetUserId: json["aspNetUserId"] == null ? null : json["aspNetUserId"],
  );

  Map<String, dynamic> toJson() => {
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "phoneNumber": phoneNumber == null ? null : phoneNumber,
    "aspNetUserId": aspNetUserId == null ? null : aspNetUserId,
  };
}
