import 'package:async/async.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Response/GetServices.dart';
import 'package:womenes/Models/Response/LoginResponse.dart';
import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/UI/Appointments/AddAppointments.dart';
import 'package:womenes/UI/Appointments/MyAppointments.dart';
import 'package:womenes/UI/Appointments/VisitSummary.dart';
import 'package:womenes/UI/Awards/Goddess.dart';
import 'package:womenes/UI/Awards/GoddessStudent.dart';
import 'package:womenes/UI/Profiles/BaseProfile.dart';
import 'package:womenes/UI/Profiles/BasicProfile.dart';
import 'package:womenes/UI/Account/ChangePassword.dart';
import 'package:womenes/UI/Account/ContactUs.dart';
import 'package:womenes/UI/Account/Login.dart';
import 'package:womenes/UI/Profiles/ExtendedProfile.dart';
import 'package:womenes/UI/Receipt.dart';
import 'package:womenes/UI/Services/ServicesList.dart';
import 'package:womenes/UI/Tips/FaceTips/FaceTips.dart';
import 'package:womenes/UI/Tips/HandsTips.dart';
import 'package:womenes/UI/Tips/LegsTips.dart';
import 'package:womenes/UI/Account/PartnerForm.dart';
import 'package:womenes/Utils/StringsFile.dart';

import 'Feedback.dart';
import 'Services/PackagesList.dart';
import 'Tips/HairTips/HairTips.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  SharedPreferences pref;
  var user, name, extension, location;
  var email;
  var points ;
  double _crossAxisSpacing = 8, _mainAxisSpacing = 8, _aspectRatio = 2;
  int _crossAxisCount = 2;

  final AsyncMemoizer _memoizer = AsyncMemoizer();

  Duration duration;

  @override
  void initState() {
    super.initState();
    getString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Womenes'),
        backgroundColor: Color(0xffff1493),
      ),
      drawer: Drawers(),
      body: Body(),
    );
  }

  Widget Body() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: Column(children: <Widget>[
            imageSlider(),
            SizedBox(height: 5),
            services(),
            SizedBox(height: 10),
          ]),
        ),
        Tips(),
      ],
    );
  }

  Future getString() async {
    pref = await SharedPreferences.getInstance();
    user = await pref.getString(StringsFile.Username);
    email = await pref.getString(StringsFile.Email);
        points  = (await pref.getString(StringsFile.RefferalPoints));
        if(points == null){
          points = 0;
        }

    name = await pref.getString(StringsFile.Filename);
    extension = await pref.getString(StringsFile.FileExtenstion);
    location = await pref.getString(StringsFile.FileLocation);
  }

  //////     IMAGE SLIDER   WIDGET //////////
  Widget imageSlider() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        padding: EdgeInsets.all(2),
        decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
              width: 2,
            ),
            borderRadius: BorderRadius.circular(9)),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height/4,
        child: Carousel(
          boxFit: BoxFit.cover,
          dotBgColor: Colors.transparent,
          dotSize: 4,
          dotIncreasedColor: Color(0xffff1493),
          autoplay: true,
          borderRadius: true,
          dotPosition: DotPosition.bottomCenter,
          autoplayDuration: Duration(seconds: 3),
          showIndicator: true,
          images: [
            AssetImage('assets/images/slide-1.jpg'),
            AssetImage('assets/images/slide-2.jpg'),
            AssetImage('assets/images/slide-3.jpg'),
          ],
          animationCurve: Curves.ease,
          animationDuration: Duration(milliseconds: 1000),
        ),
      ),
    );
  }

///////   SERVICES  WIDGET    /////////
  Widget services() {
    return FutureBuilder(
      future: _fetchData(),
      builder: (context, snapshot) {
        GetServices data = snapshot.data;
        if (snapshot.data != null) {
          return Flexible(
            child: GridView.builder(
                shrinkWrap: true,
                itemCount: 8, // services count       HARD CODED LENGTH

                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    childAspectRatio: (MediaQuery.of(context).size.width) /
                        (MediaQuery.of(context).size.height /
                            2.5)), // for Row count
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundImage: NetworkImage(data.listResult[index]
                                  .serviceRepositories[0].fileLocation +
                              data.listResult[index].serviceRepositories[0]
                                  .fileName +
                              data.listResult[index].serviceRepositories[0]
                                  .fileExtension),
                          radius: 30,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          data.listResult[index].serviceName,
                          style: TextStyle(
                            fontSize:
                                MediaQuery.textScaleFactorOf(context) * 10,
                          ),
                          overflow: TextOverflow.visible,
                          softWrap: true,
                          textWidthBasis: TextWidthBasis.longestLine,
                        )
                      ],
                    ),
                  );
                }),
          );
        } else {
          return Center(
              child: CircularProgressIndicator(
                  valueColor:
                      new AlwaysStoppedAnimation<Color>(Color(0xffff1493))));
        }
      },
    );
  }

  _fetchData() {
    return this._memoizer.runOnce(() async {
      return getData();
    });
  }

  Future<GetServices> getData() async {
    final dio = Dio();
    GetServices services;
    final client = RestClient(dio);
    services = (await client.getService().then((res) {
      return res;
    }));

    return services;
  }

  /*-----    Drawer   -------*/
  Widget Drawers() {

      getString();


    return loginResponse == null
        ? Drawer(
            child: ListView(children: <Widget>[
            DrawerHeader(
                decoration: BoxDecoration(color: Color(0xffff1493)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: AssetImage('assets/images/logo.png'),
                      radius: 40.0,
                    ),
                  ],
                )),
            ListTile(
              title: Text('My Profile'),
              leading: Icon(Icons.account_circle),
              onTap: () {
                setState(() {
                  if (user == null) Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => LoginPage()));
                });
              },
            ),
            ListTile(
              title: Text('Services'),
              leading: Icon(Icons.person),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => ServicesCalling()));
              },
            ),
            ListTile(
              title: Text('Contact Us'),
              leading: Icon(Icons.perm_identity),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => ContactUs()));
              },
            ),
          ]))
        : Drawer(
            child: ListView(
              children: <Widget>[
                DrawerHeader(
                    decoration: BoxDecoration(color: Color(0xffff1493)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        loginResponse == null
                            ? CircleAvatar(
                                backgroundImage:
                                    AssetImage('assets/images/logo.png'),
                                radius: 40.0,
                              )
                            : CircleAvatar(
                                backgroundImage:
                              loginResponse==null?  NetworkImage(null):       NetworkImage(loginResponse.result.userInfo.fileLocation + loginResponse.result.userInfo.fileName + loginResponse.result.userInfo.fileExtension),
                                radius: 40.0,
                              ),
                        loginResponse==null ?Text("", style: TextStyle(color: Colors.white)):   Text(loginResponse.result.user.userName, style: TextStyle(color: Colors.white)),
                     loginResponse==null ?Text("", style: TextStyle(color: Colors.white)):   Text(loginResponse.result.user.email, style: TextStyle(color: Colors.white)),
                        Text('Points : $points',
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                ListTile(
                  title: Text('My Profile'),
                  leading: Icon(Icons.account_circle),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => BaseProfile()));
                  },
                ),
                ListTile(
                  title: Text('Basic Profile'),
                  leading: Icon(Icons.person),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => BasicProfile()));
                  },
                ),
                ListTile(
                  title: Text('Extended Profile'),
                  leading: Icon(Icons.perm_identity),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => ExtendedProfile()));
                  },
                ),
                ListTile(
                  title: Text('Services'),
                  leading: Icon(Icons.spa),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => ServicesCalling()));
                  },
                ),
                ListTile(
                  title: Text('Packages'),
                  leading: Icon(Icons.layers),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => PackagesList()));
                  },
                ),
                ListTile(
                  title: Text('Book Appointment'),
                  leading: Icon(Icons.assignment),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => AddAppointments()));
                  },
                ),
                ListTile(
                  title: Text('My Appointments'),
                  leading: Icon(Icons.calendar_today),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => MyAppointments()));
                  },
                ),
                ListTile(
                  title: Text('Visit Summary'),
                  leading: Icon(Icons.assignment_ind),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => VisitSummary()));
                  },
                ),
                ListTile(
                  title: Text('Receipt'),
                  leading: Icon(Icons.receipt),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => Receipt()));
                  },
                ),
                ListTile(
                  title: Text('Apply for Goddess Awards'),
                  leading: Icon(Icons.stars),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => Goddess()));
                  },
                ),
                ListTile(
                  title: Text('Student As Goddess'),
                  leading: Icon(Icons.local_florist),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => GoddessStudent()));
                  },
                ),
                Divider(
                  color: Color(0xFFBDBDBD),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 25.0),
                  child: Text(
                    'Account',
                    style:
                        TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                  ),
                ),
                ListTile(
                  title: Text('Change Password'),
                  leading: Icon(Icons.mode_edit),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => ChangePassword()));
                  },
                ),
                ListTile(
                  title: Text('FeedBack'),
                  leading: Icon(Icons.feedback),
                  onTap: () {
                    Navigator.pop(context);
                           Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => FeedbackPage()));
                  },
                ),
                ListTile(
                  title: Text('Contact Us'),
                  leading: Icon(Icons.call),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => ContactUs()));
                  },
                ),
                ListTile(
                  title: Text('Refer'),
                  leading: Icon(Icons.share),
                  onTap: () {
                    Navigator.pop(context);
                    // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => FirstPage()));
                  },
                ),
                ListTile(
                  title: Text('Become a Partner'),
                  leading: Icon(Icons.people),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => PartnerForm()));
                  },
                ),
                ListTile(
                  title: Text('Logout'),
                  leading: Icon(Icons.power_settings_new),
                  onTap: () {
                    pref.clear();
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            title: Text('LogOut'),
                            content: Text('Are you sure want to LogOut?'),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  loginResponse=null;
                                  Navigator.pop(context);
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          HomePage()));
                                },
                                child: Text(
                                  'Yes',
                                  style: TextStyle(color: Color(0xffff1493)),
                                ),
                              ),
                              FlatButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    'No',
                                    style: TextStyle(color: Color(0xffff1493)),
                                  ))
                            ],
                          );
                        });
                  },
                ),
              ],
            ),
          );
  }

  /////////////////Tips  WIdget //////////////////

  Widget Tips() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: Text(
            "Tips",
            style: TextStyle(
              color: Colors.grey,
              fontSize: MediaQuery.textScaleFactorOf(context) * 18,
            ),
          ),
        ),
        Container(
          height: 90,
          alignment: FractionalOffset.bottomCenter,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => FaceTips()));
                },
                child: Column(
                  children: <Widget>[
                    CircleAvatar(
                      child: Image.asset(
                        "assets/images/face.png",
                        fit: BoxFit.cover,
                      ),
                      radius: 30,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text("Face"),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 13.0),
                child: VerticalDivider(
                  color: Colors.grey,
                  width: 2,
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HairTips()));
                },
                child: Column(
                  children: <Widget>[
                    CircleAvatar(
                      child: Image.asset(
                        "assets/images/hair.png",
                        fit: BoxFit.cover,
                      ),
                      radius: 30,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text("Hair"),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 13.0),
                child: VerticalDivider(
                  color: Colors.grey,
                  width: 2,
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HandTips()));
                },
                child: Column(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: AssetImage("assets/images/hands.png"),
                      radius: 30,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text("Hands"),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 13.0),
                child: VerticalDivider(
                  color: Colors.grey,
                  width: 2,
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LegsTips()));
                },
                child: Column(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: AssetImage("assets/images/legs.png"),
                      radius: 30,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text("Legs"),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
