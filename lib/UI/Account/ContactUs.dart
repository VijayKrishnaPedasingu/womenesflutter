import 'package:flutter/material.dart';
import 'package:womenes/Utils/callServices.dart';
//import 'package:google_maps_flutter/google_maps_flutter.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final CallsAndMessagesService _service = locator<CallsAndMessagesService>();
  String number = "7993718888";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xffff1493),
        title: Text(
          'Contact Us',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            // mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0,30.0,0.0,5.0),
                child: CircleAvatar(
                    radius: 50,
                    child: Image.asset("assets/images/logo.png")),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(45.0, 0.0, 0.0, 0.0),
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.pin_drop),
                      onPressed: () {},
                    ),
                    Text(
                      '3rd floor, ',
                      style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.pink[200],
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(90.0, 5.0, 0.0, 5.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Ramakrushi Building,',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.pink[200],
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(90.0, 5.0, 0.0, 5.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Gayathri Nagar Main Road,',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.pink[200],
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(90.0, 5.0, 0.0, 5.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'near Benz Circle,',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.pink[200],
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(90.0, 5.0, 0.0, 5.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Vijayawada,',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.pink[200],
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(90.0, 5.0, 0.0, 5.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Andhra Pradesh.',
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.pink[200],
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(45.0, 10.0, 0.0, 0.0),
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.phone),
                      onPressed: () {
                        _service.call(number);
                      },
                    ),
                    Text(
                      '7993718888  /  7993728888 ',
                      style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.pink[200],
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 4.0, vertical: 20),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey,
                      )),
                  child: Image.asset("assets/images/loc.jpg"),
                ),
              )
            ],
          ),
        ),
      ),
    );
    //getLocation();
  }
}

//            void getLocation() async {
//   Position position = await Geolocator()
//       .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
//   print(position);
//   Fluttertoast.showToast(
//       msg: "Location detected",
//       toastLength: Toast.LENGTH_SHORT,
//       gravity: ToastGravity.BOTTOM,
//       timeInSecForIos: 1,
//       backgroundColor: Colors.grey,
//       textColor: Colors.white,
//       fontSize: 16.0
//   );
// }
