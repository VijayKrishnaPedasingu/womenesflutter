// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  Result result;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  dynamic links;
  List<dynamic> validationErrors;
  dynamic exception;

  LoginResponse({
    this.result,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    result: json["result"] == null ? null : Result.fromJson(json["result"]),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"],
    validationErrors: json["validationErrors"] == null ? null : List<dynamic>.from(json["validationErrors"].map((x) => x)),
    exception: json["exception"],
  );

  Map<String, dynamic> toJson() => {
    "result": result == null ? null : result.toJson(),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links,
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x)),
    "exception": exception,
  };
}

class Result {
  User user;
  UserInfo userInfo;
  Role role;
  String roleName;
  List<ActivityRight> activityRights;
  String accessToken;
  dynamic refreshToken;
  int expiresIn;
  String tokenType;

  Result({
    this.user,
    this.userInfo,
    this.role,
    this.roleName,
    this.activityRights,
    this.accessToken,
    this.refreshToken,
    this.expiresIn,
    this.tokenType,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    user: json["user"] == null ? null : User.fromJson(json["user"]),
    userInfo: json["userInfo"] == null ? null : UserInfo.fromJson(json["userInfo"]),
    role: json["role"] == null ? null : Role.fromJson(json["role"]),
    roleName: json["roleName"] == null ? null : json["roleName"],
    activityRights: json["activityRights"] == null ? null : List<ActivityRight>.from(json["activityRights"].map((x) => ActivityRight.fromJson(x))),
    accessToken: json["accessToken"] == null ? null : json["accessToken"],
    refreshToken: json["refreshToken"],
    expiresIn: json["expiresIn"] == null ? null : json["expiresIn"],
    tokenType: json["tokenType"] == null ? null : json["tokenType"],
  );

  Map<String, dynamic> toJson() => {
    "user": user == null ? null : user.toJson(),
    "userInfo": userInfo == null ? null : userInfo.toJson(),
    "role": role == null ? null : role.toJson(),
    "roleName": roleName == null ? null : roleName,
    "activityRights": activityRights == null ? null : List<dynamic>.from(activityRights.map((x) => x.toJson())),
    "accessToken": accessToken == null ? null : accessToken,
    "refreshToken": refreshToken,
    "expiresIn": expiresIn == null ? null : expiresIn,
    "tokenType": tokenType == null ? null : tokenType,
  };
}

class ActivityRight {
  String roleId;
  int activityRightId;

  ActivityRight({
    this.roleId,
    this.activityRightId,
  });

  factory ActivityRight.fromJson(Map<String, dynamic> json) => ActivityRight(
    roleId: json["roleId"] == null ? null : json["roleId"],
    activityRightId: json["activityRightId"] == null ? null : json["activityRightId"],
  );

  Map<String, dynamic> toJson() => {
    "roleId": roleId == null ? null : roleId,
    "activityRightId": activityRightId == null ? null : activityRightId,
  };
}

class Role {
  String userId;
  String roleId;

  Role({
    this.userId,
    this.roleId,
  });

  factory Role.fromJson(Map<String, dynamic> json) => Role(
    userId: json["userId"] == null ? null : json["userId"],
    roleId: json["roleId"] == null ? null : json["roleId"],
  );

  Map<String, dynamic> toJson() => {
    "userId": userId == null ? null : userId,
    "roleId": roleId == null ? null : roleId,
  };
}

class User {
  String firstName;
  String lastName;
  int rating;
  String referalCode;
  int referalBonus;
  dynamic referredBy;
  dynamic latitude;
  dynamic longitude;
  dynamic studentRollNumber;
  dynamic collegeName;
  dynamic course;
  bool areYouStudent;
  bool goddess;
  String id;
  String userName;
  String normalizedUserName;
  String email;
  String normalizedEmail;
  bool emailConfirmed;
  String passwordHash;
  String securityStamp;
  String concurrencyStamp;
  String phoneNumber;
  bool phoneNumberConfirmed;
  bool twoFactorEnabled;
  dynamic lockoutEnd;
  bool lockoutEnabled;
  int accessFailedCount;

  User({
    this.firstName,
    this.lastName,
    this.rating,
    this.referalCode,
    this.referalBonus,
    this.referredBy,
    this.latitude,
    this.longitude,
    this.studentRollNumber,
    this.collegeName,
    this.course,
    this.areYouStudent,
    this.goddess,
    this.id,
    this.userName,
    this.normalizedUserName,
    this.email,
    this.normalizedEmail,
    this.emailConfirmed,
    this.passwordHash,
    this.securityStamp,
    this.concurrencyStamp,
    this.phoneNumber,
    this.phoneNumberConfirmed,
    this.twoFactorEnabled,
    this.lockoutEnd,
    this.lockoutEnabled,
    this.accessFailedCount,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    rating: json["rating"] == null ? null : json["rating"],
    referalCode: json["referalCode"] == null ? null : json["referalCode"],
    referalBonus: json["referalBonus"] == null ? null : json["referalBonus"],
    referredBy: json["referredBy"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    studentRollNumber: json["studentRollNumber"],
    collegeName: json["collegeName"],
    course: json["course"],
    areYouStudent: json["are_You_Student"] == null ? null : json["are_You_Student"],
    goddess: json["goddess"] == null ? null : json["goddess"],
    id: json["id"] == null ? null : json["id"],
    userName: json["userName"] == null ? null : json["userName"],
    normalizedUserName: json["normalizedUserName"] == null ? null : json["normalizedUserName"],
    email: json["email"] == null ? null : json["email"],
    normalizedEmail: json["normalizedEmail"] == null ? null : json["normalizedEmail"],
    emailConfirmed: json["emailConfirmed"] == null ? null : json["emailConfirmed"],
    passwordHash: json["passwordHash"] == null ? null : json["passwordHash"],
    securityStamp: json["securityStamp"] == null ? null : json["securityStamp"],
    concurrencyStamp: json["concurrencyStamp"] == null ? null : json["concurrencyStamp"],
    phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
    phoneNumberConfirmed: json["phoneNumberConfirmed"] == null ? null : json["phoneNumberConfirmed"],
    twoFactorEnabled: json["twoFactorEnabled"] == null ? null : json["twoFactorEnabled"],
    lockoutEnd: json["lockoutEnd"],
    lockoutEnabled: json["lockoutEnabled"] == null ? null : json["lockoutEnabled"],
    accessFailedCount: json["accessFailedCount"] == null ? null : json["accessFailedCount"],
  );

  Map<String, dynamic> toJson() => {
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "rating": rating == null ? null : rating,
    "referalCode": referalCode == null ? null : referalCode,
    "referalBonus": referalBonus == null ? null : referalBonus,
    "referredBy": referredBy,
    "latitude": latitude,
    "longitude": longitude,
    "studentRollNumber": studentRollNumber,
    "collegeName": collegeName,
    "course": course,
    "are_You_Student": areYouStudent == null ? null : areYouStudent,
    "goddess": goddess == null ? null : goddess,
    "id": id == null ? null : id,
    "userName": userName == null ? null : userName,
    "normalizedUserName": normalizedUserName == null ? null : normalizedUserName,
    "email": email == null ? null : email,
    "normalizedEmail": normalizedEmail == null ? null : normalizedEmail,
    "emailConfirmed": emailConfirmed == null ? null : emailConfirmed,
    "passwordHash": passwordHash == null ? null : passwordHash,
    "securityStamp": securityStamp == null ? null : securityStamp,
    "concurrencyStamp": concurrencyStamp == null ? null : concurrencyStamp,
    "phoneNumber": phoneNumber == null ? null : phoneNumber,
    "phoneNumberConfirmed": phoneNumberConfirmed == null ? null : phoneNumberConfirmed,
    "twoFactorEnabled": twoFactorEnabled == null ? null : twoFactorEnabled,
    "lockoutEnd": lockoutEnd,
    "lockoutEnabled": lockoutEnabled == null ? null : lockoutEnabled,
    "accessFailedCount": accessFailedCount == null ? null : accessFailedCount,
  };
}

class UserInfo {
  int id;
  String aspNetUserId;
  String fileName;
  String fileExtension;
  String fileLocation;

  UserInfo({
    this.id,
    this.aspNetUserId,
    this.fileName,
    this.fileExtension,
    this.fileLocation,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) => UserInfo(
    id: json["id"] == null ? null : json["id"],
    aspNetUserId: json["aspNetUserId"] == null ? null : json["aspNetUserId"],
    fileName: json["fileName"] == null ? null : json["fileName"],
    fileExtension: json["fileExtension"] == null ? null : json["fileExtension"],
    fileLocation: json["fileLocation"] == null ? null : json["fileLocation"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "aspNetUserId": aspNetUserId == null ? null : aspNetUserId,
    "fileName": fileName == null ? null : fileName,
    "fileExtension": fileExtension == null ? null : fileExtension,
    "fileLocation": fileLocation == null ? null : fileLocation,
  };
}
