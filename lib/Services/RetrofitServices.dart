import 'dart:core';


import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:womenes/Models/Request/BaseProfileRequest.dart';
import 'package:womenes/Models/Request/BasicProfileReq.dart';
import 'package:womenes/Models/Request/ChangePasswordReq.dart';
import 'package:womenes/Models/Request/ForgetRequest.dart';
import 'package:womenes/Models/Request/PackageRatingRequest.dart';
import 'package:womenes/Models/Request/PartnerRequest.dart';
import 'package:womenes/Models/Request/RegisterRequest.dart';
import 'package:womenes/Models/Request/feedbackpostrequest.dart';
import 'package:womenes/Models/Response/ApplyGoddess.dart';
import 'package:womenes/Models/Request/AppointmentRequest.dart';
import 'package:womenes/Models/Response/AppointmentResponse.dart';
import 'package:womenes/Models/Request/CancelAppointment.dart';
import 'package:womenes/Models/Response/BaseProfileResponse.dart';
import 'package:womenes/Models/Response/BasicProfileRes.dart';
import 'package:womenes/Models/Response/ChangePasswordResponse.dart';
import 'package:womenes/Models/Response/FeedbackResponse.dart';
import 'package:womenes/Models/Response/ForgetResponse.dart';
import 'package:womenes/Models/Response/GetAppointmentByUserId.dart';
import 'package:womenes/Models/Response/GetBaseProfileRes.dart';
import 'package:womenes/Models/Response/GetPackages.dart';
import 'package:womenes/Models/Response/GetQuestionsByUserId.dart';
import 'package:womenes/Models/Response/GetServicesTimings.dart';
import 'package:womenes/Models/Request/LoginRequest.dart';
import 'package:womenes/Models/Response/LoginResponse.dart';
import 'package:womenes/Models/Request/ServiceRatingRequest.dart';
import 'package:womenes/Models/Request/UpdateAppointmentRequest.dart';
import 'package:womenes/Models/Response/GetServices.dart';
import 'package:womenes/Models/Response/PartnerResponse.dart';
import 'package:womenes/Models/Response/ReceiptResponse.dart';
import 'package:womenes/Models/Response/RegisterResponse.dart';
import 'package:womenes/Models/Response/ServiceRatingResponse.dart';
import 'package:womenes/Models/Response/TipsResponseById.dart';
import 'package:womenes/Models/Response/VisitSummaryByuserId.dart';
import 'package:womenes/Models/Response/feedbackperametersresponse.dart';
import 'package:womenes/Models/Response/feedbackpostresponse.dart';




part 'RetrofitServices.g.dart';

//@RestApi(baseUrl: 'http://103.217.213.130:3331/api/')
//@RestApi(baseUrl: 'http://http://192.168.137.1:3331/api/')

@RestApi(baseUrl: 'http://api.womenes.com/api/')


abstract class RestClient {
  factory RestClient(Dio dio) = _RestClient;

  @POST("Account/Login")
  Future<LoginResponse> login(@Body() LoginRequest loginRequest);

  @POST("Account/Register")
  Future<RegistrationResponse> PostRegister(@Body() RegistrationRequest registrationRequest);

  @GET("Package/GetPackages")
  Future<PackagesResponse> getPackages();

  @GET("Appointment/GetAppointmentsByUserId/{UserId}")
  Future<GetAppointmentsByUserId> getAppointments(@Path("UserId") String id);

  @GET("Service/GetServices")
  Future<GetServices> getService();

  @GET("Appointment/GetServiceTimings")
  Future<GetServiceTimings> getTimings(@Query("dateTime") String time);

  @POST("Appointment/AddAppointment")
  Future<AppointmentResponse> addAppointment(@Body() AppointmentRequest  appointmentRequest);


  @PUT("Appointment/UpdateAppointment")
  Future<AppointmentResponse> updateAppointment(@Body() UpdateAppointmentReq updateAppointmentReq);


  @DELETE("Appointment/cancelappointment/{id}")
  Future<CancelAppointment> deleteAppointment(@Path("id") String id);


  @PUT("Account/updateRegistration")
  Future<ApplyGoddess> applyGoddess(@Query("userid") String userid , @Query("appliedforstudent") bool student , @Query("appliedforgoddess") bool goddess );


  @POST("UserFeedback/addratingforService")
  Future<ServiceRatingResponse> sendFeedback(@Body() ServiceRatingRequest request  );

  @POST("UserFeedback/addratingforPackage")
  Future<PackageRatingRequest> sendPackageFeedback(@Body() PackageRatingRequest request );


  @GET("VisitSummary/get_Visit_Sumary_By_User/{UserId}")
  Future<VisitSummaryByuserId> getsummary(@Path("UserId") String userId);


  @GET("Receipt/GetReceiptByUserId/{UserId}")
  Future<ReceiptResponse> getReceipts(@Path("UserId") String userId);

  @GET("Question/GetQuestionByGroupIdUserId/2/{UserId}")
  Future<GetQuestions> getQuestions(@Path("UserId") String userid);


  @GET("Question/GetQuestionByGroupIdUserId/1/{UserId}")
  Future<GetQuestions> getQuestionsAndAnswers(@Path("UserId") String userid);

  @POST("UserInfo/UpdateUserInfo")
  Future<BaseProfileRes> updateProfile(@Body() BaseProfileReq baseProfileReq);

  @POST("PartnerSignUp")
  Future<PartnerResponse> postPartner(@Body() PartnerRequset partnerRequest);

 @POST("Account/ChangePassword")
 Future<ChangePasswordResponse> changePassword(@Body() ChangePasswordReq changePasswordReq);


  @GET("Tips/GetInfo/{typeId}")
  Future<TipsResponsebyId> getTips(@Path("typeId") int tipId);

  @GET("Parameters/get")
  Future<FeedbackResponse> getParameters();

  @GET("UserInfo/GetUserInfo/{UserId}")
  Future<GetBaseProfileRes> getBaseProfile(@Path("UserId") String userid);

  @GET("Parameters/get")
  Future<Feedbackresponse> getFeedback();


  @POST("UserAnswer/PostUserAnswers")
  Future<BasicProfileRes> updateAnswers(@Body() List<BasicProfileReq> basicProfileReq);

  @POST("UserFeedback/add")
  Future<FeedbackPostResponse> postFeedBack(@Body() FeedbackPostRequest feedbackPostRequest);

  @POST("Account/ForgotPassword")
  Future<ForgetResponse> forgetPassword(@Body() ForgetRequest forgetRequest);


}


