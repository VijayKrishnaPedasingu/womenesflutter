import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Request/ChangePasswordReq.dart';
import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/UI/Account/Login.dart';
import 'package:womenes/Utils/StringsFile.dart';

bool _currentpassword = true;
bool _newpassword = true;
bool _confirmpassword = true;

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  SharedPreferences sharedPreferences;
  var email;
  var token;
  TextEditingController CurrentPassword = new TextEditingController();
  TextEditingController NewPassword = new TextEditingController();
  TextEditingController ConformPassword = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    String validatepassword(String value) {
      if (value.length == 0) {
        return "Enter Password";
      } else if (value.length < 6)
        return "password should be more than 6 charactes";
      else {
        return null;
      }
    }

    String validatcnfpwd(String value) {
      var pwd = NewPassword.text;
      if (value.length == 0) {
        return 'confirm password is required';
      } else if (value != pwd) {
        return 'password mismatch';
      } else {
        return null;
      }
    }

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('ChangePassword'),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Form(
          autovalidate: true,
          child: Column(
            children: <Widget>[
//             SizedBox(height: 50,),
//             Text("Change Password"),
              SizedBox(
                height: 50,
              ),

              Padding(
                padding: const EdgeInsets.only(
                    right: 18.0, left: 18, top: 18, bottom: 18),
                child: TextFormField(
                  obscureText: _currentpassword,
                  controller: CurrentPassword,
                  maxLines: 1,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                    labelText: "Enter Your Password",
                    hintText: "Password",
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(
                        color: Color(0xffff1493),
                      ),
                    ),
                    prefixIcon: const Icon(
                      Icons.lock,
                      color: Color(0xffff1493),
                    ),
                    suffixIcon: GestureDetector(
                      child: const Icon(Icons.remove_red_eye),
                      onTap: () {
                        setState(() {
                          _currentpassword = !_currentpassword;
                        });
                      },
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    right: 18.0, left: 18, top: 18, bottom: 18),
                child: TextFormField(
                  obscureText: _newpassword,
                  validator: validatepassword,
                  controller: NewPassword,
                  maxLines: 1,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                    labelText: "Enter Your Password",
                    hintText: "Password",
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(
                        color: Color(0xffff1493),
                      ),
                    ),
                    prefixIcon: const Icon(
                      Icons.lock,
                      color: Color(0xffff1493),
                    ),
                    suffixIcon: GestureDetector(
                      child: const Icon(Icons.remove_red_eye),
                      onTap: () {
                        setState(() {
                          _newpassword = !_newpassword;
                        });
                      },
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    right: 18.0, left: 18, top: 18, bottom: 18),
                child: TextFormField(
                  obscureText: _confirmpassword,
                  validator: validatcnfpwd,
                  controller: ConformPassword,
                  maxLines: 1,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                    labelText: "Enter Your Password",
                    hintText: "Password",
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(
                        color: Color(0xffff1493),
                      ),
                    ),
                    prefixIcon: const Icon(
                      Icons.lock,
                      color: Color(0xffff1493),
                    ),
                    suffixIcon: GestureDetector(
                      child: const Icon(Icons.remove_red_eye),
                      onTap: () {
                        setState(() {
                          _confirmpassword = !_confirmpassword;
                        });
                      },
                    ),
                  ),
                ),
              ),

              RaisedButton(
                  child: Text("Change Password"),
                  onPressed: () {
                    ChangePasswordReq changePasswordReq = new ChangePasswordReq(
                        email: email,
                        oldPassword: CurrentPassword.text,
                        newPassword: NewPassword.text,
                        confirmPassword: ConformPassword.text);

                    final dio = Dio();
                    dio.options.headers["Authorization"] =
                        ("Bearer " + "$token");
                    final client = RestClient(dio);

                    client.changePassword(changePasswordReq).then((response) {
//                     logger.d(response);
                      if (response.isSuccess) {
                        Fluttertoast.showToast(
                            msg: response.endUserMessage,
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIos: 3,
                            backgroundColor: Color(0xffff1493),
                            textColor: Colors.white,
                            fontSize: 16.0);
                        Navigator.pop(context);
                      } else {
                        print(response.validationErrors.length);
                        if (response.validationErrors.length == 1) {
//                          logger.d(response.toJson());
                          Fluttertoast.showToast(
                              msg: response.validationErrors
                                  .elementAt(0)
                                  .description,
                              toastLength: Toast.LENGTH_SHORT,
                              timeInSecForIos: 3,
                              backgroundColor: Color(0xffff1493),
                              textColor: Colors.white,
                              fontSize: 16.0);
                        } else {
                          Fluttertoast.showToast(
                              msg: response.endUserMessage,
                              toastLength: Toast.LENGTH_SHORT,
                              timeInSecForIos: 3,
                              backgroundColor: Color(0xffff1493),
                              textColor: Colors.white,
                              fontSize: 16.0);
                        }
                      }
                    }).catchError((Object obj) {
                      print(obj);
                      switch (obj.runtimeType) {
                        case DioError:
                          final res = (obj as DioError).response;

                          if (res.statusCode == 401) {
                            sharedPreferences.clear();
                            sharedPreferences.commit();
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LoginPage()));
                            Fluttertoast.showToast(
                                msg: " Connection TimeOut..",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 3,
                                backgroundColor: Color(0xffff1493),
                                textColor: Colors.white,
                                fontSize: 16.0);
                          } else if (res.statusCode == 500) {
                            Fluttertoast.showToast(
                                msg: "Failed to connect Server",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 3,
                                backgroundColor: Color(0xffff1493),
                                textColor: Colors.white,
                                fontSize: 16.0);
                          } else {
                            Fluttertoast.showToast(
                                msg: " Error Occured ..",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 3,
                                backgroundColor: Color(0xffff1493),
                                textColor: Colors.white,
                                fontSize: 16.0);
                          }
                      }
                    });
                  }),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    GetString();
  }

  Future GetString() async {
    sharedPreferences = await SharedPreferences.getInstance().then((sp) {
      token = sp.get(StringsFile.AccessToken);
      email = sp.get(StringsFile.Email);
      return sp;
    });
  }
}
