// To parse this JSON data, do
//
//     final packagesRatingRequest = packagesRatingRequestFromJson(jsonString);

import 'dart:convert';

PackageRatingRequest packagesRatingRequestFromJson(String str) => PackageRatingRequest.fromJson(json.decode(str));

String packagesRatingRequestToJson(PackageRatingRequest data) => json.encode(data.toJson());

class PackageRatingRequest {
  int packageId;
  int rating;

  PackageRatingRequest({
    this.packageId,
    this.rating,
  });

  factory PackageRatingRequest.fromJson(Map<String, dynamic> json) => PackageRatingRequest(
    packageId: json["packageId"] == null ? null : json["packageId"],
    rating: json["rating"] == null ? null : json["rating"],
  );

  Map<String, dynamic> toJson() => {
    "packageId": packageId == null ? null : packageId,
    "rating": rating == null ? null : rating,
  };
}
