// To parse this JSON data, do
//
//     final getQuestions = getQuestionsFromJson(jsonString);

import 'dart:convert';

GetQuestions getQuestionsFromJson(String str) => GetQuestions.fromJson(json.decode(str));

String getQuestionsToJson(GetQuestions data) => json.encode(data.toJson());

class GetQuestions {
  List<ListResult> listResult;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  dynamic links;
  List<dynamic> validationErrors;
  dynamic exception;

  GetQuestions({
    this.listResult,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory GetQuestions.fromJson(Map<String, dynamic> json) => GetQuestions(
    listResult: json["listResult"] == null ? null : List<ListResult>.from(json["listResult"].map((x) => ListResult.fromJson(x))),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"],
    validationErrors: json["validationErrors"] == null ? null : List<dynamic>.from(json["validationErrors"].map((x) => x)),
    exception: json["exception"],
  );

  Map<String, dynamic> toJson() => {
    "listResult": listResult == null ? null : List<dynamic>.from(listResult.map((x) => x.toJson())),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links,
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x)),
    "exception": exception,
  };
}

class ListResult {
  int id;
  String header;
  int groupId;
  int typeId;
  Question questionGroup;
  Question questionType;
  List<QuestionSet> questionSets;
  List<UserAnswer> userAnswers;

  ListResult({
    this.id,
    this.header,
    this.groupId,
    this.typeId,
    this.questionGroup,
    this.questionType,
    this.questionSets,
    this.userAnswers,
  });

  factory ListResult.fromJson(Map<String, dynamic> json) => ListResult(
    id: json["id"] == null ? null : json["id"],
    header: json["header"] == null ? null : json["header"],
    groupId: json["groupId"] == null ? null : json["groupId"],
    typeId: json["typeId"] == null ? null : json["typeId"],
    questionGroup: json["questionGroup"] == null ? null : Question.fromJson(json["questionGroup"]),
    questionType: json["questionType"] == null ? null : Question.fromJson(json["questionType"]),
    questionSets: json["questionSets"] == null ? null : List<QuestionSet>.from(json["questionSets"].map((x) => QuestionSet.fromJson(x))),
    userAnswers: json["userAnswers"] == null ? null : List<UserAnswer>.from(json["userAnswers"].map((x) => UserAnswer.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "header": header == null ? null : header,
    "groupId": groupId == null ? null : groupId,
    "typeId": typeId == null ? null : typeId,
    "questionGroup": questionGroup == null ? null : questionGroup.toJson(),
    "questionType": questionType == null ? null : questionType.toJson(),
    "questionSets": questionSets == null ? null : List<dynamic>.from(questionSets.map((x) => x.toJson())),
    "userAnswers": userAnswers == null ? null : List<dynamic>.from(userAnswers.map((x) => x.toJson())),
  };
}

class Question {
  int id;
  String name;

  Question({
    this.id,
    this.name,
  });

  factory Question.fromJson(Map<String, dynamic> json) => Question(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
  };
}

class QuestionSet {
  int id;
  String option;
  int questionId;

  QuestionSet({
    this.id,
    this.option,
    this.questionId,
  });

  factory QuestionSet.fromJson(Map<String, dynamic> json) => QuestionSet(
    id: json["id"] == null ? null : json["id"],
    option: json["option"] == null ? null : json["option"],
    questionId: json["questionId"] == null ? null : json["questionId"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "option": option == null ? null : option,
    "questionId": questionId == null ? null : questionId,
  };
}

class UserAnswer {
  int id;
  int questionId;
  String text;
  String userId;
  List<dynamic> userAnswerSets;

  UserAnswer({
    this.id,
    this.questionId,
    this.text,
    this.userId,
    this.userAnswerSets,
  });

  factory UserAnswer.fromJson(Map<String, dynamic> json) => UserAnswer(
    id: json["id"] == null ? null : json["id"],
    questionId: json["questionId"] == null ? null : json["questionId"],
    text: json["text"] == null ? null : json["text"],
    userId: json["userId"] == null ? null : json["userId"],
    userAnswerSets: json["userAnswerSets"] == null ? null : List<dynamic>.from(json["userAnswerSets"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "questionId": questionId == null ? null : questionId,
    "text": text == null ? null : text,
    "userId": userId == null ? null : userId,
    "userAnswerSets": userAnswerSets == null ? null : List<dynamic>.from(userAnswerSets.map((x) => x)),
  };
}
