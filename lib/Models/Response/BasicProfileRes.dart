// To parse this JSON data, do
//
//     final basicProfileRes = basicProfileResFromJson(jsonString);

import 'dart:convert';

BasicProfileRes basicProfileResFromJson(String str) => BasicProfileRes.fromJson(json.decode(str));

String basicProfileResToJson(BasicProfileRes data) => json.encode(data.toJson());

class BasicProfileRes {
  List<ListResult> listResult;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  List<Exception> links;
  List<ValidationError> validationErrors;
  Exception exception;

  BasicProfileRes({
    this.listResult,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory BasicProfileRes.fromJson(Map<String, dynamic> json) => BasicProfileRes(
    listResult: json["listResult"] == null ? null : List<ListResult>.from(json["listResult"].map((x) => ListResult.fromJson(x))),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"] == null ? null : List<Exception>.from(json["links"].map((x) => Exception.fromJson(x))),
    validationErrors: json["validationErrors"] == null ? null : List<ValidationError>.from(json["validationErrors"].map((x) => ValidationError.fromJson(x))),
    exception: json["exception"] == null ? null : Exception.fromJson(json["exception"]),
  );

  Map<String, dynamic> toJson() => {
    "listResult": listResult == null ? null : List<dynamic>.from(listResult.map((x) => x.toJson())),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links == null ? null : List<dynamic>.from(links.map((x) => x.toJson())),
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x.toJson())),
    "exception": exception == null ? null : exception.toJson(),
  };
}

class Exception {
  Exception();

  factory Exception.fromJson(Map<String, dynamic> json) => Exception(
  );

  Map<String, dynamic> toJson() => {
  };
}

class ListResult {
  int id;
  int questionId;
  String text;
  String userId;
  List<UserAnswerSet> userAnswerSets;

  ListResult({
    this.id,
    this.questionId,
    this.text,
    this.userId,
    this.userAnswerSets,
  });

  factory ListResult.fromJson(Map<String, dynamic> json) => ListResult(
    id: json["id"] == null ? null : json["id"],
    questionId: json["questionId"] == null ? null : json["questionId"],
    text: json["text"] == null ? null : json["text"],
    userId: json["userId"] == null ? null : json["userId"],
    userAnswerSets: json["userAnswerSets"] == null ? null : List<UserAnswerSet>.from(json["userAnswerSets"].map((x) => UserAnswerSet.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "questionId": questionId == null ? null : questionId,
    "text": text == null ? null : text,
    "userId": userId == null ? null : userId,
    "userAnswerSets": userAnswerSets == null ? null : List<dynamic>.from(userAnswerSets.map((x) => x.toJson())),
  };
}

class UserAnswerSet {
  int userAnswerId;
  int questionSetId;

  UserAnswerSet({
    this.userAnswerId,
    this.questionSetId,
  });

  factory UserAnswerSet.fromJson(Map<String, dynamic> json) => UserAnswerSet(
    userAnswerId: json["userAnswerId"] == null ? null : json["userAnswerId"],
    questionSetId: json["questionSetId"] == null ? null : json["questionSetId"],
  );

  Map<String, dynamic> toJson() => {
    "userAnswerId": userAnswerId == null ? null : userAnswerId,
    "questionSetId": questionSetId == null ? null : questionSetId,
  };
}

class ValidationError {
  String name;
  String description;

  ValidationError({
    this.name,
    this.description,
  });

  factory ValidationError.fromJson(Map<String, dynamic> json) => ValidationError(
    name: json["name"] == null ? null : json["name"],
    description: json["description"] == null ? null : json["description"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "description": description == null ? null : description,
  };
}
