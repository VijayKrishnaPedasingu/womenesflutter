// To parse this JSON data, do
//
//     final cancelAppointment = cancelAppointmentFromJson(jsonString);

import 'dart:convert';

CancelAppointment cancelAppointmentFromJson(String str) => CancelAppointment.fromJson(json.decode(str));

String cancelAppointmentToJson(CancelAppointment data) => json.encode(data.toJson());

class CancelAppointment {
  Result result;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  dynamic links;
  List<dynamic> validationErrors;
  dynamic exception;

  CancelAppointment({
    this.result,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory CancelAppointment.fromJson(Map<String, dynamic> json) => CancelAppointment(
    result: json["result"] == null ? null : Result.fromJson(json["result"]),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"],
    validationErrors: json["validationErrors"] == null ? null : List<dynamic>.from(json["validationErrors"].map((x) => x)),
    exception: json["exception"],
  );

  Map<String, dynamic> toJson() => {
    "result": result == null ? null : result.toJson(),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links,
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x)),
    "exception": exception,
  };
}

class Result {
  int id;
  String customerUserId;
  dynamic packageId;
  DateTime appointmentDate;
  String slotTime;
  String createdBy;
  dynamic modifiedBy;
  DateTime createdDate;
  DateTime modifiedDate;
  int statusId;
  dynamic status;
  int facilityId;
  dynamic facility;
  dynamic package;
  dynamic customerUser;
  dynamic services;

  Result({
    this.id,
    this.customerUserId,
    this.packageId,
    this.appointmentDate,
    this.slotTime,
    this.createdBy,
    this.modifiedBy,
    this.createdDate,
    this.modifiedDate,
    this.statusId,
    this.status,
    this.facilityId,
    this.facility,
    this.package,
    this.customerUser,
    this.services,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    customerUserId: json["customerUserId"] == null ? null : json["customerUserId"],
    packageId: json["packageId"],
    appointmentDate: json["appointmentDate"] == null ? null : DateTime.parse(json["appointmentDate"]),
    slotTime: json["slotTime"] == null ? null : json["slotTime"],
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    modifiedBy: json["modifiedBy"],
    createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
    modifiedDate: json["modifiedDate"] == null ? null : DateTime.parse(json["modifiedDate"]),
    statusId: json["statusId"] == null ? null : json["statusId"],
    status: json["status"],
    facilityId: json["facilityId"] == null ? null : json["facilityId"],
    facility: json["facility"],
    package: json["package"],
    customerUser: json["customerUser"],
    services: json["services"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "customerUserId": customerUserId == null ? null : customerUserId,
    "packageId": packageId,
    "appointmentDate": appointmentDate == null ? null : appointmentDate.toIso8601String(),
    "slotTime": slotTime == null ? null : slotTime,
    "createdBy": createdBy == null ? null : createdBy,
    "modifiedBy": modifiedBy,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
    "modifiedDate": modifiedDate == null ? null : modifiedDate.toIso8601String(),
    "statusId": statusId == null ? null : statusId,
    "status": status,
    "facilityId": facilityId == null ? null : facilityId,
    "facility": facility,
    "package": package,
    "customerUser": customerUser,
    "services": services,
  };
}
