import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Request/ServiceRatingRequest.dart';
import 'package:womenes/Models/Response/ServiceRatingResponse.dart';
import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/Utils/StringsFile.dart';

import '../Account/Login.dart';

class ServiceRating extends StatefulWidget {
  String image;
  String name;
  String desc;
  int ServiceId;

  ServiceRating(this.image, this.name, this.desc,this.ServiceId);

  @override
  _ServiceRatingState createState() => _ServiceRatingState(image, name, desc,ServiceId);
}

class _ServiceRatingState extends State<ServiceRating> {

  String image;
  String name;
  String desc;
  double feedback;
  bool showStars = true;

  SharedPreferences pref;
  var userId, token;

  var serviceId;


  _ServiceRatingState(this.image, this.name, this.desc,this.serviceId);


  @override
  void initState() {
    super.initState();
    getPrefs();
    feedback = 0;
  }

  getPrefs() async {

    pref = await SharedPreferences.getInstance();
    token = await pref.getString(StringsFile.AccessToken);
    userId = await pref.get(StringsFile.UserID);

    setState(() {
      if (token == null) {
        showStars = false;
      }
    });
    //  serviceId = await pref.get("ServiceId");

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor:Color(0xffff1493),
        title: Text("Services"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CircleAvatar(
              backgroundImage: image.contains('http:')?  NetworkImage(image):AssetImage(image),
              radius: 50,
            ),
            SizedBox(
              height: 15,
              width: double.infinity,

            ),
            Text(
              name,
              style: TextStyle(
                  fontSize: 18,
                  color: Color(0xffff1493),
                  fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              desc,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black45,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Visibility(
              visible: showStars,
              child: FlutterRatingBar(
                initialRating: 0,
                ignoreGestures: false,
                itemSize: 40,
                fillColor: Color(0xffff1493),
                borderColor: Colors.grey,
                allowHalfRating: true,
                onRatingUpdate: (rating) {
                  setState(() {
                    feedback = rating;
                  });
                },
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Visibility(
              visible: showStars,
              child: RaisedButton(
                onPressed: () async {
                  //  print(token);
                  print(" here is serive id $serviceId");
                  if (feedback != 0) {
                    ServiceRatingRequest request = ServiceRatingRequest(
                        rating: feedback, serviceId: serviceId);

                    final dio = Dio();
                    dio.options.headers["Authorization"] =
                    ("Bearer " + token);
                    final client = RestClient(dio);


                    ServiceRatingResponse response =
                    await client.sendFeedback(request).then((res) async {
                      Navigator.pop(context);
                      Fluttertoast.showToast(
                          msg: "Thanks for Your Feedback ",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIos: 3,
                          backgroundColor: Color(0xffff1493),
                          textColor: Colors.white,
                          fontSize: 16.0);
                      print(res.isSuccess);
                      return res;
                    }).catchError((Object obj) {
                      print(obj);
                      switch (obj.runtimeType) {
                        case DioError:
                          final res = (obj as DioError).response;

                          if (res.statusCode == 401) {
                            pref.clear();
                            pref.commit();
                            Navigator.pushReplacement(context,
                                MaterialPageRoute(
                                    builder: (context) => LoginPage()));
                            Fluttertoast.showToast(
                                msg: " Connection TimeOut..",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 3,
                                backgroundColor: Color(0xffff1493),
                                textColor: Colors.white,
                                fontSize: 16.0);
                          } else if (res.statusCode == 500) {
                            Fluttertoast.showToast(
                                msg: "Failed to connect Server",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 3,
                                backgroundColor: Color(0xffff1493),
                                textColor: Colors.white,
                                fontSize: 16.0);
                          }
                          else {
                            Fluttertoast.showToast(
                                msg: " Error Occured ..",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 3,
                                backgroundColor: Color(0xffff1493),
                                textColor: Colors.white,
                                fontSize: 16.0);
                          }
                      }
                    });
                  } else {
                    Fluttertoast.showToast(
                        msg: " Please give rating  ..",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIos: 3,
                        backgroundColor: Color(0xffff1493),
                        textColor: Colors.white,
                        fontSize: 16.0);
                  }
                },

                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                color: Color(0xffff1493),
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
