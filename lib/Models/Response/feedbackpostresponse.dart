// To parse this JSON data, do
//
//     final feedbackPostResponse = feedbackPostResponseFromJson(jsonString);

import 'dart:convert';

class FeedbackPostResponse {
  List<ListResult> listResult;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  List<Exception> links;
  List<ValidationError> validationErrors;
  Exception exception;

  FeedbackPostResponse({
    this.listResult,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory FeedbackPostResponse.fromRawJson(String str) => FeedbackPostResponse.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory FeedbackPostResponse.fromJson(Map<String, dynamic> json) => FeedbackPostResponse(
    listResult: json["listResult"] == null ? null : List<ListResult>.from(json["listResult"].map((x) => ListResult.fromJson(x))),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"] == null ? null : List<Exception>.from(json["links"].map((x) => Exception.fromJson(x))),
    validationErrors: json["validationErrors"] == null ? null : List<ValidationError>.from(json["validationErrors"].map((x) => ValidationError.fromJson(x))),
    exception: json["exception"] == null ? null : Exception.fromJson(json["exception"]),
  );

  Map<String, dynamic> toJson() => {
    "listResult": listResult == null ? null : List<dynamic>.from(listResult.map((x) => x.toJson())),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links == null ? null : List<dynamic>.from(links.map((x) => x.toJson())),
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x.toJson())),
    "exception": exception == null ? null : exception.toJson(),
  };
}

class Exception {
  Exception();

  factory Exception.fromRawJson(String str) => Exception.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Exception.fromJson(Map<String, dynamic> json) => Exception(
  );

  Map<String, dynamic> toJson() => {
  };
}

class ListResult {
  int id;
  String issuerId;
  Issuer issuer;
  String receiverId;
  dynamic createdDate;
  String description;
  List<FeedBack> feedBack;

  ListResult({
    this.id,
    this.issuerId,
    this.issuer,
    this.receiverId,
    this.createdDate,
    this.description,
    this.feedBack,
  });

  factory ListResult.fromRawJson(String str) => ListResult.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ListResult.fromJson(Map<String, dynamic> json) => ListResult(
    id: json["id"] == null ? null : json["id"],
    issuerId: json["issuerId"] == null ? null : json["issuerId"],
    issuer: json["issuer"] == null ? null : Issuer.fromJson(json["issuer"]),
    receiverId: json["receiverId"] == null ? null : json["receiverId"],
    createdDate: json["createdDate"] == null ? null :(json["createdDate"]),
    description: json["description"] == null ? null : json["description"],
    feedBack: json["feedBack"] == null ? null : List<FeedBack>.from(json["feedBack"].map((x) => FeedBack.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "issuerId": issuerId == null ? null : issuerId,
    "issuer": issuer == null ? null : issuer.toJson(),
    "receiverId": receiverId == null ? null : receiverId,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
    "description": description == null ? null : description,
    "feedBack": feedBack == null ? null : List<dynamic>.from(feedBack.map((x) => x.toJson())),
  };
}

class FeedBack {
  int id;
  int parameterId;
  Parameter parameter;
  double rating;
  int userFeedbackId;

  FeedBack({
    this.id,
    this.parameterId,
    this.parameter,
    this.rating,
    this.userFeedbackId,
  });

  factory FeedBack.fromRawJson(String str) => FeedBack.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory FeedBack.fromJson(Map<String, dynamic> json) => FeedBack(
    id: json["id"] == null ? null : json["id"],
    parameterId: json["parameterId"] == null ? null : json["parameterId"],
    parameter: json["parameter"] == null ? null : Parameter.fromJson(json["parameter"]),
    rating: json["rating"] == null ? null : json["rating"],
    userFeedbackId: json["userFeedbackId"] == null ? null : json["userFeedbackId"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "parameterId": parameterId == null ? null : parameterId,
    "parameter": parameter == null ? null : parameter.toJson(),
    "rating": rating == null ? null : rating,
    "userFeedbackId": userFeedbackId == null ? null : userFeedbackId,
  };
}

class Parameter {
  int id;
  String parameter;

  Parameter({
    this.id,
    this.parameter,
  });

  factory Parameter.fromRawJson(String str) => Parameter.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Parameter.fromJson(Map<String, dynamic> json) => Parameter(
    id: json["id"] == null ? null : json["id"],
    parameter: json["parameter"] == null ? null : json["parameter"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "parameter": parameter == null ? null : parameter,
  };
}

class Issuer {
  String firstName;
  String lastName;
  int rating;
  String referalCode;
  int referalBonus;
  String referredBy;
  double latitude;
  double longitude;
  String studentRollNumber;
  String collegeName;
  String course;
  bool areYouStudent;
  bool goddess;
  String id;
  String userName;
  String normalizedUserName;
  String email;
  String normalizedEmail;
  bool emailConfirmed;
  String passwordHash;
  String securityStamp;
  String concurrencyStamp;
  String phoneNumber;
  bool phoneNumberConfirmed;
  bool twoFactorEnabled;
  DateTime lockoutEnd;
  bool lockoutEnabled;
  int accessFailedCount;

  Issuer({
    this.firstName,
    this.lastName,
    this.rating,
    this.referalCode,
    this.referalBonus,
    this.referredBy,
    this.latitude,
    this.longitude,
    this.studentRollNumber,
    this.collegeName,
    this.course,
    this.areYouStudent,
    this.goddess,
    this.id,
    this.userName,
    this.normalizedUserName,
    this.email,
    this.normalizedEmail,
    this.emailConfirmed,
    this.passwordHash,
    this.securityStamp,
    this.concurrencyStamp,
    this.phoneNumber,
    this.phoneNumberConfirmed,
    this.twoFactorEnabled,
    this.lockoutEnd,
    this.lockoutEnabled,
    this.accessFailedCount,
  });

  factory Issuer.fromRawJson(String str) => Issuer.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Issuer.fromJson(Map<String, dynamic> json) => Issuer(
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    rating: json["rating"] == null ? null : json["rating"],
    referalCode: json["referalCode"] == null ? null : json["referalCode"],
    referalBonus: json["referalBonus"] == null ? null : json["referalBonus"],
    referredBy: json["referredBy"] == null ? null : json["referredBy"],
    latitude: json["latitude"] == null ? null : json["latitude"],
    longitude: json["longitude"] == null ? null : json["longitude"],
    studentRollNumber: json["studentRollNumber"] == null ? null : json["studentRollNumber"],
    collegeName: json["collegeName"] == null ? null : json["collegeName"],
    course: json["course"] == null ? null : json["course"],
    areYouStudent: json["are_You_Student"] == null ? null : json["are_You_Student"],
    goddess: json["goddess"] == null ? null : json["goddess"],
    id: json["id"] == null ? null : json["id"],
    userName: json["userName"] == null ? null : json["userName"],
    normalizedUserName: json["normalizedUserName"] == null ? null : json["normalizedUserName"],
    email: json["email"] == null ? null : json["email"],
    normalizedEmail: json["normalizedEmail"] == null ? null : json["normalizedEmail"],
    emailConfirmed: json["emailConfirmed"] == null ? null : json["emailConfirmed"],
    passwordHash: json["passwordHash"] == null ? null : json["passwordHash"],
    securityStamp: json["securityStamp"] == null ? null : json["securityStamp"],
    concurrencyStamp: json["concurrencyStamp"] == null ? null : json["concurrencyStamp"],
    phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
    phoneNumberConfirmed: json["phoneNumberConfirmed"] == null ? null : json["phoneNumberConfirmed"],
    twoFactorEnabled: json["twoFactorEnabled"] == null ? null : json["twoFactorEnabled"],
    lockoutEnd: json["lockoutEnd"] == null ? null : DateTime.parse(json["lockoutEnd"]),
    lockoutEnabled: json["lockoutEnabled"] == null ? null : json["lockoutEnabled"],
    accessFailedCount: json["accessFailedCount"] == null ? null : json["accessFailedCount"],
  );

  Map<String, dynamic> toJson() => {
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "rating": rating == null ? null : rating,
    "referalCode": referalCode == null ? null : referalCode,
    "referalBonus": referalBonus == null ? null : referalBonus,
    "referredBy": referredBy == null ? null : referredBy,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
    "studentRollNumber": studentRollNumber == null ? null : studentRollNumber,
    "collegeName": collegeName == null ? null : collegeName,
    "course": course == null ? null : course,
    "are_You_Student": areYouStudent == null ? null : areYouStudent,
    "goddess": goddess == null ? null : goddess,
    "id": id == null ? null : id,
    "userName": userName == null ? null : userName,
    "normalizedUserName": normalizedUserName == null ? null : normalizedUserName,
    "email": email == null ? null : email,
    "normalizedEmail": normalizedEmail == null ? null : normalizedEmail,
    "emailConfirmed": emailConfirmed == null ? null : emailConfirmed,
    "passwordHash": passwordHash == null ? null : passwordHash,
    "securityStamp": securityStamp == null ? null : securityStamp,
    "concurrencyStamp": concurrencyStamp == null ? null : concurrencyStamp,
    "phoneNumber": phoneNumber == null ? null : phoneNumber,
    "phoneNumberConfirmed": phoneNumberConfirmed == null ? null : phoneNumberConfirmed,
    "twoFactorEnabled": twoFactorEnabled == null ? null : twoFactorEnabled,
    "lockoutEnd": lockoutEnd == null ? null : lockoutEnd.toIso8601String(),
    "lockoutEnabled": lockoutEnabled == null ? null : lockoutEnabled,
    "accessFailedCount": accessFailedCount == null ? null : accessFailedCount,
  };
}

class ValidationError {
  String name;
  String description;

  ValidationError({
    this.name,
    this.description,
  });

  factory ValidationError.fromRawJson(String str) => ValidationError.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ValidationError.fromJson(Map<String, dynamic> json) => ValidationError(
    name: json["name"] == null ? null : json["name"],
    description: json["description"] == null ? null : json["description"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "description": description == null ? null : description,
  };
}
