// To parse this JSON data, do
//
//     final feedbackPostRequest = feedbackPostRequestFromJson(jsonString);

import 'dart:convert';

class FeedbackPostRequest {
  String receiverId;
  List<FeedBack> feedBack;

  FeedbackPostRequest({
    this.receiverId,
    this.feedBack,
  });

  factory FeedbackPostRequest.fromRawJson(String str) => FeedbackPostRequest.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory FeedbackPostRequest.fromJson(Map<String, dynamic> json) => FeedbackPostRequest(
    receiverId: json["receiverId"] == null ? null : json["receiverId"],
    feedBack: json["feedBack"] == null ? null : List<FeedBack>.from(json["feedBack"].map((x) => FeedBack.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "receiverId": receiverId == null ? null : receiverId,
    "feedBack": feedBack == null ? null : List<dynamic>.from(feedBack.map((x) => x.toJson())),
  };
}

class FeedBack {
  String parameterId;
  String rating;

  FeedBack({
    this.parameterId,
    this.rating,
  });

  factory FeedBack.fromRawJson(String str) => FeedBack.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory FeedBack.fromJson(Map<String, dynamic> json) => FeedBack(
    parameterId: json["parameterId"] == null ? null : json["parameterId"],
    rating: json["rating"] == null ? null : json["rating"],
  );

  Map<String, dynamic> toJson() => {
    "parameterId": parameterId == null ? null : parameterId,
    "rating": rating == null ? null : rating,
  };
}
