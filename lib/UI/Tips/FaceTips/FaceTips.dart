import 'package:flutter/material.dart';
import 'package:womenes/UI/Tips/FaceTips/FaceTipstabbar.dart';

class FaceTips extends StatefulWidget {
  @override
  _FaceTipsState createState() => _FaceTipsState();
}

class _FaceTipsState extends State<FaceTips> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tips'),
        backgroundColor:Color(0xffff1493),
      ),

      body: ListView(
      children: <Widget>[
        Card(
          elevation: 3.0,

          child: ListTile(
            leading: Icon(Icons.video_library,color: Color(0xffff1493),),
            title: Text('Acene & Pimple'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(0)));
            },),),
        Card(
          elevation: 3.0,

          child: ListTile(
              leading: Icon(Icons.video_library,color: Color(0xffff1493),),
            title: Text('Pigmentation'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(1)));
            },),),
        Card(
          elevation: 3.0,

          child: ListTile(
              leading: Icon(Icons.video_library,color: Color(0xffff1493),),
            title: Text('Pits & Open Pores '),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(2)));
            },),),
        Card(
          elevation: 3.0,

          child: ListTile(
              leading: Icon(Icons.video_library,color: Color(0xffff1493),),
            title: Text('Black Heads & white Heads'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(3)));
            },),),

        Card(
          elevation: 3.0,

          child: ListTile(
              leading: Icon(Icons.video_library,color: Color(0xffff1493),),
            title: Text('Dark Lips'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(4)));
            },),),
        Card(
          elevation: 3.0,

          child: ListTile(
              leading: Icon(Icons.video_library,color: Color(0xffff1493),),
            title: Text('Wrinkles'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(5)));
            },),),
        Card(
          elevation: 3.0,

          child: ListTile(
              leading: Icon(Icons.video_library,color: Color(0xffff1493),),
            title: Text('Color Improvement'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(6)));
            },),),
        Card(
          elevation: 3.0,

          child: ListTile(
              leading: Icon(Icons.video_library,color: Color(0xffff1493),),
            title: Text('Dark Circles'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(7)));
            },),),
        Card(
          elevation: 3.0,

          child: ListTile(
              leading: Icon(Icons.video_library,color:Color(0xffff1493),),
            title: Text('Uneven Skin Tone'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(8)));
            },),),
        Card(
          elevation: 3.0,

          child: ListTile(
              leading: Icon(Icons.video_library,color: Color(0xffff1493),),
            title: Text('Excessive or Unwanted hair growth on face'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(9)));
            },),),
        Card(
          elevation: 3.0,

          child: ListTile(
              leading: Icon(Icons.video_library,color: Color(0xffff1493),),
            title: Text('Dark Neck'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FaceTipstabbar(10)));
            },),),


      ],
      ),
    );
  }
}
