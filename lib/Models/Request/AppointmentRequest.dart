// To parse this JSON data, do
//
//     final appointmentRequest = appointmentRequestFromJson(jsonString);

import 'dart:convert';

AppointmentRequest appointmentRequestFromJson(String str) => AppointmentRequest.fromJson(json.decode(str));

String appointmentRequestToJson(AppointmentRequest data) => json.encode(data.toJson());

class AppointmentRequest {
  String customerUserId;
  DateTime appointmentDate;
  String slotTime;
  int facilityId;

  AppointmentRequest({
    this.customerUserId,
    this.appointmentDate,
    this.slotTime,
    this.facilityId,
  });

  factory AppointmentRequest.fromJson(Map<String, dynamic> json) => AppointmentRequest(
    customerUserId: json["customerUserId"] == null ? null : json["customerUserId"],
    appointmentDate: json["appointmentDate"] == null ? null : DateTime.parse(json["appointmentDate"]),
    slotTime: json["slotTime"] == null ? null : json["slotTime"],
    facilityId: json["facilityId"] == null ? null : json["facilityId"],
  );

  Map<String, dynamic> toJson() => {
    "customerUserId": customerUserId == null ? null : customerUserId,
    "appointmentDate": appointmentDate == null ? null : appointmentDate.toIso8601String(),
    "slotTime": slotTime == null ? null : slotTime,
    "facilityId": facilityId == null ? null : facilityId,
  };
}
