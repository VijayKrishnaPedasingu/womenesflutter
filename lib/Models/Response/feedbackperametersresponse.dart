// To parse this JSON data, do
//
//     final feedbackresponse = feedbackresponseFromJson(jsonString);

import 'dart:convert';

class Feedbackresponse {
  List<ListResult> listResult;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  dynamic links;
  List<dynamic> validationErrors;
  dynamic exception;

  Feedbackresponse({
    this.listResult,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory Feedbackresponse.fromRawJson(String str) => Feedbackresponse.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Feedbackresponse.fromJson(Map<String, dynamic> json) => Feedbackresponse(
    listResult: json["listResult"] == null ? null : List<ListResult>.from(json["listResult"].map((x) => ListResult.fromJson(x))),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"],
    validationErrors: json["validationErrors"] == null ? null : List<dynamic>.from(json["validationErrors"].map((x) => x)),
    exception: json["exception"],
  );

  Map<String, dynamic> toJson() => {
    "listResult": listResult == null ? null : List<dynamic>.from(listResult.map((x) => x.toJson())),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links,
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x)),
    "exception": exception,
  };
}

class ListResult {
  int id;
  String parameter;

  ListResult({
    this.id,
    this.parameter,
  });

  factory ListResult.fromRawJson(String str) => ListResult.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ListResult.fromJson(Map<String, dynamic> json) => ListResult(
    id: json["id"] == null ? null : json["id"],
    parameter: json["parameter"] == null ? null : json["parameter"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "parameter": parameter == null ? null : parameter,
  };
}
