// To parse this JSON data, do
//
//     final getServiceTimings = getServiceTimingsFromJson(jsonString);

import 'dart:convert';

GetServiceTimings getServiceTimingsFromJson(String str) => GetServiceTimings.fromJson(json.decode(str));

String getServiceTimingsToJson(GetServiceTimings data) => json.encode(data.toJson());

class GetServiceTimings {
  List<ListResult> listResult;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  dynamic links;
  List<dynamic> validationErrors;
  dynamic exception;

  GetServiceTimings({
    this.listResult,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory GetServiceTimings.fromJson(Map<String, dynamic> json) => GetServiceTimings(
    listResult: json["listResult"] == null ? null : List<ListResult>.from(json["listResult"].map((x) => ListResult.fromJson(x))),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"],
    validationErrors: json["validationErrors"] == null ? null : List<dynamic>.from(json["validationErrors"].map((x) => x)),
    exception: json["exception"],
  );

  Map<String, dynamic> toJson() => {
    "listResult": listResult == null ? null : List<dynamic>.from(listResult.map((x) => x.toJson())),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links,
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x)),
    "exception": exception,
  };
}

class ListResult {
  int packageId;
  List<Timing> timings;
  List<dynamic> services;
  List<dynamic> holidays;

  ListResult({
    this.packageId,
    this.timings,
    this.services,
    this.holidays,
  });

  factory ListResult.fromJson(Map<String, dynamic> json) => ListResult(
    packageId: json["packageId"] == null ? null : json["packageId"],
    timings: json["timings"] == null ? null : List<Timing>.from(json["timings"].map((x) => Timing.fromJson(x))),
    services: json["services"] == null ? null : List<dynamic>.from(json["services"].map((x) => x)),
    holidays: json["holidays"] == null ? null : List<dynamic>.from(json["holidays"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "packageId": packageId == null ? null : packageId,
    "timings": timings == null ? null : List<dynamic>.from(timings.map((x) => x.toJson())),
    "services": services == null ? null : List<dynamic>.from(services.map((x) => x)),
    "holidays": holidays == null ? null : List<dynamic>.from(holidays.map((x) => x)),
  };
}

class Timing {
  String time;
  bool avaliability;
  Status status;
  dynamic facilityName;

  Timing({
    this.time,
    this.avaliability,
    this.status,
    this.facilityName,
  });

  factory Timing.fromJson(Map<String, dynamic> json) => Timing(
    time: json["time"] == null ? null : json["time"],
    avaliability: json["avaliability"] == null ? null : json["avaliability"],
    status: json["status"] == null ? null : statusValues.map[json["status"]],
    facilityName: json["facilityName"],
  );

  Map<String, dynamic> toJson() => {
    "time": time == null ? null : time,
    "avaliability": avaliability == null ? null : avaliability,
    "status": status == null ? null : statusValues.reverse[status],
    "facilityName": facilityName,
  };
}

enum Status { NOT_FILLED }

final statusValues = EnumValues({
  "not filled": Status.NOT_FILLED
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
