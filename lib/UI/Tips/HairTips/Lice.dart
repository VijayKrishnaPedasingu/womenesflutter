import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:womenes/Models/Response/TipsResponseById.dart';

import '../../../Services/RetrofitServices.dart';

class Lice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder(
            future: getdata(),
            builder: (context, snapshot) {
              TipsResponsebyId result = snapshot.data;
              if (snapshot.data != null) {
                return ListView.builder(
                    shrinkWrap: true,
                    itemCount: result.listResult.length,
                    itemBuilder: (context, index) {
                      return Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: Card(
                              color: Colors.white,
                              elevation: 2.0,
                              child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Column(children: <Widget>[
                                    CircleAvatar(
                                      backgroundImage: NetworkImage(result
                                              .listResult[index]
                                              .typesInfo[0]
                                              .fileLocation +
                                          result.listResult[index].typesInfo[0]
                                              .fileName +
                                          result.listResult[index].typesInfo[0]
                                              .fileExtension),
                                      radius: 50.0,
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Text(
                                      result
                                          .listResult[index].typesInfo[0].title,
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    SizedBox(
                                      height: 15.0,
                                    ),
                                    Text(result.listResult[index].typesInfo[0]
                                        .description)
                                  ]))));
                    });
              } else {
                return Center(
                    child: CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Color(0xffff1493))));
              }
            }));
  }

  Future getdata() async {
    final dio = Dio();

    final client = RestClient(dio);
    TipsResponsebyId data = await client.getTips(21).then((response) {
      return response;
    });
    return data;
  }
}
