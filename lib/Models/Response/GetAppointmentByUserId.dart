// To parse this JSON data, do
//
//     final getAppointmentsByuserId = getAppointmentsByuserIdFromJson(jsonString);

import 'dart:convert';

GetAppointmentsByUserId getAppointmentsByuserIdFromJson(String str) => GetAppointmentsByUserId.fromJson(json.decode(str));

String getAppointmentsByuserIdToJson(GetAppointmentsByUserId data) => json.encode(data.toJson());

class GetAppointmentsByUserId {
  List<ListResult> listResult;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  List<Exception> links;
  List<ValidationError> validationErrors;
  Exception exception;

  GetAppointmentsByUserId({
    this.listResult,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory GetAppointmentsByUserId.fromJson(Map<String, dynamic> json) => GetAppointmentsByUserId(
    listResult: json["listResult"] == null ? null : List<ListResult>.from(json["listResult"].map((x) => ListResult.fromJson(x))),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"] == null ? null : List<Exception>.from(json["links"].map((x) => Exception.fromJson(x))),
    validationErrors: json["validationErrors"] == null ? null : List<ValidationError>.from(json["validationErrors"].map((x) => ValidationError.fromJson(x))),
    exception: json["exception"] == null ? null : Exception.fromJson(json["exception"]),
  );

  Map<String, dynamic> toJson() => {
    "listResult": listResult == null ? null : List<dynamic>.from(listResult.map((x) => x.toJson())),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links == null ? null : List<dynamic>.from(links.map((x) => x.toJson())),
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x.toJson())),
    "exception": exception == null ? null : exception.toJson(),
  };
}

class Exception {
  Exception();

  factory Exception.fromJson(Map<String, dynamic> json) => Exception(
  );

  Map<String, dynamic> toJson() => {
  };
}

class ListResult {
  int id;
  String customerUserId;
  int packageId;
  DateTime appointmentDate;
  String slotTime;
  String createdBy;
  String modifiedBy;
  DateTime createdDate;
  DateTime modifiedDate;
  int statusId;
  Status status;
  int facilityId;
  Facility facility;
  Package package;
  CustomerUser customerUser;
  List<ServiceElement> services;

  ListResult({
    this.id,
    this.customerUserId,
    this.packageId,
    this.appointmentDate,
    this.slotTime,
    this.createdBy,
    this.modifiedBy,
    this.createdDate,
    this.modifiedDate,
    this.statusId,
    this.status,
    this.facilityId,
    this.facility,
    this.package,
    this.customerUser,
    this.services,
  });

  factory ListResult.fromJson(Map<String, dynamic> json) => ListResult(
    id: json["id"] == null ? null : json["id"],
    customerUserId: json["customerUserId"] == null ? null : json["customerUserId"],
    packageId: json["packageId"] == null ? null : json["packageId"],
    appointmentDate: json["appointmentDate"] == null ? null : DateTime.parse(json["appointmentDate"]),
    slotTime: json["slotTime"] == null ? null : json["slotTime"],
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    modifiedBy: json["modifiedBy"] == null ? null : json["modifiedBy"],
    createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
    modifiedDate: json["modifiedDate"] == null ? null : DateTime.parse(json["modifiedDate"]),
    statusId: json["statusId"] == null ? null : json["statusId"],
    status: json["status"] == null ? null : Status.fromJson(json["status"]),
    facilityId: json["facilityId"] == null ? null : json["facilityId"],
    facility: json["facility"] == null ? null : Facility.fromJson(json["facility"]),
    package: json["package"] == null ? null : Package.fromJson(json["package"]),
    customerUser: json["customerUser"] == null ? null : CustomerUser.fromJson(json["customerUser"]),
    services: json["services"] == null ? null : List<ServiceElement>.from(json["services"].map((x) => ServiceElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "customerUserId": customerUserId == null ? null : customerUserId,
    "packageId": packageId == null ? null : packageId,
    "appointmentDate": appointmentDate == null ? null : appointmentDate.toIso8601String(),
    "slotTime": slotTime == null ? null : slotTime,
    "createdBy": createdBy == null ? null : createdBy,
    "modifiedBy": modifiedBy == null ? null : modifiedBy,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
    "modifiedDate": modifiedDate == null ? null : modifiedDate.toIso8601String(),
    "statusId": statusId == null ? null : statusId,
    "status": status == null ? null : status.toJson(),
    "facilityId": facilityId == null ? null : facilityId,
    "facility": facility == null ? null : facility.toJson(),
    "package": package == null ? null : package.toJson(),
    "customerUser": customerUser == null ? null : customerUser.toJson(),
    "services": services == null ? null : List<dynamic>.from(services.map((x) => x.toJson())),
  };
}

class CustomerUser {
  String firstName;
  String lastName;
  int rating;
  String referalCode;
  int referalBonus;
  String referredBy;
  int latitude;
  int longitude;
  String studentRollNumber;
  String collegeName;
  String course;
  bool areYouStudent;
  bool goddess;
  String id;
  String userName;
  String normalizedUserName;
  String email;
  String normalizedEmail;
  bool emailConfirmed;
  String passwordHash;
  String securityStamp;
  String concurrencyStamp;
  String phoneNumber;
  bool phoneNumberConfirmed;
  bool twoFactorEnabled;
  DateTime lockoutEnd;
  bool lockoutEnabled;
  int accessFailedCount;

  CustomerUser({
    this.firstName,
    this.lastName,
    this.rating,
    this.referalCode,
    this.referalBonus,
    this.referredBy,
    this.latitude,
    this.longitude,
    this.studentRollNumber,
    this.collegeName,
    this.course,
    this.areYouStudent,
    this.goddess,
    this.id,
    this.userName,
    this.normalizedUserName,
    this.email,
    this.normalizedEmail,
    this.emailConfirmed,
    this.passwordHash,
    this.securityStamp,
    this.concurrencyStamp,
    this.phoneNumber,
    this.phoneNumberConfirmed,
    this.twoFactorEnabled,
    this.lockoutEnd,
    this.lockoutEnabled,
    this.accessFailedCount,
  });

  factory CustomerUser.fromJson(Map<String, dynamic> json) => CustomerUser(
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    rating: json["rating"] == null ? null : json["rating"],
    referalCode: json["referalCode"] == null ? null : json["referalCode"],
    referalBonus: json["referalBonus"] == null ? null : json["referalBonus"],
    referredBy: json["referredBy"] == null ? null : json["referredBy"],
    latitude: json["latitude"] == null ? null : json["latitude"],
    longitude: json["longitude"] == null ? null : json["longitude"],
    studentRollNumber: json["studentRollNumber"] == null ? null : json["studentRollNumber"],
    collegeName: json["collegeName"] == null ? null : json["collegeName"],
    course: json["course"] == null ? null : json["course"],
    areYouStudent: json["are_You_Student"] == null ? null : json["are_You_Student"],
    goddess: json["goddess"] == null ? null : json["goddess"],
    id: json["id"] == null ? null : json["id"],
    userName: json["userName"] == null ? null : json["userName"],
    normalizedUserName: json["normalizedUserName"] == null ? null : json["normalizedUserName"],
    email: json["email"] == null ? null : json["email"],
    normalizedEmail: json["normalizedEmail"] == null ? null : json["normalizedEmail"],
    emailConfirmed: json["emailConfirmed"] == null ? null : json["emailConfirmed"],
    passwordHash: json["passwordHash"] == null ? null : json["passwordHash"],
    securityStamp: json["securityStamp"] == null ? null : json["securityStamp"],
    concurrencyStamp: json["concurrencyStamp"] == null ? null : json["concurrencyStamp"],
    phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
    phoneNumberConfirmed: json["phoneNumberConfirmed"] == null ? null : json["phoneNumberConfirmed"],
    twoFactorEnabled: json["twoFactorEnabled"] == null ? null : json["twoFactorEnabled"],
    lockoutEnd: json["lockoutEnd"] == null ? null : DateTime.parse(json["lockoutEnd"]),
    lockoutEnabled: json["lockoutEnabled"] == null ? null : json["lockoutEnabled"],
    accessFailedCount: json["accessFailedCount"] == null ? null : json["accessFailedCount"],
  );

  Map<String, dynamic> toJson() => {
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "rating": rating == null ? null : rating,
    "referalCode": referalCode == null ? null : referalCode,
    "referalBonus": referalBonus == null ? null : referalBonus,
    "referredBy": referredBy == null ? null : referredBy,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
    "studentRollNumber": studentRollNumber == null ? null : studentRollNumber,
    "collegeName": collegeName == null ? null : collegeName,
    "course": course == null ? null : course,
    "are_You_Student": areYouStudent == null ? null : areYouStudent,
    "goddess": goddess == null ? null : goddess,
    "id": id == null ? null : id,
    "userName": userName == null ? null : userName,
    "normalizedUserName": normalizedUserName == null ? null : normalizedUserName,
    "email": email == null ? null : email,
    "normalizedEmail": normalizedEmail == null ? null : normalizedEmail,
    "emailConfirmed": emailConfirmed == null ? null : emailConfirmed,
    "passwordHash": passwordHash == null ? null : passwordHash,
    "securityStamp": securityStamp == null ? null : securityStamp,
    "concurrencyStamp": concurrencyStamp == null ? null : concurrencyStamp,
    "phoneNumber": phoneNumber == null ? null : phoneNumber,
    "phoneNumberConfirmed": phoneNumberConfirmed == null ? null : phoneNumberConfirmed,
    "twoFactorEnabled": twoFactorEnabled == null ? null : twoFactorEnabled,
    "lockoutEnd": lockoutEnd == null ? null : lockoutEnd.toIso8601String(),
    "lockoutEnabled": lockoutEnabled == null ? null : lockoutEnabled,
    "accessFailedCount": accessFailedCount == null ? null : accessFailedCount,
  };
}

class Facility {
  int id;
  String facilityName;
  String address;
  String contactNo;
  String createdBy;
  String modifiedBy;
  DateTime createdDate;
  DateTime modifiedDate;
  bool isActive;

  Facility({
    this.id,
    this.facilityName,
    this.address,
    this.contactNo,
    this.createdBy,
    this.modifiedBy,
    this.createdDate,
    this.modifiedDate,
    this.isActive,
  });

  factory Facility.fromJson(Map<String, dynamic> json) => Facility(
    id: json["id"] == null ? null : json["id"],
    facilityName: json["facilityName"] == null ? null : json["facilityName"],
    address: json["address"] == null ? null : json["address"],
    contactNo: json["contactNo"] == null ? null : json["contactNo"],
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    modifiedBy: json["modifiedBy"] == null ? null : json["modifiedBy"],
    createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
    modifiedDate: json["modifiedDate"] == null ? null : DateTime.parse(json["modifiedDate"]),
    isActive: json["isActive"] == null ? null : json["isActive"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "facilityName": facilityName == null ? null : facilityName,
    "address": address == null ? null : address,
    "contactNo": contactNo == null ? null : contactNo,
    "createdBy": createdBy == null ? null : createdBy,
    "modifiedBy": modifiedBy == null ? null : modifiedBy,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
    "modifiedDate": modifiedDate == null ? null : modifiedDate.toIso8601String(),
    "isActive": isActive == null ? null : isActive,
  };
}

class Package {
  int id;
  String duration;
  String name;
  dynamic totalAmount;
  int taxPercentage;
  String description;
  bool isActive;
  String fileName;
  String fileExtension;
  String fileLocation;
  dynamic avgRating;
  String createdBy;
  String modifiedBy;
  DateTime createdDate;
  DateTime modifiedDate;
  dynamic discountPercentageUptoOrFlatAmount;
  int discountTypeId;
  DiscountType discountType;
  String packageCode;
  List<PackageService> packageServices;

  Package({
    this.id,
    this.duration,
    this.name,
    this.totalAmount,
    this.taxPercentage,
    this.description,
    this.isActive,
    this.fileName,
    this.fileExtension,
    this.fileLocation,
    this.avgRating,
    this.createdBy,
    this.modifiedBy,
    this.createdDate,
    this.modifiedDate,
    this.discountPercentageUptoOrFlatAmount,
    this.discountTypeId,
    this.discountType,
    this.packageCode,
    this.packageServices,
  });

  factory Package.fromJson(Map<String, dynamic> json) => Package(
    id: json["id"] == null ? null : json["id"],
    duration: json["duration"] == null ? null : json["duration"],
    name: json["name"] == null ? null : json["name"],
    totalAmount: json["totalAmount"] == null ? null : json["totalAmount"],
    taxPercentage: json["taxPercentage"] == null ? null : json["taxPercentage"],
    description: json["description"] == null ? null : json["description"],
    isActive: json["isActive"] == null ? null : json["isActive"],
    fileName: json["fileName"] == null ? null : json["fileName"],
    fileExtension: json["fileExtension"] == null ? null : json["fileExtension"],
    fileLocation: json["fileLocation"] == null ? null : json["fileLocation"],
    avgRating: json["avgRating"] == null ? null : json["avgRating"],
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    modifiedBy: json["modifiedBy"] == null ? null : json["modifiedBy"],
    createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
    modifiedDate: json["modifiedDate"] == null ? null : DateTime.parse(json["modifiedDate"]),
    discountPercentageUptoOrFlatAmount: json["discountPercentageUptoOrFlatAmount"] == null ? null : json["discountPercentageUptoOrFlatAmount"],
    discountTypeId: json["discountTypeId"] == null ? null : json["discountTypeId"],
    discountType: json["discountType"] == null ? null : DiscountType.fromJson(json["discountType"]),
    packageCode: json["packageCode"] == null ? null : json["packageCode"],
    packageServices: json["packageServices"] == null ? null : List<PackageService>.from(json["packageServices"].map((x) => PackageService.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "duration": duration == null ? null : duration,
    "name": name == null ? null : name,
    "totalAmount": totalAmount == null ? null : totalAmount,
    "taxPercentage": taxPercentage == null ? null : taxPercentage,
    "description": description == null ? null : description,
    "isActive": isActive == null ? null : isActive,
    "fileName": fileName == null ? null : fileName,
    "fileExtension": fileExtension == null ? null : fileExtension,
    "fileLocation": fileLocation == null ? null : fileLocation,
    "avgRating": avgRating == null ? null : avgRating,
    "createdBy": createdBy == null ? null : createdBy,
    "modifiedBy": modifiedBy == null ? null : modifiedBy,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
    "modifiedDate": modifiedDate == null ? null : modifiedDate.toIso8601String(),
    "discountPercentageUptoOrFlatAmount": discountPercentageUptoOrFlatAmount == null ? null : discountPercentageUptoOrFlatAmount,
    "discountTypeId": discountTypeId == null ? null : discountTypeId,
    "discountType": discountType == null ? null : discountType.toJson(),
    "packageCode": packageCode == null ? null : packageCode,
    "packageServices": packageServices == null ? null : List<dynamic>.from(packageServices.map((x) => x.toJson())),
  };
}

class DiscountType {
  int id;
  String discounttype;

  DiscountType({
    this.id,
    this.discounttype,
  });

  factory DiscountType.fromJson(Map<String, dynamic> json) => DiscountType(
    id: json["id"] == null ? null : json["id"],
    discounttype: json["discounttype"] == null ? null : json["discounttype"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "discounttype": discounttype == null ? null : discounttype,
  };
}

class PackageService {
  int packageId;
  int serviceId;
  PackageServiceService service;

  PackageService({
    this.packageId,
    this.serviceId,
    this.service,
  });

  factory PackageService.fromJson(Map<String, dynamic> json) => PackageService(
    packageId: json["packageId"] == null ? null : json["packageId"],
    serviceId: json["serviceId"] == null ? null : json["serviceId"],
    service: json["service"] == null ? null : PackageServiceService.fromJson(json["service"]),
  );

  Map<String, dynamic> toJson() => {
    "packageId": packageId == null ? null : packageId,
    "serviceId": serviceId == null ? null : serviceId,
    "service": service == null ? null : service.toJson(),
  };
}

class PackageServiceService {
  int id;
  String serviceName;
  String serviceCode;
  String description;
  dynamic avgRating;
  String createdBy;
  String modifiedBy;
  DateTime createdDate;
  DateTime modifiedDate;
  dynamic discountPercentageUptoOrFlatAmount;
  int discountTypeId;
  DiscountType discountType;
  dynamic totalAmount;
  String duration;
  int taxPercentage;
  bool isActive;
  List<ServiceRepository> serviceRepositories;

  PackageServiceService({
    this.id,
    this.serviceName,
    this.serviceCode,
    this.description,
    this.avgRating,
    this.createdBy,
    this.modifiedBy,
    this.createdDate,
    this.modifiedDate,
    this.discountPercentageUptoOrFlatAmount,
    this.discountTypeId,
    this.discountType,
    this.totalAmount,
    this.duration,
    this.taxPercentage,
    this.isActive,
    this.serviceRepositories,
  });

  factory PackageServiceService.fromJson(Map<String, dynamic> json) => PackageServiceService(
    id: json["id"] == null ? null : json["id"],
    serviceName: json["serviceName"] == null ? null : json["serviceName"],
    serviceCode: json["serviceCode"] == null ? null : json["serviceCode"],
    description: json["description"] == null ? null : json["description"],
    avgRating: json["avgRating"] == null ? null : json["avgRating"],
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    modifiedBy: json["modifiedBy"] == null ? null : json["modifiedBy"],
    createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
    modifiedDate: json["modifiedDate"] == null ? null : DateTime.parse(json["modifiedDate"]),
    discountPercentageUptoOrFlatAmount: json["discountPercentageUptoOrFlatAmount"] == null ? null : json["discountPercentageUptoOrFlatAmount"],
    discountTypeId: json["discountTypeId"] == null ? null : json["discountTypeId"],
    discountType: json["discountType"] == null ? null : DiscountType.fromJson(json["discountType"]),
    totalAmount: json["totalAmount"] == null ? null : json["totalAmount"],
    duration: json["duration"] == null ? null : json["duration"],
    taxPercentage: json["taxPercentage"] == null ? null : json["taxPercentage"],
    isActive: json["isActive"] == null ? null : json["isActive"],
    serviceRepositories: json["serviceRepositories"] == null ? null : List<ServiceRepository>.from(json["serviceRepositories"].map((x) => ServiceRepository.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "serviceName": serviceName == null ? null : serviceName,
    "serviceCode": serviceCode == null ? null : serviceCode,
    "description": description == null ? null : description,
    "avgRating": avgRating == null ? null : avgRating,
    "createdBy": createdBy == null ? null : createdBy,
    "modifiedBy": modifiedBy == null ? null : modifiedBy,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
    "modifiedDate": modifiedDate == null ? null : modifiedDate.toIso8601String(),
    "discountPercentageUptoOrFlatAmount": discountPercentageUptoOrFlatAmount == null ? null : discountPercentageUptoOrFlatAmount,
    "discountTypeId": discountTypeId == null ? null : discountTypeId,
    "discountType": discountType == null ? null : discountType.toJson(),
    "totalAmount": totalAmount == null ? null : totalAmount,
    "duration": duration == null ? null : duration,
    "taxPercentage": taxPercentage == null ? null : taxPercentage,
    "isActive": isActive == null ? null : isActive,
    "serviceRepositories": serviceRepositories == null ? null : List<dynamic>.from(serviceRepositories.map((x) => x.toJson())),
  };
}

class ServiceRepository {
  int id;
  int serviceId;
  String fileName;
  String fileExtension;
  String fileLocation;

  ServiceRepository({
    this.id,
    this.serviceId,
    this.fileName,
    this.fileExtension,
    this.fileLocation,
  });

  factory ServiceRepository.fromJson(Map<String, dynamic> json) => ServiceRepository(
    id: json["id"] == null ? null : json["id"],
    serviceId: json["serviceId"] == null ? null : json["serviceId"],
    fileName: json["fileName"] == null ? null : json["fileName"],
    fileExtension: json["fileExtension"] == null ? null : json["fileExtension"],
    fileLocation: json["fileLocation"] == null ? null : json["fileLocation"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "serviceId": serviceId == null ? null : serviceId,
    "fileName": fileName == null ? null : fileName,
    "fileExtension": fileExtension == null ? null : fileExtension,
    "fileLocation": fileLocation == null ? null : fileLocation,
  };
}

class ServiceElement {
  int id;
  int appointmentId;
  int serviceId;
  PackageServiceService service;

  ServiceElement({
    this.id,
    this.appointmentId,
    this.serviceId,
    this.service,
  });

  factory ServiceElement.fromJson(Map<String, dynamic> json) => ServiceElement(
    id: json["id"] == null ? null : json["id"],
    appointmentId: json["appointmentId"] == null ? null : json["appointmentId"],
    serviceId: json["serviceId"] == null ? null : json["serviceId"],
    service: json["service"] == null ? null : PackageServiceService.fromJson(json["service"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "appointmentId": appointmentId == null ? null : appointmentId,
    "serviceId": serviceId == null ? null : serviceId,
    "service": service == null ? null : service.toJson(),
  };
}

class Status {
  int id;
  String status;

  Status({
    this.id,
    this.status,
  });

  factory Status.fromJson(Map<String, dynamic> json) => Status(
    id: json["id"] == null ? null : json["id"],
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "status": status == null ? null : status,
  };
}

class ValidationError {
  String name;
  String description;

  ValidationError({
    this.name,
    this.description,
  });

  factory ValidationError.fromJson(Map<String, dynamic> json) => ValidationError(
    name: json["name"] == null ? null : json["name"],
    description: json["description"] == null ? null : json["description"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "description": description == null ? null : description,
  };
}
