// To parse this JSON data, do
//
//     final changePasswordReq = changePasswordReqFromJson(jsonString);

import 'dart:convert';

ChangePasswordReq changePasswordReqFromJson(String str) => ChangePasswordReq.fromJson(json.decode(str));

String changePasswordReqToJson(ChangePasswordReq data) => json.encode(data.toJson());

class ChangePasswordReq {
  String email;
  String oldPassword;
  String newPassword;
  String confirmPassword;

  ChangePasswordReq({
    this.email,
    this.oldPassword,
    this.newPassword,
    this.confirmPassword,
  });

  factory ChangePasswordReq.fromJson(Map<String, dynamic> json) => ChangePasswordReq(
    email: json["email"] == null ? null : json["email"],
    oldPassword: json["oldPassword"] == null ? null : json["oldPassword"],
    newPassword: json["newPassword"] == null ? null : json["newPassword"],
    confirmPassword: json["confirmPassword"] == null ? null : json["confirmPassword"],
  );

  Map<String, dynamic> toJson() => {
    "email": email == null ? null : email,
    "oldPassword": oldPassword == null ? null : oldPassword,
    "newPassword": newPassword == null ? null : newPassword,
    "confirmPassword": confirmPassword == null ? null : confirmPassword,
  };
}
