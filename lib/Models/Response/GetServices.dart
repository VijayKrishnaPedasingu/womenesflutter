// To parse this JSON data, do
//
//     final getServices = getServicesFromJson(jsonString);

import 'dart:convert';

GetServices getServicesFromJson(String str) => GetServices.fromJson(json.decode(str));

String getServicesToJson(GetServices data) => json.encode(data.toJson());

class GetServices {
  List<ListResult> listResult;
  bool isSuccess;
  dynamic affectedRecords;
  String endUserMessage;
  dynamic links;
  List<dynamic> validationErrors;
  dynamic exception;

  GetServices({
    this.listResult,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory GetServices.fromJson(Map<String, dynamic> json) => GetServices(
    listResult: json["listResult"] == null ? null : List<ListResult>.from(json["listResult"].map((x) => ListResult.fromJson(x))),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"],
    validationErrors: json["validationErrors"] == null ? null : List<dynamic>.from(json["validationErrors"].map((x) => x)),
    exception: json["exception"],
  );

  Map<String, dynamic> toJson() => {
    "listResult": listResult == null ? null : List<dynamic>.from(listResult.map((x) => x.toJson())),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links,
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x)),
    "exception": exception,
  };
}

class ListResult {
  dynamic id;
  String serviceName;
  String serviceCode;
  String description;
  dynamic avgRating;
  String createdBy;
  String modifiedBy;
  DateTime createdDate;
  DateTime modifiedDate;
  dynamic discountPercentageUptoOrFlatAmount;
  dynamic discountTypeId;
  DiscountType discountType;
  dynamic totalAmount;
  String duration;
  dynamic taxPercentage;
  bool isActive;
  List<ServiceRepository> serviceRepositories;

  ListResult({
    this.id,
    this.serviceName,
    this.serviceCode,
    this.description,
    this.avgRating,
    this.createdBy,
    this.modifiedBy,
    this.createdDate,
    this.modifiedDate,
    this.discountPercentageUptoOrFlatAmount,
    this.discountTypeId,
    this.discountType,
    this.totalAmount,
    this.duration,
    this.taxPercentage,
    this.isActive,
    this.serviceRepositories,
  });

  factory ListResult.fromJson(Map<String, dynamic> json) => ListResult(
    id: json["id"] == null ? null : json["id"],
    serviceName: json["serviceName"] == null ? null : json["serviceName"],
    serviceCode: json["serviceCode"] == null ? null : json["serviceCode"],
    description: json["description"] == null ? null : json["description"],
    avgRating: json["avgRating"] == null ? null : json["avgRating"].toDouble(),
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    modifiedBy: json["modifiedBy"] == null ? null : json["modifiedBy"],
    createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
    modifiedDate: json["modifiedDate"] == null ? null : DateTime.parse(json["modifiedDate"]),
    discountPercentageUptoOrFlatAmount: json["discountPercentageUptoOrFlatAmount"] == null ? null : json["discountPercentageUptoOrFlatAmount"],
    discountTypeId: json["discountTypeId"] == null ? null : json["discountTypeId"],
    discountType: json["discountType"] == null ? null : DiscountType.fromJson(json["discountType"]),
    totalAmount: json["totalAmount"] == null ? null : json["totalAmount"],
    duration: json["duration"] == null ? null : json["duration"],
    taxPercentage: json["taxPercentage"] == null ? null : json["taxPercentage"],
    isActive: json["isActive"] == null ? null : json["isActive"],
    serviceRepositories: json["serviceRepositories"] == null ? null : List<ServiceRepository>.from(json["serviceRepositories"].map((x) => ServiceRepository.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "serviceName": serviceName == null ? null : serviceName,
    "serviceCode": serviceCode == null ? null : serviceCode,
    "description": description == null ? null : description,
    "avgRating": avgRating == null ? null : avgRating,
    "createdBy": createdBy == null ? null : createdBy,
    "modifiedBy": modifiedBy == null ? null : modifiedBy,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
    "modifiedDate": modifiedDate == null ? null : modifiedDate.toIso8601String(),
    "discountPercentageUptoOrFlatAmount": discountPercentageUptoOrFlatAmount == null ? null : discountPercentageUptoOrFlatAmount,
    "discountTypeId": discountTypeId == null ? null : discountTypeId,
    "discountType": discountType == null ? null : discountType.toJson(),
    "totalAmount": totalAmount == null ? null : totalAmount,
    "duration": duration == null ? null : duration,
    "taxPercentage": taxPercentage == null ? null : taxPercentage,
    "isActive": isActive == null ? null : isActive,
    "serviceRepositories": serviceRepositories == null ? null : List<dynamic>.from(serviceRepositories.map((x) => x.toJson())),
  };
}

class DiscountType {
  int id;
  Discounttype discounttype;

  DiscountType({
    this.id,
    this.discounttype,
  });

  factory DiscountType.fromJson(Map<String, dynamic> json) => DiscountType(
    id: json["id"] == null ? null : json["id"],
    discounttype: json["discounttype"] == null ? null : discounttypeValues.map[json["discounttype"]],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "discounttype": discounttype == null ? null : discounttypeValues.reverse[discounttype],
  };
}

enum Discounttype { PERCENTAGE, FLAT }

final discounttypeValues = EnumValues({
  "Flat": Discounttype.FLAT,
  "Percentage": Discounttype.PERCENTAGE
});

class ServiceRepository {
  int id;
  int serviceId;
  String fileName;
  String fileExtension;
  String fileLocation;

  ServiceRepository({
    this.id,
    this.serviceId,
    this.fileName,
    this.fileExtension,
    this.fileLocation,
  });

  factory ServiceRepository.fromJson(Map<String, dynamic> json) => ServiceRepository(
    id: json["id"] == null ? null : json["id"],
    serviceId: json["serviceId"] == null ? null : json["serviceId"],
    fileName: json["fileName"] == null ? null : json["fileName"],
    fileExtension: json["fileExtension"] == null ? null : json["fileExtension"],
    fileLocation: json["fileLocation"] == null ? null : json["fileLocation"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "serviceId": serviceId == null ? null : serviceId,
    "fileName": fileName == null ? null : fileName,
    "fileExtension": fileExtension == null ? null : fileExtension,
    "fileLocation": fileLocation == null ? null : fileLocation,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
