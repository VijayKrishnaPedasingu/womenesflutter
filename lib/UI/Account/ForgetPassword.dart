import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:womenes/Models/Request/ForgetRequest.dart';
import 'package:womenes/Services/RetrofitServices.dart';

final _fromkey = GlobalKey<FormState>();

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {


  TextEditingController _controllerFP = TextEditingController();

  String validateEmail(String value) {
    if (value.isEmpty) {
      return 'Enter Your Email';
    }
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ForgetPassword'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _fromkey,
          child: Padding(
            padding: const EdgeInsets.all(9.0),
            child: Column(
              children: <Widget>[
                //SizedBox(height: MediaQuery.of(context).size.width * 0.55),
                SizedBox(height: 10,),
                Image.asset("assets/images/mail-send.png"),
                SizedBox(height: 50,),
                TextFormField(
                  controller: _controllerFP,
                  validator: validateEmail,
                  maxLines: 1,
                  style: TextStyle(fontSize: 18),
                  decoration: InputDecoration(
                    hintText: "Enter EMail",
                    labelText: "Enter Your MailId",
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(
                        color: Color(0xffff1493),
                      ),
                    ),
                    prefixIcon: const Icon(
                      Icons.mail,
                      color: Color(0xffff1493),
                    ),
                  ),
                ),

                SizedBox(height: 16,),
                RaisedButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50)),
                    color: Color(0xffff1493),
                    animationDuration: Duration(milliseconds: 2000),
                    child: const Text(
                      'Submit',
                      style: TextStyle(color: Colors.white, fontSize: 19),
                    ),
                    onPressed: (){
                      if(_fromkey.currentState.validate()){
                        String mail = _controllerFP.text.toString().trim();
                        ForgetRequest request = ForgetRequest(
                          userNameOrEmail: mail
                        );
                        final dio = Dio();
                        final client = RestClient(dio);
                        client.forgetPassword(request).then((respo){

                          if (respo.isSuccess) {
                            Fluttertoast.showToast(
                                msg: respo.endUserMessage,
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 3,
                                backgroundColor: Color(0xffff1493),
                                textColor: Colors.white,
                                fontSize: 16.0);
                            Navigator.pop(context);
                          } else {
                            print(respo.validationErrors.length);
                            if (respo.validationErrors.length == 1) {
//                          logger.d(response.toJson());
                              Fluttertoast.showToast(
                                  msg: respo.validationErrors
                                      .elementAt(0)
                                      .description,
                                  toastLength: Toast.LENGTH_SHORT,
                                  timeInSecForIos: 3,
                                  backgroundColor: Color(0xffff1493),
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            } else {
                              Fluttertoast.showToast(
                                  msg: respo.endUserMessage,
                                  toastLength: Toast.LENGTH_SHORT,
                                  timeInSecForIos: 3,
                                  backgroundColor: Color(0xffff1493),
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                            }
                          }

                        }).catchError((Object obj) {
                          print(obj);
                          switch (obj.runtimeType) {
                            case DioError:
                              final res = (obj as DioError).response;

                              if (res.statusCode == 401) {
//                                  Navigator.pushReplacement(
//                                      context,
//                                      MaterialPageRoute(
//                                          builder: (context) => LoginPage()));
                                Fluttertoast.showToast(
                                    msg: " Something Went Wrong..",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 3,
                                    backgroundColor: Color(0xffff1493),
                                    textColor: Colors.white,
                                    fontSize: 16.0);
                              } else if (res.statusCode == 500) {
                                Fluttertoast.showToast(
                                    msg: "Failed to connect Server",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 3,
                                    backgroundColor: Color(0xffff1493),
                                    textColor: Colors.white,
                                    fontSize: 16.0);
                              } else {
                                Fluttertoast.showToast(
                                    msg: " Error Occured ..",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIos: 3,
                                    backgroundColor: Color(0xffff1493),
                                    textColor: Colors.white,
                                    fontSize: 16.0);
                              }
                          }
                        });
                      }
                    }
                    ),

              ],
            ),
          ),
        ),
      ),
    );
  }


}
