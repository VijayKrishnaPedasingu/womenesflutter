// To parse this JSON data, do
//
//     final appointmentreq = appointmentreqFromJson(jsonString);

import 'dart:convert';

UpdateAppointmentReq appointmentreqFromJson(String str) => UpdateAppointmentReq.fromJson(json.decode(str));

String appointmentreqToJson(UpdateAppointmentReq data) => json.encode(data.toJson());

class UpdateAppointmentReq {
  int id;
  String customerUserId;
  int packageId;
  DateTime appointmentDate;
  String slotTime;
  int facilityId;

  UpdateAppointmentReq({
    this.id,
    this.customerUserId,
    this.packageId,
    this.appointmentDate,
    this.slotTime,
    this.facilityId,
  });

  factory UpdateAppointmentReq.fromJson(Map<String, dynamic> json) => UpdateAppointmentReq(
    id: json["id"] == null ? null : json["id"],
    customerUserId: json["customerUserId"] == null ? null : json["customerUserId"],
    packageId: json["packageId"] == null ? null : json["packageId"],
    appointmentDate: json["appointmentDate"] == null ? null : DateTime.parse(json["appointmentDate"]),
    slotTime: json["slotTime"] == null ? null : json["slotTime"],
    facilityId: json["facilityId"] == null ? null : json["facilityId"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "customerUserId": customerUserId == null ? null : customerUserId,
    "packageId": packageId == null ? null : packageId,
    "appointmentDate": appointmentDate == null ? null : appointmentDate.toIso8601String(),
    "slotTime": slotTime == null ? null : slotTime,
    "facilityId": facilityId == null ? null : facilityId,
  };
}
