// To parse this JSON data, do
//
//     final partnerResponse = partnerResponseFromJson(jsonString);

import 'dart:convert';

PartnerResponse partnerResponseFromJson(String str) => PartnerResponse.fromJson(json.decode(str));

String partnerResponseToJson(PartnerResponse data) => json.encode(data.toJson());

class PartnerResponse {
  Result result;
  bool isSuccess;
  int affectedRecords;
  String endUserMessage;
  dynamic links;
  List<dynamic> validationErrors;
  dynamic exception;

  PartnerResponse({
    this.result,
    this.isSuccess,
    this.affectedRecords,
    this.endUserMessage,
    this.links,
    this.validationErrors,
    this.exception,
  });

  factory PartnerResponse.fromJson(Map<String, dynamic> json) => PartnerResponse(
    result: json["result"] == null ? null : Result.fromJson(json["result"]),
    isSuccess: json["isSuccess"] == null ? null : json["isSuccess"],
    affectedRecords: json["affectedRecords"] == null ? null : json["affectedRecords"],
    endUserMessage: json["endUserMessage"] == null ? null : json["endUserMessage"],
    links: json["links"],
    validationErrors: json["validationErrors"] == null ? null : List<dynamic>.from(json["validationErrors"].map((x) => x)),
    exception: json["exception"],
  );

  Map<String, dynamic> toJson() => {
    "result": result == null ? null : result.toJson(),
    "isSuccess": isSuccess == null ? null : isSuccess,
    "affectedRecords": affectedRecords == null ? null : affectedRecords,
    "endUserMessage": endUserMessage == null ? null : endUserMessage,
    "links": links,
    "validationErrors": validationErrors == null ? null : List<dynamic>.from(validationErrors.map((x) => x)),
    "exception": exception,
  };
}

class Result {
  int id;
  String firstName;
  String lastName;
  String email;
  String address;
  String phoneNumber;
  String message;
  bool isActive;
  DateTime createdDate;

  Result({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.address,
    this.phoneNumber,
    this.message,
    this.isActive,
    this.createdDate,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"] == null ? null : json["id"],
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    email: json["email"] == null ? null : json["email"],
    address: json["address"] == null ? null : json["address"],
    phoneNumber: json["phoneNumber"] == null ? null : json["phoneNumber"],
    message: json["message"] == null ? null : json["message"],
    isActive: json["isActive"] == null ? null : json["isActive"],
    createdDate: json["createdDate"] == null ? null : DateTime.parse(json["createdDate"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "email": email == null ? null : email,
    "address": address == null ? null : address,
    "phoneNumber": phoneNumber == null ? null : phoneNumber,
    "message": message == null ? null : message,
    "isActive": isActive == null ? null : isActive,
    "createdDate": createdDate == null ? null : createdDate.toIso8601String(),
  };
}
