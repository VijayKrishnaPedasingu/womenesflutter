import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Request/PackageRatingRequest.dart';

import '../../Services/RetrofitServices.dart';
import '../../Utils/StringsFile.dart';
import '../Account/Login.dart';

class PackageRating extends StatefulWidget {
  String image;
  String name;
  String desc;
  double amount;
  String packageduration;
  int packageId;

  PackageRating(this.image, this.name, this.desc, this.amount,
      this.packageduration, this.packageId);

  @override
  _PackageRatingState createState() => _PackageRatingState(
      image, name, desc, amount, packageduration, packageId);
}

class _PackageRatingState extends State<PackageRating> {
  String image;
  String name;
  String desc;
  double amount;
  String packageduration;
  int packageId;

  double feedback;

  SharedPreferences pref;
  var userId, token;

  _PackageRatingState(this.image, this.name, this.desc, this.amount,
      this.packageduration, this.packageId);

  @override
  void initState() {
    super.initState();
    getPrefs();
    feedback = 0;
  }

  getPrefs() async {
    pref = await SharedPreferences.getInstance();
    token = await pref.get(StringsFile.AccessToken);
    userId = await pref.get(StringsFile.UserID);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xffff1493),
        title: Text(name),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CircleAvatar(
              backgroundImage: NetworkImage(image),
              backgroundColor: Colors.white,
              radius: 50,
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              name,
              style: TextStyle(
                  fontSize: 18,
                  color: Color(0xffff1493),
                  fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              desc,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black45,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              color: Colors.grey[200],
              width: double.infinity,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Amount : Rs $amount",
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Package Duration : ${packageduration.trim().substring(
                        0, 5)} Hr",
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            FlutterRatingBar(
              initialRating: 0,
              ignoreGestures: false,
              itemSize: 40,
              fillColor: Color(0xffff1493),
              borderColor: Colors.black,
              allowHalfRating: true,
              onRatingUpdate: (rating) {
                setState(() {
                  feedback = rating;
                });
              },
            ),
            SizedBox(
              height: 15,
            ),
            RaisedButton(
              onPressed: () async {
                if (feedback != 0) {
                  PackageRatingRequest request = PackageRatingRequest(
                      rating: feedback.floor(), packageId: packageId);

                  final dio = Dio();
                  dio.options.headers["Authorization"] = ("Bearer " + token);
                  final client = RestClient(dio);

                  await client.sendPackageFeedback(request).then((res) async {
                    Fluttertoast.showToast(
                        msg: "Thanks for Your Feedback ",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIos: 3,
                        backgroundColor: Color(0xffff1493),
                        textColor: Colors.white,
                        fontSize: 16.0);
                    Navigator.pop(context);
                    return res;
                  }).catchError((Object obj) {
                    print(obj);
                    switch (obj.runtimeType) {
                      case DioError:
                        final res = (obj as DioError).response;

                        if (res.statusCode == 401) {
                          pref.clear();
                          pref.commit();
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginPage()));
                          Fluttertoast.showToast(
                              msg: " Connection TimeOut..",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 3,
                              backgroundColor: Color(0xffff1493),
                              textColor: Colors.white,
                              fontSize: 16.0);
                        } else if (res.statusCode == 500) {
                          Fluttertoast.showToast(
                              msg: "Failed to connect Server",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 3,
                              backgroundColor: Color(0xffff1493),
                              textColor: Colors.white,
                              fontSize: 16.0);
                        } else {
                          Fluttertoast.showToast(
                              msg: " Error Occured ..",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 3,
                              backgroundColor: Color(0xffff1493),
                              textColor: Colors.white,
                              fontSize: 16.0);
                        }

                    }
                  });
                } else {
                  Fluttertoast.showToast(
                      msg: "Please Give Rating ",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 3,
                      backgroundColor: Color(0xffff1493),
                      textColor: Colors.white,
                      fontSize: 16.0);
                }
              },
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              color: Color(0xffff1493),
              child: Text(
                "Submit",
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
