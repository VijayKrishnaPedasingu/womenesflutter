import 'package:flutter/material.dart';

import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:womenes/Models/Response/GetAppointmentByUserId.dart';
import 'package:womenes/Models/Request/CancelAppointment.dart';
import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/UI/Appointments/EditAppointment.dart';
import 'package:womenes/Utils/StringsFile.dart';

import '../../Models/Request/CancelAppointment.dart';
import '../../Models/Response/GetAppointmentByUserId.dart';
import '../../Services/RetrofitServices.dart';
import '../Account/Login.dart';
import 'AddAppointments.dart';
import 'EditAppointment.dart';
import 'VisitSummary.dart';

final logger = Logger();

class MyAppointments extends StatefulWidget {
  @override
  _MyAppointmentsState createState() => _MyAppointmentsState();
}

class _MyAppointmentsState extends State<MyAppointments> {
  TextStyle textStyle = TextStyle(
      color:Color(0xffff1493), fontSize: 16, fontWeight: FontWeight.w600);
  TextStyle contentStyle = TextStyle(
      color: Color(0xff757575), fontSize: 16, fontWeight: FontWeight.w500);

  SharedPreferences pref;
  var userid;
  var token;
  var appointmentid;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xffff1493),
          title: Text('My Appointments'),
        ),
        body: FutureBuilder(
            future: getdata(),
            builder: (context, snapshot) {
              GetAppointmentsByUserId result = snapshot.data;
              if(snapshot.connectionState == ConnectionState.done){
              if (snapshot.data != null) {
                return result.listResult.length == 0
                    ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          " Book your Appointment Now!!!",
                          style: TextStyle(
                              fontSize: 23.0, color: Color(0xff757575)),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        OutlineButton(
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> AddAppointments()));
                          },
                          child: Text(
                            'Book Appointments',
                            style: TextStyle(
                              color: Color(0xffff1493),
                              fontSize: 20.0,
                            ),
                          ),
                          borderSide: BorderSide(
                              color: Color(0xffff1493),
                              style: BorderStyle.solid,
                              width: 2.0),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                        ),
                      ],
                    ))
                    : ListView.builder(
                    shrinkWrap: true,
                    itemCount: result.listResult.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Card(
                          color: Colors.white,
                          elevation: 3.0,
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "Appointment Date ",
                                          style: textStyle,
                                        ),
                                        Text(
                                          "Slot Time",
                                          style: textStyle,
                                        ),
                                        Text(
                                          "Package Name",
                                          style: textStyle,
                                        ),
                                        Text(
                                          "Status",
                                          style: textStyle,
                                        ),
                                        Text(
                                          "Facility",
                                          style: textStyle,
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          result.listResult[index]
                                              .appointmentDate
                                              .toIso8601String()
                                              .trim()
                                              .substring(0, 10),
                                          style: contentStyle,
                                        ),
                                        Text(
                                          result.listResult[index].slotTime,
                                          style: contentStyle,
                                        ),
                                        result.listResult[index].package == null ? Text(
                                          "No Packages Available",
                                          style:TextStyle(fontSize: 14,color: Color(0xff757575)),
                                        ) : Text(
                                          result.listResult[index].package.name,
                                          style: contentStyle,
                                        ),
                                        Text(
                                          result.listResult[index].status
                                              .status,
                                          style: contentStyle,
                                        ),
                                        Text(
                                          result.listResult[index].facility
                                              .facilityName,
                                          style: contentStyle,
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    IconButton(
                                        icon: Icon(Icons.edit,
                                            color: Color(0xff757575)),
                                        onPressed: () async {
                                          /*  await pref.setInt("AppointmentId", result.listResult[index].id);*/
                                          Navigator.push(context, MaterialPageRoute(builder: (context) =>EditAppointment(
                                              result.listResult[index].id,
                                              result.listResult[index].appointmentDate.toIso8601String().trim().substring(0, 10),
                                              result.listResult[index].slotTime.toString())));
                                        }),
                                    IconButton(icon: Icon(Icons.delete, color: Color(0xff757575),),
                                        onPressed: () {
                                          showDialog(
                                              context: context,
                                              builder: (context) {
                                                return AlertDialog(
                                                  title: Text('Cancel Appointment'),
                                                  content: Text('Are you sure want to cancel the Appointment?'),
                                                  actions: <Widget>[
                                                    FlatButton(
                                                      onPressed: () async {
                                                        final dio = Dio();
                                                        dio.options.headers["Authorization"] = ("Bearer " + token);
                                                        final client = RestClient(dio);
                                                        CancelAppointment response = await client.deleteAppointment(result.listResult[index].id.toString()).then((res) async {
                                                          return res;
                                                        });
                                                        if (response.isSuccess == true) {
                                                          setState(() {
                                                            getdata();
                                                          });
                                                        }
                                                        Fluttertoast.showToast(
                                                            msg: "Appointment Cancelled",
                                                            toastLength: Toast.LENGTH_SHORT,
                                                            gravity: ToastGravity.BOTTOM,
                                                            timeInSecForIos: 3,
                                                            backgroundColor:Color(0xffff1493),
                                                            textColor: Colors.white,
                                                            fontSize: 16.0);

                                                        Navigator.pop(
                                                            context);
                                                      }, //
                                                      child: Text('Yes',style: textStyle,),
                                                    ),
                                                    FlatButton(
                                                        onPressed: () {
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: Text('No',style: textStyle,))
                                                  ],
                                                );
                                              });
                                        })
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    });
              } else {
                return Center(child: CircularProgressIndicator( valueColor: new AlwaysStoppedAnimation<Color>(Color(0xffff1493)),));
              }}else if (snapshot.connectionState == ConnectionState.waiting){return Center(child: CircularProgressIndicator(  valueColor: new AlwaysStoppedAnimation<Color>(Color(0xffff1493)),),);}
    else{
    return Center(
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
    Icon(Icons.flash_on,color: Colors.yellow,size: 50,),
    Text("No Internet",style: TextStyle(color: Colors.black87,fontSize: 18),)
    ],
    ),
    );
    }
            }
        ));
  }

  Future getdata() async {

    pref = await SharedPreferences.getInstance().then((sp) {
      token = sp.getString(StringsFile.AccessToken);
      userid = sp.getString(StringsFile.UserID);
      return sp;
    });

    final dio = Dio();
    dio.options.headers["Authorization"] = ("Bearer " + token);
    final client = RestClient(dio);
    GetAppointmentsByUserId data = await client.getAppointments(userid).then((response) {
      return response;
    }).catchError((Object obj) {
      print(obj);
      switch (obj.runtimeType) {
        case DioError:
          final res = (obj as DioError).response;

          if (res.statusCode == 401) {
            pref.clear();
            pref.commit();
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => LoginPage()));
            Fluttertoast.showToast(
                msg: " Connection TimeOut..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          } else if (res.statusCode == 500) {
            Fluttertoast.showToast(
                msg: "Failed to connect Server",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }
          else {
            Fluttertoast.showToast(
                msg: " Error Occured ..",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                backgroundColor: Color(0xffff1493),
                textColor: Colors.white,
                fontSize: 16.0);
          }
          break;
        default:
      }
    });

    return data;
  }
}
