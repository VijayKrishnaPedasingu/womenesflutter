import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:womenes/Models/Request/LoginRequest.dart';
import 'package:womenes/Models/Response/LoginResponse.dart';
import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/UI/Account/ForgetPassword.dart';
import 'package:womenes/UI/HomePage.dart';
import 'package:womenes/UI/Account/Register.dart';
import 'package:womenes/Utils/StringsFile.dart';

final logger = Logger();

bool _obscureText = true;

LoginResponse loginResponse =null;
class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  SharedPreferences prefs;
  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();

  Future GetPref() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    GetPref();
  }

  @override
  Widget build(BuildContext context) {
    void _toggle() {
      setState(() {
        _obscureText = !_obscureText;
      });
    }

    // TODO: implement build
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 100,
              ),
              SizedBox(
                height: 100,
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                    child: Image.asset(
                      'assets/images/logo.png',
                    ),
                  ),
                ),
              ),
              Text(
                "Login",
                style: TextStyle(
                    fontSize: 25,
                    decorationThickness: 20,
                    decorationColor: Colors.black26),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 18.0, left: 18),
                child: TextField(
                  controller: username,
                  maxLines: 1,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                    hintText: "User Name",
                    labelText: "Enter UserName",
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(
                        color: Color(0xffff1493),
                      ),
                    ),
                    prefixIcon: const Icon(
                      Icons.account_circle,
                      color: Color(0xffff1493),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    right: 18.0, left: 18, top: 18, bottom: 18),
                child: TextFormField(
                  obscureText: _obscureText,
                  controller: password,
                  maxLines: 1,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                    labelText: "Enter Your Password",
                    hintText: "Password",
                    border: new OutlineInputBorder(
                      borderSide: new BorderSide(
                        color: Color(0xffff1493),
                      ),
                    ),
                    prefixIcon: const Icon(
                      Icons.lock,
                      color: Color(0xffff1493),
                    ),
                    suffixIcon: GestureDetector(
                      child: const Icon(Icons.remove_red_eye),
                      onTap: _toggle,
                    ),
                  ),
                ),
              ),

              /*FlatButton(
                  onPressed: _toggle,
                  child: new Text(_obscureText ? "Show" : "Hide")),*/

              SizedBox(
                height: 50,
                width: 250,
                child: RaisedButton(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(50)),
                  animationDuration: Duration(milliseconds: 2000),
                  color: Color(0xffff1493),
                  child: const Text(
                    'Login',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                  onPressed: () {
                    if (username.text.toString().isNotEmpty &&
                        password.text.toString().isNotEmpty) {
                      LoginRequest loginRequest = new LoginRequest(
                          scope: "api1",
                          clientId: "sd.mobile",
                          clientSecret: "Womenes!@",
                          userName: username.text.toString(),
                          password: password.text.toString());

                      Map<String, String> headers = {
                        HttpHeaders.contentTypeHeader:
                            "application/json", // or whatever
                        HttpHeaders.authorizationHeader: "Bearer ",
                      };
                      final dio = Dio(); // Provide a dio instance
                      // dio.options.headers["Authorization"] = "Bearer ";
                      //   dio.interceptors.add(element);
                      final client = RestClient(dio);

                      client.login(loginRequest).then((login) async {
                        if (login.isSuccess) {


                              loginResponse = login;
                            //   LoginResponse login =LoginResponse.fromJson(loginresponse.toJson());
                            //  Toast.show("Login", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                            // logger.d(login.toJson());
                            //  await  prefs.setString("Token", login.result.accessToken);
                            await prefs.setString(StringsFile.Username,
                                login.result.user.userName.toString());
                            await prefs.setString(StringsFile.AccessToken,
                                login.result.accessToken);
                            await prefs.setString(
                                StringsFile.TokenType, login.result.tokenType);

                            await prefs.setString(
                                StringsFile.UserID, login.result.user.id);
                            await prefs.setString(
                                StringsFile.Email, login.result.user.email);
                            await prefs.setString(StringsFile.FirstName,
                                login.result.user.firstName);
                            await prefs.setString(StringsFile.LastName,
                                login.result.user.lastName);
                            await prefs.setString(StringsFile.PhoneNumber,
                                login.result.user.phoneNumber);
                            await prefs.setString(StringsFile.RefferalPoints,
                                login.result.user.referalBonus.toString());
                            if ((login.result.userInfo) != null) {
                              login.result.userInfo.fileName.toString() == null
                                  ? null
                                  : await prefs.setString(
                                      StringsFile.Filename,
                                      login.result.userInfo.fileName
                                          .toString());
                              login.result.userInfo.fileExtension.toString() ==
                                      null
                                  ? null
                                  : await prefs.setString(
                                      StringsFile.FileExtenstion,
                                      login.result.userInfo.fileExtension
                                          .toString());
                              login.result.userInfo.fileLocation.toString() ==
                                      null
                                  ? null
                                  : await prefs.setString(
                                      StringsFile.FileLocation,
                                      login.result.userInfo.fileLocation
                                          .toString());
                              //   print(loginresponse.result.user.userName.toString());
                              //    print(prefs.get("Token"));
                            }
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomePage()));
                              Fluttertoast.showToast(
                                  msg:login.endUserMessage,
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 3,
                                  backgroundColor:Color(0xffff1493),
                                  textColor: Colors.white,
                                  fontSize: 16.0);

                        } else {
                          Fluttertoast.showToast(
                              msg:login.endUserMessage,
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 3,
                              backgroundColor:Color(0xffff1493),
                              textColor: Colors.white,
                              fontSize: 16.0);
                        }
                      }).catchError((Object obj) {
                        print(obj);
                        switch (obj.runtimeType) {
                          case DioError:
                            final res = (obj as DioError).response;
                            if (res.statusCode == 401) {
                              Fluttertoast.showToast(
                                  msg:"UnAuthorized attempt",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 3,
                                  backgroundColor:Color(0xffff1493),
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                          /*    Toast.show("UnAuthorized attempt", context,
                                  duration: Toast.LENGTH_SHORT,
                                  gravity: Toast.BOTTOM);*/
                            } else {
                              Fluttertoast.showToast(
                                  msg:"Error Occured",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIos: 3,
                                  backgroundColor:Color(0xffff1493),
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                             /* Toast.show("Error Occured", context,
                                  duration: Toast.LENGTH_SHORT,
                                  gravity: Toast.BOTTOM);*/
                            }
                            break;
                          default:
                        }
                      });
                    } else {
                      Fluttertoast.showToast(
                          msg:"Please Enter All Fields",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIos: 3,
                          backgroundColor:Color(0xffff1493),
                          textColor: Colors.white,
                          fontSize: 16.0);
                    /*  Toast.show("Please Enter All Fields", context,
                          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);*/
                    }

                    // Perform some action
                  },
                ),
              ),

              SizedBox(height: 10,),
              InkWell(
                child: Text(
                  " Forget Password.?",
                  style: TextStyle(color: Color(0xffff1493), fontSize: 16),
                ),
                onTap: (){
                Navigator.push(context, MaterialPageRoute(
                      builder: (context) => ForgetPassword()));
                },
              ),

              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "New User?",
                    style: TextStyle(fontSize: 20),
                  ),
                  GestureDetector(
                    child: Text(
                      " Register..",
                      style: TextStyle(color: Color(0xffff1493), fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegistrationPage()));
                    },
                  )
                ],
              )
            ],
          ),
        ),

      ),
    );
  }
}


