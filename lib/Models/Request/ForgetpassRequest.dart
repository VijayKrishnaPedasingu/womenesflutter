// To parse this JSON data, do
//
//     final frogetPassRequest = frogetPassRequestFromJson(jsonString);

import 'dart:convert';

class FrogetPassRequest {
  String userNameOrEmail;

  FrogetPassRequest({
    this.userNameOrEmail,
  });

  factory FrogetPassRequest.fromRawJson(String str) => FrogetPassRequest.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory FrogetPassRequest.fromJson(Map<String, dynamic> json) => FrogetPassRequest(
    userNameOrEmail: json["userNameOrEmail"] == null ? null : json["userNameOrEmail"],
  );

  Map<String, dynamic> toJson() => {
    "userNameOrEmail": userNameOrEmail == null ? null : userNameOrEmail,
  };
}
