import 'package:dio/dio.dart';
import 'package:flutter/material.dart';


import 'package:flutter_rounded_date_picker/rounded_date_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:womenes/Models/Response/AppointmentResponse.dart';
import 'package:womenes/Models/Response/GetServicesTimings.dart';
import 'package:womenes/Models/Request/UpdateAppointmentRequest.dart';
import 'package:womenes/Services/RetrofitServices.dart';
import 'package:womenes/Utils/StringsFile.dart';


final logger  = Logger();

class EditAppointment extends StatefulWidget {
  String appointmentdate;
  var appointmentId;

  EditAppointment(this.appointmentId,this.appointmentdate, this.slottime);

  String slottime;


  @override
  _EditAppointmentState createState() =>
      _EditAppointmentState(this.appointmentId,appointmentdate, slottime);
}

class _EditAppointmentState extends State<EditAppointment> {
  String day;
  String month;
  String year;
  var appointmentId;
  String appointmentdate;
  String slottime;
  _EditAppointmentState(this.appointmentId,this.appointmentdate, this.slottime);
  List<bool> btn_bg = List<bool>();
  List<bool> btn_txt = List<bool>();
  int _selectedItem ;
  bool timings = false;
  SharedPreferences pref;
  var userid;
  var token;
  String time;
  String selectedTime;
  DateTime date;



  TextStyle textStyle = TextStyle(color: Color(0xffff1493), fontSize: 18, fontWeight: FontWeight.w600);
  TextStyle contentStyle = TextStyle(color: Color(0xff757575), fontSize: 18, fontWeight: FontWeight.w500);

  selectItem(index) {
    setState(() {
      _selectedItem = index;

    });
  }

  selecttime(time) {
    setState(() {
      selectedTime = time;

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffff1493),
        title: Text("Edit Appointments"),
      ),
      body: SingleChildScrollView(

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Appointment Date :",
                        style: contentStyle,
                      ),

                      Text(
                        "Slot Booked Time :",
                        style: contentStyle,
                      ),
                    ],
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(appointmentdate, style: contentStyle,),
                      Text(slottime, style: contentStyle,
                      ),
                    ],
                  ),
                ],
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: Text('Choose Date',style: contentStyle,),
            ),

            Padding(
              padding: const EdgeInsets.all(20.0),
              child: ListTile(
                title: Text(
                  "Pick Up Date",
                  style: TextStyle(fontSize: 18, color:Color(0xffff1493),),
                ),
                trailing: Icon(
                  Icons.calendar_today,
                  color: Color(0xffff1493),
                ),
                subtitle: Text(
                  "Click here",
                  style: TextStyle(color: Color(0xff757575)),
                ),
                onTap: () async {
                  DateTime newDateTime = await RoundedDatePicker.show(context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(DateTime.now().year,
                          DateTime.now().month, DateTime.now().day),
                      lastDate: DateTime(DateTime.now().year + 101),
                      theme: ThemeData(
                          primaryColor: Color(0xffff1493),
                          accentColor:Color(0xffff1493),
                          textTheme: TextTheme(
                            caption: TextStyle(
                                color: Color(0xffff1493),
                                fontWeight: FontWeight.w600),
                          )));
                  date = newDateTime;
                  day = newDateTime.day.toString();
                  month = newDateTime.month.toString();
                  year = newDateTime.year.toString();
                  setState(() {
                    timings = true;
                  });
                },
              ),
            ),
            Visibility(
              visible: timings,
              child: FutureBuilder(
                future: timings ? getData() : null,
                builder: (context, snapshot) {
                  GetServiceTimings data = snapshot.data;

                  if (snapshot.data != null) {
                    return Column(children: <Widget>[
                      Text(
                        "Date : $day/$month/$year",
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GridView.builder(
                        shrinkWrap: true,
                        itemCount:
                        data.listResult[0].timings.length, // timings  count
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          childAspectRatio: MediaQuery.of(context).size.width /
                              (MediaQuery.of(context).size.height / 4),
                        ), // for Row count

                        itemBuilder: (BuildContext context, int index) {
                          return CustomItem(
                            selecttime,
                            selectItem, // callback function, setstate for parent
                            index: index,
                            isSelected: _selectedItem == index ? true : false,
                            title: data.listResult[0].timings[index].time,
                          );
                        },
                      ),
                    ]);
                  } else {
                    return Center(child: (CircularProgressIndicator( valueColor: new AlwaysStoppedAnimation<Color>(Color(0xffff1493)),)));
                  }
                },
              ),
            ),
            SizedBox(
              height: 20,
            ),
            RaisedButton(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              onPressed: () async {
                if(day != null && _selectedItem != null){

                  UpdateAppointmentReq request = UpdateAppointmentReq(
                    id: appointmentId,
                    appointmentDate: date,
                    facilityId: 9,
                    customerUserId: userid,
                    slotTime: selectedTime,

                  );

                  final dio = Dio();
                  dio.options.headers["Authorization"] = ("Bearer "+token);
                  final client = RestClient(dio);
                  AppointmentResponse response = await client.updateAppointment(request).then((res) async {
                    return res;

                  }).catchError((Object obj) {
                    print(obj.toString());
                    switch (obj.runtimeType) {
                      case DioError:
                        final res = (obj as DioError).response;
                        if (res.statusCode == 401) {
                          logger.e("Got error 401 : ${res.statusCode} -> ${res
                              .statusMessage}");
                        }
                        else {
                          logger.e("Got error : ${res.statusCode} -> ${res
                              .statusMessage}");
                        }
                        break;
                      default:
                    }
                  });
                  Navigator.pop(context);

                  Fluttertoast.showToast(
                      msg: "Updated Successfully",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 3,
                      backgroundColor:Color(0xffff1493),
                      textColor: Colors.white,
                      fontSize: 16.0);
                }else if(day == null){
                  Fluttertoast.showToast(
                      msg: "Please Select Date",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIos: 3,
                      backgroundColor:Color(0xffff1493),
                      textColor: Colors.white,
                      fontSize: 16.0);
                }else {

                  if(_selectedItem == null){
                    Fluttertoast.showToast(
                        msg: "Please Select Time",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIos: 3,
                        backgroundColor: Color(0xffff1493),
                        textColor: Colors.white,
                        fontSize: 16.0);
                  }
                }

              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),),
              color:Color(0xffff1493),
              child: Text(" Update Appointment ",
                style: TextStyle( color: Colors.white, fontSize: 18, fontWeight: FontWeight.w600),
              ),
            )],
        ),
      ),
    );
  }

  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    GetStrings();
  }

  Future GetStrings() async {
    pref =  await SharedPreferences.getInstance();
    token = await pref.getString(StringsFile.AccessToken);
    userid = await pref.getString(StringsFile.UserID);
   // appointmentid = await pref.getInt("AppointmentId");

  }

  Future<GetServiceTimings> getData() async {
    final dio = Dio();
    GetServiceTimings appointments; // model class  obj
    final client = RestClient(dio); // retro fit  obj
    appointments = (await client.getTimings("$month/$day/$year").then((res) {
      return res;
    }));
    for (int i = 0; i < appointments.listResult[0].timings.length; i++) {
      btn_bg.add(false);
      btn_txt.add(false);
    }

    return appointments;
  }


}

class CustomItem extends StatefulWidget {
  final String title;
  final int index;
  final bool isSelected;
  Function(int) selectItem;
  Function(String) selecttime;

  CustomItem(
      this.selecttime,
      this.selectItem, {
        Key key,
        this.title,
        this.index,
        this.isSelected,
      }) : super(key: key);

  _CustomItemState createState() => _CustomItemState();
}

class _CustomItemState extends State<CustomItem> {
  Color pink =Color(0xffff1493);
  Color white = Color(0xffffffff);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: RaisedButton(
        onPressed: () {
          widget.selectItem(widget.index);
          widget.selecttime(widget.title);
        },
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: widget.isSelected ? pink : white,
        child: Text("${widget.title}",
          style: TextStyle(color: widget.isSelected ? white : pink),
        ),
      ),
    );
  }



}
